/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
/ Rotinas de Inicializacao da MCU
===============================================================================================*/

/* MODULE MCUinit */

#include <MC9S08AC32.h>                /* I/O map for MC9S08AC32CFG */
#include "MCUinit.h"
#include "masterheader.h"



/*
** ===================================================================
**     Interrupt handler : isrVsci1tx
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVsci1tx(void)
{
  /* Write your interrupt code here ... */
  
}
// end of isrVsci1tx //


// ===================================================================
//     Interrupt handler : isrVadc1
//
//     Description :
//         User interrupt service routine. 
//     Parameters  : None
//     Returns     : Nothing
// ===================================================================
__interrupt void isrVadc1(void)
{
   if(cntMediaADC < MEDIA_ADC)
   {
      cntMediaADC++;
      somaADC += (((ADC1RH & 0x03) << 8) + ADC1RL);
   } 
   else
   {
      if(indexADC > 0)
      {
          valorADC[indexADC] = somaADC / 56;
      }   
      else
      {
          valorADC[indexADC] = somaADC >> 6; 
      }
      somaADC = 0;
      cntMediaADC = 0;
     
      if(indexADC < 4)
      {
          indexADC++; 
          auxConfigADC++;
      }
      else
      {
        indexADC = 0;
        auxConfigADC = 0x43;
      }
                
      tempT1 = valorADC[1] + off_set_temperatura1;
      tempT2 = valorADC[2] + off_set_temperatura2;
      tempT3 = valorADC[3] + off_set_temperatura3;
      
      
 
   }
  (void)(ADC1RH == 0); // Dummy read 
  (void)(ADC1RL == 0);
  ADC1SC1 = auxConfigADC;   	 
}

// ===================================================================
//     Interrupt handler : isrVsci1rx
//
//     Description :
//         User interrupt service routine. 
//     Parameters  : None
//     Returns     : Nothing
// ===================================================================
__interrupt void isrVsci1rx(void)
{
   char chrrx;

	 chrrx = SCI1S1;
   chrrx = SCI1C3;
   chrrx = SCI1D;
	 
   PtrEnt1++;
	 if(PtrEnt1 >= MaxBufRx) {PtrEnt1 = 0;}
	 BufRx1[PtrEnt1] = chrrx;   
   //TxChar1(chrrx); 
}
////////////////////////////////////////////
//

byte Received;
__interrupt void isrVsci2rx(void)
{
   char chrrx;
	
	 chrrx = SCI2S1;
   chrrx = SCI2C3;
   chrrx = SCI2D;
	 
   PtrEnt2++;
	 if(PtrEnt2 >= MaxBufRx) {PtrEnt2 = 0;}
	 BufRx2[PtrEnt2] = chrrx;
	 
	 
 
}
// end of isrVsci1rx

// ===================================================================
//     Interrupt handler : isrVsci1err
//
//    Description :
//         User interrupt service routine. 
//     Parameters  : None
//     Returns     : Nothing
// ===================================================================
__interrupt void isrVsci1err(void)
{
  // Write your interrupt code here ...

}
// end of isrVsci1err 
   
// ===================================================================
//     Interrupt handler : isrVtpm1ovf
//
//     Description :
//         User interrupt service routine. 
//     Parameters  : None
//     Returns     : Nothing
// ===================================================================
char DebSensor(char pino)
{
 /*
  #define CNT_DEB_PINON 1
  #define MASK_PINO     1

  unsigned char pinoN,cntPinoN++;
  
  if(pinoN == 1){cntPinoN++;}else{cntPinoN--;}
  
  if(cntPinoN > CNT_DEB_PINON){MASK_PINO = 1;}
  else if(cntPinoN < CNT_DEB_PINON){MASK_PINO = 0;}
  */
}

byte reg;
__interrupt void isrVtpm1ovf(void)
{
   char aux;
   (void)(TPM1SC == 0);
 ///DEBOUNCES
 //GIRO
   
   if(SENS_GIRO_CARROSSEL == 1)
   {
    if (cntDebGiro < CNT_DEB_GIRO){cntDebGiro++;}
   } 
   else {cntDebGiro = 0;}
   if(cntDebGiro >= CNT_DEB_GIRO){SENSOR_GIRO_CARROSSEL = 1;} 
   else{SENSOR_GIRO_CARROSSEL = 0;} 
   
 //BAIXO
   if(SENS_CESTO_BAIXO == 1) 
   {
    if (cntDebBaixo  < CNT_DEB_BAIXO){cntDebBaixo ++;}       
   }   
   else {cntDebBaixo  = 0;}
   if(cntDebBaixo  >= CNT_DEB_BAIXO){SENSOR_CESTO_BAIXO = 1;} 
   else{SENSOR_CESTO_BAIXO = 0;} 
 //ALTO
   if(SENS_CESTO_ALTO == 1)
   {
    if(cntDebAlto < CNT_DEB_ALTO){cntDebAlto ++;}
   }
   else {cntDebAlto  = 0;}
   if(cntDebAlto  >= CNT_DEB_ALTO){SENSOR_CESTO_ALTO = 1;} 
   else{SENSOR_CESTO_ALTO = 0;} 

 //POSICAO FINAL
   if(SENS_POSICAO_0 == 1)
   {
    if(cntDebPos0 < CNT_DEB_ZERO){cntDebPos0 ++;}
   }  
   else {cntDebPos0  = 0;}
   if(cntDebPos0  >= CNT_DEB_ZERO){SENSOR_POSICAO_0 = 1;} 
   else{SENSOR_POSICAO_0 = 0;} 
   
 ///FIM DEBOUNCE
   
   if(multTimer > 3)//500ms
   {
     multTimer = 0;
     if(DelTela < 200){DelTela++;} //Atualiza��o automatica telas   
      
     AtuPosCarro();   
     
     if(DelSendNuvem < 200){DelSendNuvem++; } 
     
     if(DelComando < 200){DelComando++;}//time-out comando nuvem   
     //Entra a cada 1 segundo   
     if(meioSeg > 1)
     {
        Seg++;

        if(numAlarmes > 0){ligaBuzzer = 1;} 
        else{ligaBuzzer = 0;}
        
        if(ligaBuzzer){BUZZER_EXT ^=1;}
        else{BUZZER_EXT =0;}
        //LED externo indicacao
        if(numAlarmes>0){LED_EXT = BUZZER_EXT;}
        else{LED_EXT = 1;}
        
   
        if(flagLeRtc < 200){ flagLeRtc++; }
        
        //Tempos = 0xFF parado 
        if(TempoRetPause < 0xFF){TempoRetPause++;}
        if(TempoEscorrendo < 0xFF){TempoEscorrendo++;}
        if(TempoTempBaixa1 < 0xFF){TempoTempBaixa1++;}
        if(TempoTempBaixa2 < 0xFF){TempoTempBaixa2++;}
        if(TempoTempBaixa3 < 0xFF){TempoTempBaixa3++;}
        
        if(TempoSubindo  < 0xFF){TempoSubindo++;}
        if(TempoDescendo < 0xFF){TempoDescendo++;}
        if(TempoGirando  < 0xFF){TempoGirando++;}  
        
        meioSeg = 0;     
     }
     else
     {meioSeg++;}
     
     //Entra a cada 1 minuto
     if(Seg > 59)
     {
        if(TempoAgitando < 0xFF){TempoAgitando++;}
        Seg = 0;   
     }     
   }
   else
   {
    multTimer++;
   }
   TPM1SC_TOF = 0;
}
byte reg1;
//////////////////////////////////////////////////////
//
__interrupt void isrVtpm2ovf(void)			//500ms
{
   (void)(TPM2SC == 0);
  
    
    Base_PWM++;
    if(Base_PWM > 99) Base_PWM = 0;
    
    ControlaTemperatura(); 
    
    //PTBD^=(1<<1);
    //tempo_erro++;
    //tempo_5_segundos++;   
    TPM2SC_TOF=0;
}



//
// ===================================================================
//     Interrupt handler : isrVicg
//
//     Description :
//         User interrupt service routine. 
//     Parameters  : None
//     Returns     : Nothing
// ===================================================================
//
__interrupt void isrVicg(void)
{
  /* Write your interrupt code here ... */
  char aux;
  aux=ICGS1;
  ICGS1_ICGIF=1;
   /*  System clock initialization */
  /* ICGC1: HGO=0,RANGE=1,REFS=1,CLKS1=1,CLKS0=1,OSCSTEN=1,LOCD=0 */
  ICGC1 = 0x7C;                                      
  /* ICGC2: LOLRE=0,MFD2=0,MFD1=0,MFD0=0,LOCRE=0,RFD2=0,RFD1=0,RFD0=0 */
  ICGC2 = 0x00;                                      
  if (*(unsigned char*far)0xFFBE != 0xFF) { /* Test if the device trim value is stored on the specified address */
    ICGTRM = *(unsigned char*far)0xFFBE; /* Initialize ICGTRM register from a non volatile memory */
  }
  while(!ICGS1_LOCK) {                 /* Wait */
  }
}
/* end of isrVicg */



/* Initialization of the CPU registers in FLASH */

/* NVPROT: FPS7=1,FPS6=1,FPS5=1,FPS4=1,FPS3=1,FPS2=1,FPS1=1,FPDIS=1 */
const unsigned char NVPROT_INIT @0x0000FFBD = 0xFF;

/* NVOPT: KEYEN=0,FNORED=1,SEC01=1,SEC00=0 */
const unsigned char NVOPT_INIT @0x0000FFBF = 0x7E;



extern near void _Startup(void);

/* Interrupt vector table */
#ifndef UNASSIGNED_ISR
  #define UNASSIGNED_ISR ((void(*near const)(void)) 0xFFFF) /* unassigned interrupt service routine */
#endif

void (* near const _vect[])(void) @0xFFC6 = { /* Interrupt vector table */
         UNASSIGNED_ISR,               /* Int.no. 28 Vtpm3ovf (at FFC6)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 27 Vtpm3ch1 (at FFC8)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 26 Vtpm3ch0 (at FFCA)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 25 Vrti (at FFCC)                  Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 24 Viic1 (at FFCE)                 Unassigned */
         isrVadc1,                     /* Int.no. 23 Vadc1 (at FFD0)                 Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 22 Vkeyboard1 (at FFD2)            Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 21 Vsci2tx (at FFD4)               Unassigned */
         isrVsci2rx,                   /* Int.no. 20 Vsci2rx (at FFD6)               Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 19 Vsci2err (at FFD8)              Unassigned */
         isrVsci1tx,                   /* Int.no. 18 Vsci1tx (at FFDA)               Used */
         isrVsci1rx,                   /* Int.no. 17 Vsci1rx (at FFDC)               Used */
         isrVsci1err,                  /* Int.no. 16 Vsci1err (at FFDE)              Used */
         UNASSIGNED_ISR,               /* Int.no. 15 Vspi1 (at FFE0)                 Unassigned */
         isrVtpm2ovf,                  /* Int.no. 14 Vtpm2ovf (at FFE2)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 13 Vtpm2ch1 (at FFE4)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 12 Vtpm2ch0 (at FFE6)              Unassigned */
         isrVtpm1ovf,                  /* Int.no. 11 Vtpm1ovf (at FFE8)              Used */
         UNASSIGNED_ISR,               /* Int.no. 10 VReserved10 (at FFEA)           Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  9 VReserved9 (at FFEC)            Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  8 Vtpm1ch3 (at FFEE)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  7 Vtpm1ch2 (at FFF0)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  6 Vtpm1ch1 (at FFF2)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  5 Vtpm1ch0 (at FFF4)              Unassigned */
         isrVicg,                      /* Int.no.  4 Vicg (at FFF6)                  Used */
         UNASSIGNED_ISR,               /* Int.no.  3 Vlvd (at FFF8)                  Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  2 Virq (at FFFA)                  Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  1 Vswi (at FFFC)                  Unassigned */
         _Startup                      /* Int.no.  0 Vreset (at FFFE)                Reset vector */
};

         


/* END MCUinit */

/*
** ###################################################################
**
**     This file was created by Processor Expert 3.07 [04.34]
**     for the Freescale HCS08 series of microcontrollers.
**
** ###################################################################
*/
