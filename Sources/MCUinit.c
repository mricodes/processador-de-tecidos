//===============================================================================================
//	EQUIPAMENTO:Placa LPCPU02
//	EMPRESA: LUPETEC
// Rotinas de Inicializacao da MCU
//===============================================================================================

// MODULE MCUinit 

#include <MC9S08AC32.h>                // I/O map for MC9S08AC32CFG
#include "MCUinit.h"
#include "masterheader.h"

/*
** ===================================================================
**     Method      :  MCU_init (component MC9S08AC60)
**
**     Description :
**         Device initialization code for selected peripherals.
** ===================================================================
*/
void MCU_init(void)
{
 	 	 PTCD = 0b00010100;			  //inicializa a porta
//	      	    ||||||||__BIT 0 = 
//	 		    |||||||___BIT 1 = 
//	 		    ||||||____BIT 2 = AQC11
//	 		    |||||_____BIT 3 = 
//	 		    ||||______BIT 4 = AQC12
//	 		    |||_______BIT 5 = 
//	 		    ||________BIT 6 = 
//	 		    |_________BIT 7 = 

  		 PTED = 0b11111100;			  //inicializa a porta
  //	 		    ||||||||__BIT 0 = ---
  //	 		    |||||||___BIT 1 = ---
  //	 		    ||||||____BIT 2 = AQC21
  //	 		    |||||_____BIT 3 = AQC22
  //	 		    ||||______BIT 4 = AQC31
  //	 		    |||_______BIT 5 = AQC32
  //	 		    ||________BIT 6 = AQC41
  //	 		    |_________BIT 7 = AQC42


  // Common initialization of the write once registers
  // SOPT: COPE=0,COPT=1,STOPE=0
  SOPT = 0x53;                                      
  // SPMSC1: LVDF=0,LVDACK=0,LVDIE=0,LVDRE=1,LVDSE=1,LVDE=1,BGBE=0
  SPMSC1 = 0x1C;                                      
  // SPMSC2: LVWF=0,LVWACK=0,LVDV=0,LVWV=0,PPDF=0,PPDACK=0,PPDC=0
  SPMSC2 = 0x00;                                      
  // SMCLK: MPE=0,MCSEL=0
  SMCLK &= (unsigned char)~0x17;                     
  //  System clock initialization 
  // ICGC1: HGO=0,RANGE=1,REFS=1,CLKS1=1,CLKS0=1,OSCSTEN=1,LOCD=0 
  ICGC1 = 0xFC;                                      
  // ICGC2: LOLRE=0,MFD2=0,MFD1=1,MFD0=1,LOCRE=0,RFD2=0,RFD1=0,RFD0=0
  ICGC2 = 0x30;                                      
  if (*(unsigned char*far)0xFFBE != 0xFF)
  { // Test if the device trim value is stored on the specified address 
    ICGTRM = *(unsigned char*far)0xFFBE; // Initialize ICGTRM register from a non volatile memory 
  }
  while(!ICGS1_LOCK) {                 // Wait
  }
  // Common initialization of the CPU registers 
  // PTASE: PTASE1=0,PTASE0=0 
  PTASE &= (unsigned char)~0x03;                     
  // PTBSE: PTBSE3=0,PTBSE2=0,PTBSE1=0,PTBSE0=0 
  PTBSE &= (unsigned char)~0x0F;                     
  // PTCSE: PTCSE5=0,PTCSE4=0,PTCSE3=0,PTCSE2=1,PTCSE1=1,PTCSE0=1 
  PTCSE = (PTCSE & (unsigned char)~0x38) | (unsigned char)0x07;
  // PTDSE: PTDSE3=0,PTDSE2=0,PTDSE1=0,PTDSE0=0 
  PTDSE &= (unsigned char)~0x0F;                     
  // PTESE: PTESE7=0,PTESE6=0,PTESE5=0,PTESE4=0,PTESE3=0,PTESE2=0,PTESE1=0,PTESE0=0 
  PTESE = 0x00;                                      
  // PTFSE: PTFSE5=0,PTFSE4=0,PTFSE1=0,PTFSE0=0
  PTFSE &= (unsigned char)~0x33;                     
  // PTGSE: PTGSE6=0,PTGSE5=0,PTGSE3=0,PTGSE2=0,PTGSE1=0,PTGSE0=0 
  PTGSE &= (unsigned char)~0x6F;                     
  // PTADS: PTADS7=0,PTADS6=0,PTADS5=0,PTADS4=0,PTADS3=0,PTADS2=0,PTADS1=0,PTADS0=0 
  PTADS = 0x00;                                      
  // PTBDS: PTBDS7=0,PTBDS6=0,PTBDS5=0,PTBDS4=0,PTBDS3=0,PTBDS2=0,PTBDS1=0,PTBDS0=0
  PTBDS = 0x00;                                      
  // PTCDS: PTCDS6=0,PTCDS5=0,PTCDS4=0,PTCDS3=0,PTCDS2=0,PTCDS1=0,PTCDS0=0 
  PTCDS = 0x00;                                      
  // PTDDS: PTDDS7=0,PTDDS6=0,PTDDS5=0,PTDDS4=0,PTDDS3=0,PTDDS2=0,PTDDS1=0,PTDDS0=0 
  PTDDS = 0x00;                                      
  // PTEDS: PTEDS7=0,PTEDS6=0,PTEDS5=0,PTEDS4=0,PTEDS3=0,PTEDS2=0,PTEDS1=0,PTEDS0=0 
  PTEDS = 0x00;                                      
  // PTFDS: PTFDS7=0,PTFDS6=0,PTFDS5=0,PTFDS4=0,PTFDS3=0,PTFDS2=0,PTFDS1=0,PTFDS0=0 
  PTFDS = 0x00;                                      
  // PTGDS: PTGDS6=0,PTGDS5=0,PTGDS4=0,PTGDS3=0,PTGDS2=0,PTGDS1=0,PTGDS0=0
  PTGDS = 0x00;                                      
  
   //PTCD: PTCD2=0 
 
 
//////////////////////////////////
//	 portas paralelas
//	 PTxDD => 1-saida; 0-entrada
//////////////////////////////////

  //PTAD = 0x00;
		PTADD = 0b11111111;
//	 	          ||||||||__BIT 0 = Reset FT232
//	 	          |||||||___BIT 1 = Aciona motor que sobe o cesto
//	 	          ||||||____BIT 2 = Aciona motor que desce o cesto
//	 	          |||||_____BIT 3 = Gira carrossel
//	 	          ||||______BIT 4 = Aciona vibra��o do cesto
//	 	          |||_______BIT 5 = 
//	 	          ||________BIT 6 = 
//	 	          |_________BIT 7 = 
//	PTBD = 0x00;
	
	    	PTBDD = 0b00000111;
//	      	   	    ||||||||__BIT 0 = Drive de saida transistorizado
//	 		    |||||||___BIT 1 = Drive de saida transistorizado
//	 		    ||||||____BIT 2 = Drive de saida transistorizado
//	 		    |||||_____BIT 3 = Medida da corrente dos motores
//	 		    ||||______BIT 4 = Medida de temperatura 1
//	 		    |||_______BIT 5 = Medida de temperatura 2
//	 		    ||________BIT 6 = Medida de temperatura 3
//	 		    |_________BIT 7 = Medida de temperatura 4
//	PTBD = 0x00;

  		PTCDD = 0b11011111;
  //	 		    ||||||||__BIT 0 = ---
  //	 		    |||||||___BIT 1 = ---
  //	 		    ||||||____BIT 2 = Aciona Resist�ncia 1 110/220
  //	 		    |||||_____BIT 3 = TX Disp.
  //	 		    ||||______BIT 4 = Aciona Resist�ncia 1 110
  //	 		    |||_______BIT 5 = 
  //	 		    ||________BIT 6 = ---
  //	 		    |_________BIT 7 = ---
	//PTCD = 0x00;
    
		PTDDD = 0b00000000;
  //	 		    ||||||||__BIT 0 = Fim de curso opto acoplada	 4
  //	 		    |||||||___BIT 1 = 
  //	 		    ||||||____BIT 2 = Fim de curso opto acoplada	 3
  //	 		    |||||_____BIT 3 = Fim de curso opto acoplada	 2
  //	 		    ||||______BIT 4 = Fim de curso opto acoplada	 1
  //	 		    |||_______BIT 5 = Detecta tens�o de entrada
  //	 		    ||________BIT 6 = Entrada digital 1 opto acoplada
  //	 		    |_________BIT 7 = Entrada digital 2 opto acoplada
  //PTDD = 0xFF;

		PTEDD = 0b11111101;
  //	 		    ||||||||__BIT 0 = SCI1 Tx
  //	 		    |||||||___BIT 1 = SCI1 Rx
  //	 		    ||||||____BIT 2 = Aciona Resist�ncia 2 110/220
  //	 		    |||||_____BIT 3 = Aciona Resist�ncia 2 110
  //	 		    ||||______BIT 4 = Aciona Resist�ncia 3 110/220
  //	 		    |||_______BIT 5 = Aciona Resist�ncia 3 110
  //	 		    ||________BIT 6 = Aciona Resist�ncia 4 110/220
  //	 		    |_________BIT 7 = Aciona Resist�ncia 4 110
  //PTED = 0x00;
     
  		PTFDD = 0b11100000;
    //	 	    ||||||||__BIT 0 = Tecla Avan�o
    //	 	    |||||||___BIT 1 = Tecla Retro
    //	 	    ||||||____BIT 2 = Tecla Avan�o rapido
    //	 	    |||||_____BIT 3 = Tecla Retro rapido
    //	 	    ||||______BIT 4 = Fim de curso tampa superior
    //	 	    |||_______BIT 5 = ---
    //	 	    ||________BIT 6 = ---
    //	 	    |_________BIT 7 = ---
    //PTFD = 0x00;  
    
  		PTGDD = 0b11110111;
    //	 	    ||||||||__BIT 0 = xxx
    //	 	    |||||||___BIT 1 = xxx
    //	 	    ||||||____BIT 2 = xxx
    //	 	    |||||_____BIT 3 = ---
    //	 	    ||||______BIT 4 = xxx
    //	 	    |||_______BIT 5 = xxx
    //	 	    ||________BIT 6 = xxx
    //	 	    |_________BIT 7 = xxx
    PTGDD = 0xFF;  
   
  //PTBDD |=   0x07; //PTB0=PTB1=PTB2=saida
  //PTCD &= (unsigned char)~0x04;                     
  //PTDD = 0x00;
  //PTDDD= 0x00;
  // PTCDD: PTCDD2=1,PTCDD1=1,PTCDD0=1
  //PTCDD |= (unsigned char)0x07;                      
  //PTEDD_PTEDD0 = 0x01;
  //PTEDD_PTEDD1 = 0x00;
  
   // TPM1SC: TOF=0,TOIE=0,CPWMS=0,CLKSB=0,CLKSA=0,PS2=0,PS1=0,PS0=0 
  TPM1SC = 0x00;                       // Stop and reset counter 
  TPM1MODL = 0x00;                     // Period value setting 
  TPM1MODH = 0x14;                     // Period value setting 
  (void)(TPM1SC == 0);                 // Overflow int. flag clearing (first part)
  /* TPM1SC: TOF=0,TOIE=1,CPWMS=0,CLKSB=0,CLKSA=1,PS2=0,PS1=0,PS0=0 */
  TPM1SC = 0x55;                       /* Int. flag clearing (2nd part) and timer control register setting */
  
  // TPM2SC: TOF=0,TOIE=0,CPWMS=0,CLKSB=0,CLKSA=0,PS2=0,PS1=0,PS0=0 
  
  //500ms
  TPM2SC = 0x00;                       // Stop and reset counter 
  TPM2MODL = 0x00;                     // Period value setting 
  TPM2MODH = 0x20;
  (void)(TPM2SC == 0);                 // Overflow int. flag clearing (first part)
  // TPM2SC: TOF=0,TOIE=1,CPWMS=0,CLKSB=0,CLKSA=1,PS2=0,PS1=0,PS0=0 
  TPM2SC = 0x57; 

  
 // TPM3SC: TOF=0,TOIE=0,CPWMS=0,CLKSB=0,CLKSA=0,PS2=0,PS1=0,PS0=0 
 // TPM3SC = 0x00;                       // Stop and reset counter 
 // TPM3MOD = 0x2D;                      // Period value setting 
 // (void)(TPM3SC == 0);                 // Overflow int. flag clearing (first part)
  // TPM3SC: TOF=0,TOIE=1,CPWMS=0,CLKSB=0,CLKSA=1,PS2=0,PS1=0,PS0=0 
 // TPM3SC = 0x49; 
      
  /// Init_SCI init code
  
  ///////////////////////////////////////////////////////////////////////////////////////////////
  //Registradores de configuracao serial
  ///////////////////////////////////////////////////////////////////////////////////////////////
  
  ///Serial 1////////////////////////////////////////////////////////////////////////////////////
  //SCI1C2: TIE=0,TCIE=0,RIE=0,ILIE=0,TE=0,RE=0,RWU=0,SBK=0
  SCI1C2 = 0x00;                       // Disable the SCI1 module
  (void)(SCI1S1 == 0);                 // Dummy read of the SCI1S1 register to clear flags
  (void)(SCI1D == 0);                  // Dummy read of the SCI1D register to clear flags
  //SCI1S2: LBKDIF=0,RXEDGIF=0,RXINV=0,RWUID=0,BRK13=0,LBKDE=0,RAF=0
  SCI1S2 = 0x00;                                      
  // SCI1BDH: LBKDIE=0,RXEDGIE=0,SBR12=0,SBR11=0,SBR10=0,SBR9=0,SBR8=0
  SCI1BDH = 0x00;                                      
  // SCI1BDL: SBR7=1,SBR6=0,SBR5=0,SBR4=0,SBR3=0,SBR2=0,SBR1=0,SBR0=1
  //SCI1BDL = 0x0B; //115200 Baud Rate
  //SCI1BDL = 0x65;//19200 Baud Rate  
  SCI1BDL = 0x84;//9600 Baud Rate  
                                        
  // SCI1C1: LOOPS=0,SCISWAI=0,RSRC=0,M=0,WAKE=0,ILT=0,PE=0,PT=0
  SCI1C1 = 0x00;                                      
  // SCI1C3: R8=0,T8=0,TXDIR=0,TXINV=0,ORIE=0,NEIE=0,FEIE=0,PEIE=0
  SCI1C3 = 0x00;                                      
  // SCI1C2: TIE=0,TCIE=0,RIE=0,ILIE=0,TE=1,RE=1,RWU=0,SBK=0
  SCI1C2 = 0x2C; 
  
  ///Serial 2////////////////////////////////////////////////////////////////////////////////////  
  // SCI1C2: TIE=0,TCIE=0,RIE=0,ILIE=0,TE=0,RE=0,RWU=0,SBK=0 
  SCI2C2 = 0x00;                       // Disable the SCI1 module 
  (void)(SCI2S1 == 0);                 // Dummy read of the SCI1S1 register to clear flags
  (void)(SCI2D == 0);                  // Dummy read of the SCI1D register to clear flags
  // SCI1S2: LBKDIF=0,RXEDGIF=0,RXINV=0,RWUID=0,BRK13=0,LBKDE=0,RAF=0 
  SCI2S2 = 0x00;                                      
  // SCI1BDH: LBKDIE=0,RXEDGIE=0,SBR12=0,SBR11=0,SBR10=0,SBR9=0,SBR8=0 
  SCI2BDH = 0x00;                                      
  // SCI1BDL: SBR7=1,SBR6=0,SBR5=0,SBR4=0,SBR3=0,SBR2=0,SBR1=0,SBR0=1 
  SCI2BDL = 0x0B; //115200 Baud Rate
  //SCI2BDL = 0x65;//19200 Baud Rate                                        
  // SCI1C1: LOOPS=0,SCISWAI=0,RSRC=0,M=0,WAKE=0,ILT=0,PE=0,PT=0 
  SCI2C1 = 0x00;                                      
  // SCI1C3: R8=0,T8=0,TXDIR=0,TXINV=0,ORIE=0,NEIE=0,FEIE=0,PEIE=0
  SCI2C3 = 0x00;                                      
  // SCI1C2: TIE=0,TCIE=0,RIE=0,ILIE=0,TE=1,RE=1,RWU=0,SBK=0 
  SCI2C2 = 0x2C;
  ///////////////////////////////////////////////////////////////////////////////////////////////                                     

 
              ADC1SC1 = 0b01000011;
    //	 		  ||||||||__BIT 0 = ADCH
    //	 		  |||||||___BIT 1 = ADCH
    //	 		  ||||||____BIT 2 = ADCH
    //	 		  |||||_____BIT 3 = ADCH
    //	 		  ||||______BIT 4 = ADCH
    //	 		  |||_______BIT 5 = ADCO Continuous Conversion Enable
    //	 		  ||________BIT 6 = AIEN Interrupt Enable
    //	 		  |_________BIT 7 = COCO(conversion complete)
 		  ADC1SC2 = 0b00000000;
    //	 		  ||||||||__BIT 0 = ---
    //	 		  |||||||___BIT 1 = ---
    //	 		  ||||||____BIT 2 = ---
    //	 		  |||||_____BIT 3 = ---
    //	 		  ||||______BIT 4 = ACFGT Compare function greater than Enable
    //	 		  |||_______BIT 5 = ACFE  Compare Function 
    //	 		  ||________BIT 6 = ADTRG Conversion Triger Select (HW ou SW)
    //	 		  |_________BIT 7 = ADACT Conersion in progress
 
 /*   
  ADCRH = 0b00000000   
  //	 		  ||||||||__BIT 0 = ADR8
  //	 		  |||||||___BIT 1 = ADR9
  //	 		  ||||||____BIT 2 = ---
  //	 		  |||||_____BIT 3 = ---
  //	 		  ||||______BIT 4 = ---
  //	 		  |||_______BIT 5 = ---
  //	 		  ||________BIT 6 = ---
  //	 		  |_________BIT 7 = ---
  
  ADCRL = 0b00000000   
  //	 		  ||||||||__BIT 0 = ADR8
  //	 		  |||||||___BIT 1 = ADR8
  //	 		  ||||||____BIT 2 = ADR8
  //	 		  |||||_____BIT 3 = ADR8
  //	 		  ||||______BIT 4 = ACFGT Compare function greater than Enable
  //	 		  |||_______BIT 5 = ACFE  Compare Function 
  //	 		  ||________BIT 6 = ADTRG Conversion Triger Select (HW ou SW)
  //	 		  |_________BIT 7 = ADACT Conersion in progress
  */
    
 	  ADC1CFG = 0b01101001;
  //	 		  ||||||||__BIT 0 = ADICLK Input Clock Select
  //	 		  |||||||___BIT 1 = ADICLK Input Clock Select
  //	 		  ||||||____BIT 2 = MODE Conversion Mode Selection
  //	 		  |||||_____BIT 3 = MODE Conversion Mode Selection
  //	 		  ||||______BIT 4 = ADLSMP Long Sample Time Configuration
  //	 		  |||_______BIT 5 = ADIV Clock Divide Select
  //	 		  ||________BIT 6 = ADIV Clock Divide Select
  //	 		  |_________BIT 7 = ADLPC Low Power Config
  
	   APCTL1 = 0b11111000;
  //	 		  ||||||||__BIT 0 = ADPC0 ADC Pin Control 0 
  //	 		  |||||||___BIT 1 = ADPC1 ADC Pin Control 1 
  //	 		  ||||||____BIT 2 = ADPC2 ADC Pin Control 2 
  //	 		  |||||_____BIT 3 = ADPC3 ADC Pin Control 3 
  //	 		  ||||______BIT 4 = ADPC4 ADC Pin Control 4 
  //	 		  |||_______BIT 5 = ADPC5 ADC Pin Control 5 
  //	 		  ||________BIT 6 = ADPC6 ADC Pin Control 6 
  //	 		  |_________BIT 7 = ADPC7 ADC Pin Control 7 

// (void)(ADC1RH == 0);             // Dummy read 
// (void)(ADC1RL == 0);
  
  asm CLI;                             // Enable interrupts 
} //MCU_init


