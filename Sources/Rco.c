/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU02
/	EMPRESA: LUPETEC
/ Rotinas de Comunica��o com Operador
===============================================================================================*/
#include "masterheader.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// 									 ////////////////////////
////////////////    Maquina de estados Rotina Comunica��o com Operador     //////////////////////
//////////////////                                                       ////////////////////////                                                             
/////////////////////////////////////////////////////////////////////////////////////////////////


void Rco_MRI(void)
{
                   
////////////////Estado de introdu��o///////////////////
  if(EstadoRco == ESTA_INTRO)
  {
      CarregaTela(TELA_INTRO);
      if(LeTouch())
      {
      
        teclaDisp = 0xFF;  
      }
      
      if(DelTela > 20)
      {
        EstadoRco = ESTA_HOME;
        EstadoRcoAnt = EstadoRco;
        DelTela = 0;
      }

  }
////////////////Estado da tela home///////////////////
  else if(EstadoRco==ESTA_HOME)
  {
      CarregaTela(TELA_HOME);
      if(LeTouch() && teclaDisp != 0xFF)
      {
          if(teclaDisp == BOT_MANUAL)
          {
              EstadoRcoAnt = EstadoRco;
              EstadoRco = ESTA_MANUAL;
          } 
          else if(teclaDisp == BOT_PRESET)
          {
              EstadoRcoAnt = EstadoRco;
              EstadoRco=ESTA_PRESET;
            
          }
          else if(teclaDisp == BOT_INIT_CICLO)
          {
              EstadoRcoAnt = EstadoRco;
              EstadoRco = ESTA_SEL_CICLO;
          }
          else if(teclaDisp == BOT_SOBRE)//////PARA FEIRA ABOUT LIMPA ERRO DE MOVIMENTO  BAIXA
          {
              alarSobe = alarDesce = alarGiro = acetAlarSobe = acetAlarDesce = acetAlarGiro =0;
              TempoSubindo =  TempoDescendo = TempoGirando = 0xFF;
              acetAlarTempBaixa[1] = 0;
              alarTempBaixa[1] = 0;
              acetAlarTempBaixa[2] = 0;
              alarTempBaixa[2] = 0;
              acetAlarTempBaixa[3] = 0;
              alarTempBaixa[3] = 0;
          }
          teclaDisp = 0xFF;  
      } 
      //Atualiza dados do display
      if(DelTela > 1)
      {
        DelTela = 0;
        AtualizaAlarme = 1;
        AtuCamposExeCiclo();
        AtuCampoAlarmes(); 
        AtuBarraInfo();
        //ChecaPopUPAceita(EstadoRco);
      }
  }
////////////////Estado da tela manual///////////////////
  else if(EstadoRco == ESTA_MANUAL)
  {
      CarregaTela(TELA_MANUAL);
      if(LeTouch() && teclaDisp != 0xFF)
      {
          if(teclaDisp == BOT_VOLTAR)
          {
              if(EstadoCiclo == ESTA_CICLO_PAUSADO)
              {
                   Proc_Ret_Ciclo(1);
                   EstadoRco = ESTA_EXE_CICLO; 
              }
              else{EstadoRco = ESTA_HOME;}              
          } 
          else if(teclaDisp == BOT_SOBE)
          {
              Proc_Subida();            
          }
          else if(teclaDisp == BOT_DESCE)
          {
              Proc_Descida();            
          }
          else if(teclaDisp == BOT_GIRA)
          {
              Proc_Gira();           
          }
          else if(teclaDisp == BOT_ZERAR)
          {
             Proc_Zerar();           
          }
          else if(teclaDisp == BOT_HOME)
          {
              if(EstadoCiclo == ESTA_CICLO_PAUSADO)
              {
                   Proc_Ret_Ciclo(1);
                   EstadoRco = ESTA_EXE_CICLO; 
              }
              else{EstadoRco = ESTA_HOME;} 
          }
          TempoRetPause = 0;
          teclaDisp = 0xFF;  
      }
      //Atualiza dados do display
      if(DelTela > 1)
      {
     
         AtuBarraInfo();
         AtualizaAlarme = 1;
         AtuCampoAlarmes();
         AtuCamposExeCiclo();
         AtualizaCampo(POS_CANECA_DISP , posCaneca); 
         ChecaPopUPAceita(EstadoRco);
         DelTela = 0;              
      } 
  }
////////////////Estado seleciona ciclo///////////////////
  else if(EstadoRco==ESTA_SEL_CICLO)
  {
      CarregaTela(TELA_SEL_CICLO);
      if(LeTouch() && teclaDisp != 0xFF)
      {
          if ((teclaDisp > 27) && (teclaDisp < 48)) //programa 1 a 20
          {
              NumCicloTemp = teclaDisp - 28;
              EstadoRcoAnt = EstadoRco;
              EstadoRco = ESTA_TIPO_CICLO;
          }
          else if(teclaDisp == BOT_VOLTAR)
          {
              EstadoRco = ESTA_HOME;
          } 
          else if(teclaDisp == BOT_HOME)
          {
              EstadoRco = ESTA_HOME;            
          }        
          teclaDisp = 0xFF;  
      }
      //Atualiza dados do display
      if(DelTela > 2)
      {
          DelTela = 0;
          AtuCamposExeCiclo();
          AtuCampoAlarmes();
          ChecaPopUPAceita(EstadoRco);
      }
    
  }
  ////////////////Estado seleciona tipo do ciclo///////////////////
  else if(EstadoRco == ESTA_TIPO_CICLO)
  {
      CarregaPopUp(POP_TIPO_CICLO + off_set_idioma);
      if(LeTouch() && teclaDisp != 0xFF)
      {
          if(teclaDisp == BOT_CANCELA_POP)
          {
              EstadoRco = EstadoRcoAnt;
          } 
          else if(teclaDisp == BOT_ATRASO_POP)
          {
               SubEstadoRco = 0;
               EstadoRco = ESTA_DEF_RETARDO;
          } 
          else if(teclaDisp == BOT_IMEDIATO_POP)
          {
               Proc_Init_Ciclo(1);
               EstadoRco = ESTA_EXE_CICLO;
          }
          auxPopAtual = 0;
         // CarregaPopUp(0x0D);      
          teclaDisp = 0xFF;   
  
      }
      //Atualiza dados do display
      if(DelTela > 2)
      {
           DelTela = 0;
           AtualizaAlarme = 1;
           AtuBarraInfo();
           AtuCamposExeCiclo();
           AtuCampoAlarmes();
      }    
  }
  ////////////////Estado executando ciclo///////////////////
  else if(EstadoRco==ESTA_EXE_CICLO)
  {
      CarregaTela(TELA_EXE_CICLO);
      if(LeTouch() && teclaDisp != 0xFF || pausaCicloAlarme == 1)
      {
          if(teclaDisp == BOT_MANUAL)
          {
            
            
          } 
          else if(teclaDisp == BOT_PRESET)
          {
            
            
          }
          else if(teclaDisp == BOT_INIT_CICLO)
          {
            
            
          }
          else if(teclaDisp == BOT_VOLTAR)
          {
              Proc_Pausa_Ciclo(1);
              EstadoRco = ESTA_PARA_CICLO;
          } 
          else if(teclaDisp == BOT_HOME)
          {
              Proc_Pausa_Ciclo(1);
              EstadoRco = ESTA_PARA_CICLO;
          }
          else if(teclaDisp == BOT_PAUSAR || pausaCicloAlarme == 1)
          {
              Proc_Pausa_Ciclo(1);
              pausaCicloAlarme = 0;
              EstadoRco = ESTA_PARA_CICLO;
          }        
          teclaDisp = 0xFF;  
      } 
      if(EstadoCiclo == ESTA_CICLO_FIM)
      {
          EstadoRco = ESTA_HOME;                    
      }
      //Atualiza dados do display
      if(DelTela > 2)
      {
        AtuCamposExeCiclo();
        AtualizaAlarme = 1;
        AtuCampoAlarmes();
        AtuBarraInfo();
        ChecaPopUPAceita(EstadoRco);
        DelTela = 0;
      }
    
  }
   ////////////////Estado Seleciona Acao Programa pausado///////////////////
  else if(EstadoRco == ESTA_PARA_CICLO)
  {   
      CarregaPopUp(POP_PARA_CICLO + off_set_idioma);
      if(LeTouch() && teclaDisp != 0xFF)
      {                    
          if(teclaDisp == BOT_ABORTAR_POP)
          {
              EstadoCiclo = ESTA_CICLO_FIM;
              EstadoRco = ESTA_HOME;
          } 
          else if(teclaDisp == BOT_RETOMAR_POP)
          {
              Proc_Ret_Ciclo(1);
              EstadoRco = ESTA_EXE_CICLO;
          } 
          else if(teclaDisp == BOT_MANUAL_POP)
          {
              //Retorna Automatico depois de um tempo
              EstadoRco = ESTA_MANUAL;
          }
          auxPopAtual = 0;
          //CarregaPopUp(0x0D);      
          teclaDisp = 0xFF;   
      } 
      //Atualiza dados do display
      if(DelTela > 2)
      {
          DelTela = 0;
          AtuCamposExeCiclo();
      }   
  }
  ////////////////Estado da tela preset///////////////////
  else if(EstadoRco==ESTA_PRESET)
  {
     CarregaTela(TELA_PRESET);
      if(LeTouch() && teclaDisp != 0xFF)
      {
          if(teclaDisp == BOT_FABRICA)
          {
            //Faz ESP voltar para AP
            EnviaComandoNuvem(CMD_NUVEM_AP);
            
          } 
          else if(teclaDisp == BOT_AJU_TEMP)
          {
             EstadoRco = ESTA_AJU_TEMP;
            
          }
          else if(teclaDisp == BOT_INIT_CICLO)
          {
            
            
          } 
          else if(teclaDisp == BOT_TESTE_MANU)
          {
             EstadoRco = ESTA_TESTE_MANU; 
            
          } 
          else if(teclaDisp == BOT_VOLTAR)
          {
              EstadoRco = ESTA_HOME;              
          }
          else if(teclaDisp == BOT_HOME)
          {
            EstadoRco = ESTA_HOME; 
          }
     
          teclaDisp = 0xFF;  
      }
      //Atualiza dados do display
      if(DelTela > 2)
      {
          DelTela = 0;
          AtuCamposExeCiclo();
          AtuCampoAlarmes();
      }    
  }
////////////////Estado com alarme ativo///////////////////
  else if(EstadoRco == ESTA_ALARME_ATIVO)
  {
      if(LeTouch() && teclaDisp != 0xFF)
      {  
          if(teclaDisp == BOT_OK_POP)
          {
              if(numAlarmes>0)
              {
                pausaCicloAlarme = 1; 
              }
              EstadoRco = EstadoRcoAnt;
          }      
          if(teclaDisp == BOT_MANUAL)
          {
              
              EstadoRco = ESTA_MANUAL;
              EstadoRcoAnt = EstadoRco;
          }
          if(teclaDisp == BOT_PRESET)
          {
              
              EstadoRco = ESTA_HOME;
              EstadoRcoAnt = EstadoRco;
          }
          if(teclaDisp == BOT_INIT_CICLO)
          {
              EstadoRco = ESTA_SEL_CICLO;
              EstadoRcoAnt = EstadoRco;              
          } 
          if(teclaDisp == BOT_VOLTAR)
          {
              EstadoRco = ESTA_HOME;
          } 
          if(teclaDisp == BOT_HOME)
          {
              
              EstadoRco = ESTA_HOME;
          }
          teclaDisp = 0xFF;   
      }
      //Atualiza dados do display
      if(DelTela > 2)
      {
          DelTela = 0;
          AtualizaAlarme = 1;
          
          AtuCampoAlarmes();
          AtuCamposExeCiclo();
      }    
  }
  ////////////////Estado solicita senha///////////////////
  else if(EstadoRco==ESTA_SOL_SENHA)
  {
    
    
  }
  ////////////////Estado Ajusta Set-Point///////////////////
  else if(EstadoRco == ESTA_AJU_TEMP)
  {
      CarregaTela(TELA_AJU_TEMP);
      if(LeTouch() && teclaDisp != 0xFF)
      {  
          
          
          if(teclaDisp == BOT_VOLTAR)
          {
             
              EstadoRco = ESTA_PRESET;
          } 
          else if(teclaDisp == BOT_HOME)
          {
              
              EstadoRco = ESTA_HOME;
          } 
          else if (teclaDisp == BOT_DEC_T1) 
          {
            temperatura_ajustada_T1 = temperatura_ajustada_T1 - 10;
          } 
          else if (teclaDisp == BOT_INC_T1)
          {
            temperatura_ajustada_T1 = temperatura_ajustada_T1 + 10;
          } 
          else if(teclaDisp == BOT_DEC_T2) 
          {
            temperatura_ajustada_T2 = temperatura_ajustada_T2 - 10;
          } 
          else if(teclaDisp == BOT_INC_T2) 
          {
            temperatura_ajustada_T2 = temperatura_ajustada_T2 + 10;
          } 
          else if (teclaDisp == BOT_DEC_T3) 
          {
            temperatura_ajustada_T3 = temperatura_ajustada_T3 - 10;
          } 
          else if (teclaDisp == BOT_INC_T3) 
          {
            temperatura_ajustada_T3 = temperatura_ajustada_T3 + 10;
          } 
          else if(teclaDisp == BOT_ENTER_RET)
          {
            AtualizaFlash_UP();
          }
          teclaDisp = 0xFF;  
      }

      //Atualiza dados do display
      if(DelTela > 0)
      {
          DelTela = 0;
          AtualizaAlarme = 1;
          AtualizaCampo(CAMPO_AJU_T1, temperatura_ajustada_T1);
          AtualizaCampo(CAMPO_AJU_T2, temperatura_ajustada_T2);
          AtualizaCampo(CAMPO_AJU_T3, temperatura_ajustada_T3);
          AtuBarraInfo();
          AtuCampoAlarmes();
          AtuCamposExeCiclo();
      }    
  }
  ////////////////Estado Teste e Manuten��o /////////////////// 
  else if (EstadoRco == ESTA_TESTE_MANU)  
  {
      CarregaTela(TELA_TESTE_MANU);
      if(LeTouch() && teclaDisp != 0xFF)
      {     
          if(teclaDisp == BOT_VOLTAR)
          {
             EstadoRco = ESTA_PRESET;
          }
          teclaDisp = 0xFF;  
      }
      //Atualiza dados do display
      if(DelTela > 0)
      {
          DelTela = 0;
          AtualizaAlarme = 1;
          AtualizaCampo(CAMPO_ADC_T1, valorADC[1]);
          AtualizaCampo(CAMPO_ADC_T2, valorADC[2]);
          AtualizaCampo(CAMPO_ADC_T3, valorADC[3]);
          AtuBarraInfo();
          AtuCampoAlarmes();
      }
  }
  ////////////////Estado Ajusta Retardo///////////////////
  else if(EstadoRco == ESTA_DEF_RETARDO)
  {
      CarregaTela(TELA_DEF_RETARDO);
      if(SubEstadoRco == 0)
      {
        LeRtc();
        dia_execucao = diaRtc;
        ano_execucao = anoRtc;  
        mes_execucao = mesRtc;   
        dia_execucao = diaRtc;  
        hora_execucao = horaRtc; 
        minutos_execucao = minRtc;
        SubEstadoRco = 1; 
      }
     
      if(LeTouch() && teclaDisp != 0xFF)
      {  
          
          if(teclaDisp == BOT_VOLTAR)
          {
              EstadoRco = ESTA_SEL_CICLO;
          } 
          else if(teclaDisp == BOT_HOME)
          {
              
              EstadoRco = ESTA_HOME;
          } 
          else if (teclaDisp == BOT_DEC_DIA_RET) 
          {
            if(dia_execucao > 1)
              dia_execucao = dia_execucao - 1;
            else
              dia_execucao = 31;
          } 
          else if (teclaDisp == BOT_INC_DIA_RET)
          {
            if(dia_execucao < 31)
              dia_execucao = dia_execucao + 1;
            else
              dia_execucao = 1;
          } 
          else if(teclaDisp == BOT_DEC_MES_RET) 
          {
            if(mes_execucao > 1)
                mes_execucao = mes_execucao - 1;
            else
                mes_execucao = 12;
          } 
          else if(teclaDisp == BOT_INC_MES_RET) 
          {
            if(mes_execucao < 12)
                mes_execucao = mes_execucao + 1;
            else
                mes_execucao = 1;
          } 
          else if (teclaDisp == BOT_DEC_ANO_RET) 
          {
            if(ano_execucao > 19)
              ano_execucao = ano_execucao - 1;
          } 
          else if (teclaDisp == BOT_INC_ANO_RET) 
          {
            if(ano_execucao < 90)
                ano_execucao = ano_execucao + 1;              
          }
          else if (teclaDisp == BOT_DEC_HORA_RET) 
          {
            if(hora_execucao > 0)
               hora_execucao = hora_execucao - 1;
            else
               hora_execucao = 23;
          } 
          else if (teclaDisp == BOT_INC_HORA_RET) 
          {
            if(hora_execucao < 23)
              hora_execucao = hora_execucao + 1;
            else
              hora_execucao = 0;  
          }
          else if (teclaDisp == BOT_DEC_MIN_RET) 
          {
            if(minutos_execucao > 0)
              minutos_execucao = minutos_execucao - 1;
            else
              minutos_execucao = 59;
          } 
          else if (teclaDisp == BOT_INC_MIN_RET) 
          {
            if(minutos_execucao < 58)            
              minutos_execucao = minutos_execucao + 1;
            else
              minutos_execucao = 0;
          } 
          else if(teclaDisp == BOT_ENTER_RET)
          {
               Proc_Init_Ciclo(2);
               EstadoRco = ESTA_EXE_CICLO;  
          }
          teclaDisp = 0xFF;  
      }

      //Atualiza dados do display
      if(DelTela > 0)
      {
          DelTela = 0;
          AtualizaAlarme = 1;
          AtualizaCampo(CAMPO_AJU_ANO_RET, ano_execucao);
          AtualizaCampo(CAMPO_AJU_MES_RET, mes_execucao);
          AtualizaCampo(CAMPO_AJU_DIA_RET, dia_execucao);
          AtualizaCampo(CAMPO_AJU_HORA_RET, hora_execucao);
          AtualizaCampo(CAMPO_AJU_MIN_RET, minutos_execucao);
           AtuBarraInfo();
          AtuCampoAlarmes();
          AtuCamposExeCiclo();
      }    
  }   
  
  
}

void Rco(void) 
{ 
}
void confere_senha() {
  grau_da_senha = 0;
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // 1 2 3 4   SENHA USU�RIO	     	grau_da_senha 4												   //
  // 5 6 7 8   SENHA FABRICANTE 	grau_da_senha 8												   //
  //																				   //
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //           SENHA MASTER		grau_da_senha 12													   //
  //																				   //
  // somar semente com 7 5 3 3 algarismo por algarismo, caso o resultado seja maior que 10, considera-se somente a unidade.  //
  //																				   //
  //   exemplo:       semente:      8   4    8    3												   //
  //			  constante:  + 7   5    3    2  												   //
  //					  --------------------												   //
  //					   15	  9	11    5												   //
  //																				   //
  //					   -----------------												   //
  //              senha valida:	  ( 5	  9	 1    5 )												   //
  //					   -----------------												   //
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if (senha_usuario[3] == contra_senha[3]) grau_da_senha = 1;
  else grau_da_senha = 0;
  if (senha_usuario[2] == contra_senha[2]) grau_da_senha++;
  else grau_da_senha = 0;
  if (senha_usuario[1] == contra_senha[1]) grau_da_senha++;
  else grau_da_senha = 0;
  if (senha_usuario[0] == contra_senha[0]) grau_da_senha++;
  else grau_da_senha = 0;
  if (grau_da_senha == 4) {
    if (off_set_idioma == 0) escreve_texto(" Digite a senha                                        ", 0X01, 0X12);
    if (off_set_idioma == 1) escreve_texto(" Escriba la contrasena                                 ", 0X01, 0X12);
    if (off_set_idioma == 2) escreve_texto(" Type the password                                     ", 0X01, 0X12);
    return;
  }
  if (senha_fab[3] == contra_senha[3]) grau_da_senha = 5;
  else grau_da_senha = 0;
  if (senha_fab[2] == contra_senha[2]) grau_da_senha++;
  else grau_da_senha = 0;
  if (senha_fab[1] == contra_senha[1]) grau_da_senha++;
  else grau_da_senha = 0;
  if (senha_fab[0] == contra_senha[0]) grau_da_senha++;
  else grau_da_senha = 0;
  if (grau_da_senha == 8) {
    if (off_set_idioma == 0) escreve_texto(" Digite a senha                                        ", 0X01, 0X12);
    if (off_set_idioma == 1) escreve_texto(" Escriba la contrasena                                 ", 0X01, 0X12);
    if (off_set_idioma == 2) escreve_texto(" Type the password                                     ", 0X01, 0X12);
    return;
  }
  if (senha_master[3] == contra_senha[3]) grau_da_senha = 9;
  else grau_da_senha = 0;
  if (senha_master[2] == contra_senha[2]) grau_da_senha++;
  else grau_da_senha = 0;
  if (senha_master[1] == contra_senha[1]) grau_da_senha++;
  else grau_da_senha = 0;
  if (senha_master[0] == contra_senha[0]) grau_da_senha++;
  else grau_da_senha = 0;
  if (grau_da_senha == 12) {
    if (off_set_idioma == 0) escreve_texto(" Digite a senha                                        ", 0X01, 0X12);
    if (off_set_idioma == 1) escreve_texto(" Escriba la contrasena                                 ", 0X01, 0X12);
    if (off_set_idioma == 2) escreve_texto(" Type the password                                     ", 0X01, 0X12);
    return;
  }
  if (off_set_idioma == 0) escreve_texto(" Senha incorreta, digite novamente                          ", 0X01, 0X12);
  if (off_set_idioma == 1) escreve_texto(" Contrasena incorrecta, vuelva a escribir                   ", 0X01, 0X12);
  if (off_set_idioma == 2) escreve_texto(" Wrong password, retype                                     ", 0X01, 0X12);
  grau_da_senha = 0;
}

void destacador() /////////////////////////////////////////            destaca o testo a ser editado     /////////////////////
{
  indexadorPP = (index_parametro * 20) + 21583;
  AtualizaCampo(indexadorPP, 0XF800);
  AtualizaCampo(indexadorPPold, 0X0000);
  indexadorPPold = indexadorPP;
  AtualizaCampo(0X0E, index_parametro + 1);
  AtualizaCampo(0XCC, array_parametros[index_parametro][Numero_programa_edit]);
}

void elimina_campo() {
  if (primeira_caneca_de_parafina == 12) {
    AtualizaCampo(5703, 0XFFFF);
    AtualizaCampo(5783, 0XFFFF);
    AtualizaCampo(5863, 0XFFFF);
    AtualizaCampo(5723, 0XFFFF);
    AtualizaCampo(5803, 0XFFFF);
    AtualizaCampo(5883, 0XFFFF);
    AtualizaCampo(5743, 0X0000);
    AtualizaCampo(5823, 0X0000);
    AtualizaCampo(5903, 0X0000);
    AtualizaCampo(5763, 0XFFFF);
    AtualizaCampo(5843, 0XFFFF);
    AtualizaCampo(5923, 0XFFFF);
  } else if (primeira_caneca_de_parafina == 11) {
    AtualizaCampo(5703, 0XFFFF);
    AtualizaCampo(5783, 0XFFFF);
    AtualizaCampo(5863, 0XFFFF);
    AtualizaCampo(5723, 0X0000);
    AtualizaCampo(5803, 0X0000);
    AtualizaCampo(5883, 0X0000);
    AtualizaCampo(5743, 0X0000);
    AtualizaCampo(5823, 0X0000);
    AtualizaCampo(5903, 0X0000);
    AtualizaCampo(5763, 0XFFFF);
    AtualizaCampo(5843, 0XFFFF);
    AtualizaCampo(5923, 0XFFFF);
  } else if (primeira_caneca_de_parafina == 10) {
    AtualizaCampo(5703, 0X0000);
    AtualizaCampo(5783, 0X0000);
    AtualizaCampo(5863, 0X0000);
    AtualizaCampo(5723, 0X0000);
    AtualizaCampo(5803, 0X0000);
    AtualizaCampo(5883, 0X0000);
    AtualizaCampo(5743, 0X0000);
    AtualizaCampo(5823, 0X0000);
    AtualizaCampo(5903, 0X0000);
    AtualizaCampo(5763, 0XFFFF);
    AtualizaCampo(5843, 0XFFFF);
    AtualizaCampo(5923, 0XFFFF);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Delay                                                      
/////////////////////////////////////////////////////////////////////////////////////////////
char Delay(char tempo) {
  if (flagDelay) {
    flagDelay = 0;
    if (contDelay >= tempo) {
      contDelay = 0;
      if (contDelay1 >= tempo) {
        contDelay1 = 0;
        return 1;
      } else {
        contDelay1++;
      }
    } else {
      contDelay++;
      return 0;
    }
  }
  return 0;
}