/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
/ Rotinas de comunica��o serial
===============================================================================================*/
#include "masterheader.h" 

void InitSci(void) {

}
///////////////////////////////////////////////////////
//Retorna no vetor posicoes os indices do separador
///////////////////////////////////////////////////////
unsigned char SplitString(unsigned char Nchar,unsigned char *string, char *posicoes,char separador,unsigned char ini)
{
	char i,j=ini;

	for(i=ini-1;i<Nchar;i++)
	{
		if(*string ==separador)
		{
			*posicoes = i;
			posicoes++;
			j++;
		}
		string++;
	}
	return j;
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Enviar caracter serial 1 - PC                                                            
/////////////////////////////////////////////////////////////////////////////////////////////

void TxChar1(char c)
{
  while(SCI1S1_TDRE==0){};
  SCI1D=c;

}


/////////////////////////////////////////////////////////////////////////////////////////////
// Enviar caracter serial 2  - DWIN                                                            
/////////////////////////////////////////////////////////////////////////////////////////////

void TxChar2(char c)
{
  while(SCI2S1_TDRE==0){};
  SCI2D=c;
}


/////////////////////////////////////////////////////////////////////////////////////////////
// Leitura dos dados da serial  1 - PC
/////////////////////////////////////////////////////////////////////////////////////////////

char LeDadoRx1(){
    if (PtrSai1!=PtrEnt1)
    {
        PtrSai1++;
        if (PtrSai1 >= MaxBufRx)
          {PtrSai1 = 0;}
        return (1); // chegou dado
        
     }
     else 
          {return (0);} // n�o chegou dado    
}
   
/////////////////////////////////////////////////////////////////////////////////////////////
// Leitura dos dados da serial 2 - DWIN
/////////////////////////////////////////////////////////////////////////////////////////////

char LeDadoRx2(){     
      if (PtrSai2!=PtrEnt2)
      {
        PtrSai2++;
        if (PtrSai2 >= MaxBufRx)
          {PtrSai2 = 0;}
        return (1); // chegou dado
        
      }
      else 
          {return (0);} // n�o chegou dado
}


////////////////////////////////////////////////////////////////////////////////////////////////
//Trata protocolo do display
////////////////////////////////////////////////////////////////////////////////////////////////

void TrataProtocolo(void) {
//Estado 0////////////////////////////////////////////////////////////////////////////////////
   if(EstSerial == 0){
      if(LeDadoRx2()){ 
        if(BufRx2[PtrSai2] == 0xA5)   
            {EstSerial++;}  
        else
            {PtrEnt2=PtrSai2=0;}
      }
   }
//Estado 1////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 1){
     if(LeDadoRx2()){
         if(BufRx2[PtrSai2] == 0x5A)   
              {EstSerial++;}  
          else
              {EstSerial=PtrEnt2=PtrSai2=0;}
     }
   }
//Estado 2////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 2){
     if(LeDadoRx2()){
         numBytes = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
             EstSerial++;
         }
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
     }
   }
//Estado 3////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 3){
     if(LeDadoRx2()){
     comando = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
             EstSerial++;
         }
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }
//Estado 4////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 4){
     if(LeDadoRx2()){
       endRamAlto = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
            EstSerial++;}
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }
//Estado 5////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 5){
     if(LeDadoRx2()){
     endRamBaixo = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
            EstSerial++;}
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }
//Estado 6////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 6){
     if(LeDadoRx2()){
      valorLido = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
            EstSerial++;
         }
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }
//Estado 7////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 7){
     if(LeDadoRx2()){
         valorLido0 = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
            EstSerial++;}
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }    
//Estado 8////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 8){
     if(LeDadoRx2()){
         valorLido1 = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
           EstSerial++;
           //EstSerial = 0;
           }
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }
   
 //Estado 9////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 9){
     if(LeDadoRx2()){
     valorLido2 = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
            EstSerial++;}
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }
//Estado 10////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 10){
     if(LeDadoRx2()){
      valorLido3 = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
            EstSerial++;
         }
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }
//Estado 11////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 11){
     if(LeDadoRx2()){
         valorLido4 = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
            EstSerial++;}
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }    
//Estado 12////////////////////////////////////////////////////////////////////////////////////
   else if(EstSerial == 12){
     if(LeDadoRx2()){
         valorLido5 = BufRx2[PtrSai2];
         if(EstSerial < (numBytes + 2)){
           //EstSerial++;
           EstSerial = 0;}
         else{
            EstSerial = 0;
            PtrEnt2=PtrSai2=0;
         }
      }
   }  
}
////////////////////////////////////////////////////////////////////////////////////////////  

/////////////////////////////////////////////////////////////////////////////////////////////
// Ponte Serial - copia tudo da serial 1 para serial 2 e vice versa
/////////////////////////////////////////////////////////////////////////////////////////////

void PonteSerial(void){
     if(LeDadoRx1()){
        //TxChar2(BufRx1[PtrSai1]);
     }
     if(LeDadoRx2()){
      //  TxChar1(BufRx2[PtrSai2]);
   }  
}


///////////////////////////////////////////////////
//  Funcao que envia resposta
//
//////////////////////////////////////////////////////
void EnviaResposta(void)
{
	   unsigned char i;
     TxChar1('#');
     TxChar1('8');
     TxChar1('|');
     
	   for(i = 0;i < TamResp; i++)
	   {
		   TxChar1(Dado2Resp[i]);
	   }
	   TamResp = 0;
	   TxChar1('$');
	   TxChar1('\r');
	   TxChar1('\n');   
}
////////////////////////////////////////////////
//Formata o vetor de resposta
// a cada chamada enfileira um novo dado
// Nchar numero de bytes a ser enfileirado
/////////////////////////////////////////////////
void FormataResp(unsigned char Nchar, char *dado,char separador)
{
	char i=0;

	for(i=TamResp;i<=(TamResp+Nchar);i++)
	{
		
		Dado2Resp[i] = *dado;
		if(Dado2Resp[i] == 0x00){
      Dado2Resp[i]=' ';		  
		}
		dado++;
	}
	TamResp = TamResp + Nchar;

	Dado2Resp[TamResp]=separador;
	TamResp++;
}
///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
///////////////////////////////////////INICIO DEMO///////////////////////
/////////////////////////REMOVER APOS ATUALIZACAO////////////////////////
////////////
//
//Interpreta com base em:
// OLD DEMO
void FormatFaseMov_DEMO(void)
{
    if(Carrossel_sobe) {FormataResp(2, "10",'|');}//subindo
    else if(Carrossel_desce){FormataResp(2, "20",'|');}//descendo
    else if(Carrossel_gira) {FormataResp(2, "30",'|');}//girando
    else if(Carrossel_agita){FormataResp(2, "40",'|');}//agitando
    else{{FormataResp(2, "50",'|');}} //Parado
}

// Le Status de execu�ao do ciclo
// OLD DEMO
//
void FormatStaCiclo_DEMO(void)
{
    unsigned char ret,i;
    ret = 0;
    if(STATUS_EXECUCAO == 0){FormataResp(2, "70",'|');}//Parado
    else if(STATUS_EXECUCAO == 1){FormataResp(2, "71",'|');}//Pausado
    else if(STATUS_EXECUCAO == 2){FormataResp(2, "72",'|');}//Aguardando retardo
    else if(STATUS_EXECUCAO == 3)//Rodando programa
    {
      //ConverteAscii(Numero_programa+1);
      ConverteAscii(NumCiclo);
      FormataResp(4, Msg,'|'); 
    }
    else
    {
      FormataResp(2, "70",'|');
    }
     FormataResp(2, "T1",'|');
      ConverteAscii(alarTempBaixa[1]);
      FormataResp(4, Msg,'|');
      ConverteAscii(alarTempAlta[1]);
      FormataResp(4, Msg,'|');
      ConverteAscii(alarTempSensor[1]);
      FormataResp(4, Msg,'|');
     FormataResp(2, "T2",'|'); 
      ConverteAscii(alarTempBaixa[2]);
      FormataResp(4, Msg,'|');
      ConverteAscii(alarTempAlta[2]);
      FormataResp(4, Msg,'|');
      ConverteAscii(alarTempSensor[2]); 
      FormataResp(4, Msg,'|');
     FormataResp(2, "T3",'|'); 
      ConverteAscii(alarTempBaixa[3]);
      FormataResp(4, Msg,'|');
      ConverteAscii(alarTempAlta[3]);
      FormataResp(4, Msg,'|');
      ConverteAscii(alarTempSensor[3]); 
      FormataResp(4, Msg,'|');
     FormataResp(3, "MOV",'|'); 
      ConverteAscii( alarSobe);
      FormataResp(4, Msg,'|');
      ConverteAscii( alarDesce);
      FormataResp(4, Msg,'|');
      ConverteAscii( alarGiro);
      FormataResp(4, Msg,'|');
      /*
      ConverteAscii( numBytes);//alarSensor);
      FormataResp(4, Msg,'|');
      ConverteAscii( anoRtc);//alarSensor);
      FormataResp(4, Msg,'|');
      ConverteAscii( mesRtc);//alarSensor);
      FormataResp(4, Msg,'|');
      ConverteAscii( diaRtc);//alarSensor);
      FormataResp(4, Msg,'|');
      ConverteAscii( horaRtc);//alarSensor);
      FormataResp(4, Msg,'|');
      ConverteAscii( minRtc);//alarSensor);
      FormataResp(4, Msg,'|');
      ConverteAscii( segRtc);//alarSensor);
      FormataResp(4, Msg,'|');
      */

      
       
}
//
// Formata temperatura para envio
// OLD DEMO
void FormatTemp_DEMO(unsigned int temp)
{
    if(temp < 999)
    {
      ConverteAscii(temp);
      Msg[0] = Msg[1];
      Msg[1] = Msg[2];
      Msg[2] = '.';
      FormataResp(4, Msg,'|');  
    }
    else{FormataResp(4, "99.9",'|');}
}


/////////////////////////////////////////////////////////////////////////////////////////////
// Envia dados para sistema nuvem
//OLD DEMO
/////////////////////////////////////////////////////////////////////////////////////////////
void EnviaDataNuvem_DEMO(void)
{   
     FormatStaCiclo_DEMO();//Ciclo
     ConverteAscii(posicao_carrossel);//Caneca
     FormataResp(4, Msg,'|');
    // FormatFaseMov_DEMO();
     ConverteAscii(Tempo_de_sub_estagio);//Tempo
     FormataResp(4, Msg,'|');
     FormataResp(2, "DU",'|');//DUMMY
     FormatTemp_DEMO(valorADC[1] + off_set_temperatura1);
     FormatTemp_DEMO(valorADC[2] + off_set_temperatura2);
     FormatTemp_DEMO(valorADC[3] + off_set_temperatura3);      
    
     EnviaResposta(); 
}
///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
///////////////////////////////////////FIM DEMO///////////////////////
/////////////////////////REMOVER APOS ATUALIZACAO////////////////////////
///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//
//Interpreta com base em:
//   Carrossel_sobe;Carrossel_desce;Carrossel_gira;Carrossel_agita;STATUS_EXECUCAO
//Qual a fase de movimento
//Retorna (1.subindo,2.descendo,3.girando,4.agitando,5.parado,6.aguardando retardo)
void FormatFaseMov(void)
{
    if(Carrossel_sobe) {FormataResp(7, "Subindo",'|');}//subindo
    else if(Carrossel_desce){FormataResp(8, "Descendo",'|');}//descendo
    else if(Carrossel_gira) {FormataResp(7, "Girando",'|');}//girando
    else if(Carrossel_agita){FormataResp(8, "Agitando",'|');}//agitando
    else{{FormataResp(6, "Parado",'|');}} //Parado
 
}
////////////
// Le Status de execu�ao do ciclo
//
//
void FormatStaCiclo(void)
{
    unsigned char ret;
    ret = 0;
    if(STATUS_EXECUCAO == 0){FormataResp(6, "Parado",'|');}//Parado
    else if(STATUS_EXECUCAO == 1){FormataResp(7, "Pausado",'|');}//Pausado
    else if(STATUS_EXECUCAO == 2){FormataResp(7, "Retardo",'|');}//Aguardando retardo
    else if(STATUS_EXECUCAO == 3)//Rodando programa
    {
      FormataResp(10, "Executando",':');
      ConverteAscii(Numero_programa+1);
      FormataResp(4, Msg,'|'); 
    }
    else
    {
      FormataResp(6, "Parado",'|');
    }
}
//
// Formata temperatura para envio
// T = 124 => 12.4;//�C
void FormatTemp(unsigned int temp)
{
    if(temp < 999)
    {
      ConverteAscii(temp);
      Msg[0] = Msg[1];
      Msg[1] = Msg[2];
      Msg[2] = '.';
      FormataResp(4, Msg,'|');  
    }
    else{FormataResp(4, "99.9",'|');}
}


/////////////////////////////////////////////////////////////////////////////////////////////
// Envia dados para sistema nuvem
//#8$ - Envia os dados para nuvem. Formato #8|Val0|Val1|Val2|Val3|Val4|Val4|Val5|Val6|Val7|Val8|Val9|Val10|Val11|Val12|$ 
//Val0: Id_maquina 
//Val1: Numero do programa que est� rodando atualmente
//Val2: Tempo regressivo de espera para t�rmino da fase  HH:MM:SS
//Val3: In�cio Programa com Retardo (DD/MM/AA HH:MM) sem validar
//Val4: Caneca atual que est� o primeiro cesto
//Val5: Fase de movimento: (1.subindo,2.descendo,3.girando,4.agitando,5.parado,6.aguardando retardo)
//Val6: Alarme Temperatura Caneca 1 (0:normal, 1: baixa, 2: alta, 3: erro sensor)
//Val7: Alarme Temperatura Caneca 2 (0:normal, 1: baixa, 2: alta, 3: erro sensor)
//Val8: Alarme Temperatura Caneca 3 (0:normal, 1: baixa, 2: alta, 3: erro sensor)
//Val9: Alarme de Movimento (0: normal, 1: subida, 2: descida , 3: giro)
//Val10: Temperatura caneca 1
//Val11: Temperatura caneca 2
//Val12: Temperatura caneca 3
/////////////////////////////////////////////////////////////////////////////////////////////
void EnviaDataNuvem(void)
{   
  if(DEMO_NUVEM==1)
  {   
      EnviaDataNuvem_DEMO();
  }
  else
  {
      
     //#8|87987|Ciclo|Tempo|xxx|Caneca|Alarme Ativo|A��o|T1:|T2:|T3:|$
     //Id_maquina  
     FormataResp(5,"87987",'|');          
     //CICLO
     FormataResp(15, text2ciclo,'|');
     //TEMPO
     FormataResp(7, text2tcaneca,'|');       
     //?????????
     FormataResp(2, "??",'|');       
     //CANECA
     FormataResp(10, text2caneca,'|');
     //ALARME       
     FormataResp(10, text2alarme,'|');                                     
     //ACAO
     FormataResp(10, text2acao,'|');       
     //TEMPERATURA 1
     FormataResp(5, text2temp1,'|');       
     //TEMPERATURA 2
     FormataResp(5, text2temp2,'|');       
     //TEMPERATURA 3
     FormataResp(5, text2temp3,'|');       
     
     EnviaResposta();   
  }
}
//////////////////////////////////////
// Converte String para numero at� 255
/////////////////////////////////////
unsigned char Str2I(char *ptrString)
{
   
   if(*ptrString >= '0' && *ptrString <= '9')
   {
      
    
   }
  
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Processa o comando vindo da Nuvem
// #|W|<#num_comando>|<#num_ciclo>|<DD/MM/AA HH:MM>|<id>|$
// #|W|1|3|09/04/19 03:30>|153|$
/////////////////////////////////////////////////////////////////////////////////////////////
void ProcessaCmdNuvem(void)
{
    unsigned char posicoes[10],tam;
    
    CarregaPopUp(0x34);//fechar pop-up
    
    if(BufProt[2]=='1')// Iniciar Ciclo  
    {
      //Encontra a posi��o data
      SplitString(PtrProt + 1,BufProt, posicoes,'|',1);      
      tam = posicoes[3] - posicoes[2];
      if(tam > 2)
      {
         numCicloRemoto  = ((BufProt[posicoes[2]+1]-0x30)*10)+(BufProt[posicoes[2]+2]-0x30);
      }
      else
      {
        numCicloRemoto  =  (BufProt[posicoes[2]+1]-0x30);
      }
      
      //Encontra a posi��o data
      SplitString(PtrProt + 1,BufProt, posicoes,'/',1);      
      //Converte string para numero
      diaRemoto	= ((BufProt[posicoes[0]-2]-0x30)*10)+(BufProt[posicoes[0]-1]-0x30);
      mesRemoto	= ((BufProt[posicoes[0]+1]-0x30)*10)+(BufProt[posicoes[0]+2]-0x30);
      //Encontra a posicao hora            
      SplitString(PtrProt + 1,BufProt, posicoes,':',1);
      //Converte string para numero                  
      segRemoto	= 0;
      minRemoto	= ((BufProt[posicoes[0]+1]-0x30)*10)+(BufProt[posicoes[0]+2]-0x30);
      horaRemoto= ((BufProt[posicoes[0]-2]-0x30)*10)+(BufProt[posicoes[0]-1]-0x30);     
       
      Proc_Init_Ciclo(3);
      
            
    } 
    else if(BufProt[2]=='2') //Pausar Ciclo
    {
      Proc_Pausa_Ciclo(3);        
      
    } 
    else if(BufProt[2]=='3') //Interromper Ciclo 
    {
      Proc_Inter_Ciclo(3);
      
    }
    else if(BufProt[2]=='4') //Subir 
    {
      if(EstadoRco == ESTA_EXE_CICLO)
      {
        Proc_Pausa_Ciclo(3);
        
      }
      EstadoRco = ESTA_MANUAL;
      Proc_Subida();
      
    }
    else if(BufProt[2]=='5') //Descer
    {
      if(EstadoRco == ESTA_EXE_CICLO)
      {
        Proc_Pausa_Ciclo(3); 
      }
      EstadoRco = ESTA_MANUAL;
      Proc_Descida();
      
    }
    else if(BufProt[2]=='6') //Girar
    {
      if(EstadoRco == ESTA_EXE_CICLO)
      {
        Proc_Pausa_Ciclo(3); 
        
      }
      EstadoRco = ESTA_MANUAL;
      Proc_Gira();
    }
    else if(BufProt[2]=='7') //Zerar
    {
      EstadoRco = ESTA_PARA_CICLO;
      Proc_Pausa_Ciclo(3);
      
    }
    else if(BufProt[2]=='8') //Ignorar Alarme
    {
      
    }
    else if(BufProt[2]=='9') //Ignorar Alarme
    {
      Aj_Ano = ((BufProt[4]-0x30)*10)+(BufProt[5]-0x30);
      Aj_Mes = ((BufProt[6]-0x30)*10)+(BufProt[7]-0x30);
      Aj_Dia = ((BufProt[8]-0x30)*10)+(BufProt[9]-0x30);
      Aj_Horas = ((BufProt[10]-0x30)*10)+(BufProt[11]-0x30);
      Aj_Minutos = ((BufProt[12]-0x30)*10)+(BufProt[13]-0x30);

      EscreveRtc();
    }
    
    PtrProt = 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para tratar o protocolo da nuvem
/////////////////////////////////////////////////////////////////////////////////////////////

void TrataNuvem(void)
{  
///////////////////////Aguarda inicio de pacote    
  	if (Estpro==0)
  	{
  		if(LeDadoRx1()==1)
  		{
  		   if(BufRx1[PtrSai1] == '#') //'#' asinala o inicio da mensagem
  		   {
 //Gambiarra pois protocolo esta errado tira o '|' quando � comando
  			  if(flagConectado)
  			  {
  			    Estpro++;
  			  }
  			  else
  			  {
  			    Estpro = 10;
  			  }
  			  
  			  PtrProt = 0;
  			  DelComando = 0;
  		   }
  		   else {PtrEnt1 = PtrSai1 = DelComando = 0;}
  		}
  	}
///////////////////////////////////	 
  	if (Estpro==10)
  	{
  		if(LeDadoRx1()==1)
  		{
  		   if(BufRx1[PtrSai1] == '|') //'|' asinala o inicio da mensagem
  		   {
  			  Estpro = 1;
  			  PtrProt = 0;
  			  DelComando = 0;
  			  flagConectado = 1;
  		   }
  		   else {Estpro=PtrEnt1 = PtrSai1 = DelComando = 0;}
  		}
  	}                             
 ///////////////////Capturando pacote ate o fim
  	if (Estpro==1)
  	{
  		if(LeDadoRx1()==1)
  		{
  		   if(BufRx1[PtrSai1] == '$') //$ assinala o fim da mensagem
  		   {
  			   Estpro++;
  			   DelComando = 0;
  		   }
  		   else
  		   {
  			   BufProt[PtrProt] = BufRx1[PtrSai1];
  			   if(PtrProt < 49){ PtrProt++; }
  			   else
  			   {
  			      PtrProt = 0;
  			      Estpro = 0;
  			   }
  		   }
  		}
  	}
//////////////////Processa pacote  	
  	if (Estpro==2)
  	{
  		if(BufProt[0]=='W')//Comando para maquina
  		{
  			 ProcessaCmdNuvem();
  		}
  		Estpro  = 0; // retorna ao estado inicial
  		PtrProt = 0;
  		DelComando = 0;
  	}
////////////////Time-out  	
  	if(DelComando > T_OUT_COM)//Time-out para falha de protocolo
  	{
  		Estpro   = 0; // retorna ao estado inicial
  		PtrEnt1 = PtrSai1 = DelComando = 0;
  	}	  
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Trata Serial                                                         
/////////////////////////////////////////////////////////////////////////////////////////////
void TrataSerial(void)
{     
    TrataProtocolo(); 
    TrataNuvem();
}


void zera_buffer_dados_display()
{
    valorLido = 0xff;
    valorLido0 = 0xff;
    valorLido1 = 0xff;
    valorLido2 = 0xff;
    valorLido3 = 0xff;
    valorLido4 = 0xff;
    valorLido5 = 0xff;
    endRamAlto = 0xff;
    endRamBaixo = 0xff; 
}


/////////////////////////////////////////////////////////////////////////////////////////////
// Envia Comando Para retornar ESP para AP                                                         
/////////////////////////////////////////////////////////////////////////////////////////////
void ComandoRetornaAP(void)
{
     TxChar1('#');
     TxChar1('4');
     TxChar1('$');
	   TxChar1('\r');
	   TxChar1('\n');

}

/////////////////////////////////////////////////////////////////////////////////////////////
// Envia Comando Nuvem                                                         
/////////////////////////////////////////////////////////////////////////////////////////////
void EnviaComandoNuvem(unsigned char comando)
{
  if(comando == CMD_NUVEM_AP)
  {
    ComandoRetornaAP();
  }
}





