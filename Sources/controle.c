/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
===============================================================================================*/
#include "masterheader.h"

/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para controle da temperatura
// Controle ON/OFFcom histerese
/////////////////////////////////////////////////////////////////////////////////////////////
void ControlaTemperatura(void) {
  KP = 4;
  KI = 2;
  if (Flag_aquecimento == 1) 
  {
    if (Flag_PT100_1 == 1) 
    {
      Erro_t1 = temperatura_ajustada_T1 - tempT1;//(temperatura_ajustada_T1) - (valorADC[1] + off_set_temperatura1);
      erro_integral_t1 += (Erro_t1 + erro_anterior_t1);
      
      if (erro_integral_t1 > 30) erro_integral_t1 = 30;
      if (erro_integral_t1 < -30) erro_integral_t1 = -30;
      if (Erro_t1 == 0) erro_integral_t1 = 0;
      
      erro_anterior_t1 = Erro_t1;
      DUTY_1 = ((Erro_t1 * KP) / 5) + (erro_integral_t1 * KI);
      
      if (DUTY_1 > 99){ DUTY_1 = 99;}
      if (DUTY_1 < 0) {DUTY_1 = 0;}
    } 
    else
    {
        DUTY_1 = 0; 
    }

    if (Flag_PT100_2 == 1)
    {
      Erro_t2 = temperatura_ajustada_T2 - tempT2;//(temperatura_ajustada_T2) - (valorADC[2] + off_set_temperatura2);
      erro_integral_t2 += (Erro_t2 + erro_anterior_t2);
      if (erro_integral_t2 > 30) erro_integral_t2 = 30;
      if (erro_integral_t2 < -30) erro_integral_t2 = -30;
      if (Erro_t2 == 0) erro_integral_t2 = 0;
      DUTY_2 = ((Erro_t2 * KP) / 5) + (erro_integral_t2 * KI);
      erro_anterior_t2 = Erro_t2;
      if (DUTY_2 > 99) DUTY_2 = 99;
      if (DUTY_2 < 0) DUTY_2 = 0;

    } 
    else DUTY_2 = 0;

    if (Flag_PT100_3 == 1) {
      Erro_t3 = temperatura_ajustada_T3 - tempT3;//(temperatura_ajustada_T3) - (valorADC[3] + off_set_temperatura3);
      erro_integral_t3 += ((Erro_t3 + erro_anterior_t3));
      if (erro_integral_t3 > 30) erro_integral_t3 = 30;
      if (erro_integral_t3 < -30) erro_integral_t3 = -30;
      if (Erro_t3 == 0) erro_integral_t3 = 0;
      DUTY_3 = ((Erro_t3 * KP) / 5) + (erro_integral_t3 * KI);
      erro_anterior_t3 = Erro_t3;
      if (DUTY_3 > 99) DUTY_3 = 99;
      if (DUTY_3 < 0) DUTY_3 = 0;

    } else DUTY_3 = 0;

    if (IN_127_220 == 0) {
      if (Flag_PT100_1 == 1) {
        if (DUTY_1 > Base_PWM) {
          AQC11 = 0;
          AQC12 = 0;
        } else {
          AQC11 = 1;
          AQC12 = 1;
        }
      }
      if (Flag_PT100_2 == 1) {
        if (DUTY_2 > Base_PWM) {
          AQC21 = 0;
          AQC22 = 0;
        } else {
          AQC21 = 1;
          AQC22 = 1;
        }
      }
      if (Flag_PT100_3 == 1) {
        if (DUTY_3 > Base_PWM) {
          AQC31 = 0;
          AQC32 = 0;
        } else {
          AQC31 = 1;
          AQC32 = 1;
        }
      }
      if (Flag_PT100_4 == 1) {
        if (DUTY_4 > Base_PWM) {
          AQC41 = 0;
          AQC42 = 0;
        } else {
          AQC41 = 1;
          AQC42 = 1;
        }
      }
    } else {
      if (Flag_PT100_1 == 1) {
        if (DUTY_1 > Base_PWM) {
          AQC11 = 1;
          AQC12 = 0;
        } else {
          AQC11 = 1;
          AQC12 = 1;
        }
      }
      if (Flag_PT100_2 == 1) {
        if (DUTY_2 > Base_PWM) {
          AQC21 = 1;
          AQC22 = 0;
        } else {
          AQC21 = 1;
          AQC22 = 1;
        }
      }
      if (Flag_PT100_3 == 1) {
        if (DUTY_3 > Base_PWM) {
          AQC31 = 1;
          AQC32 = 0;
        } else {
          AQC31 = 1;
          AQC32 = 1;
        }
      }
      if (Flag_PT100_4 == 1) {
        if (DUTY_4 > Base_PWM) {
          AQC41 = 1;
          AQC42 = 0;
        } else {
          AQC41 = 1;
          AQC42 = 1;
        }
      }
    }
    AQC41 = 1;
    AQC42 = 1; //desliga resistencia 4 enquanto n�o for implementado o controle
  } else {
    if (EstRco != MANUTENCAO) {
      AQC11 = 1;
      AQC12 = 1;
      AQC21 = 1;
      AQC22 = 1;
      AQC31 = 1;
      AQC32 = 1;
      AQC41 = 1;
      AQC42 = 1;
    }

  }
}

void controle_de_erros()
{
  

}

void Detecta_erro_de_Sensor_temperatura() //////////////////////////////////////////
{ // Detecta erro de Sensor de temperatura//
  
}

void Detecta_erro_Temperatura_baixa() ///////////////////////////////////////
{ 
}

void erro_sensor_temperatura() {
 
}

void temperatura_alta() 
{
  
}

void temperatura_baixa() {
 
}

void erro_movimento() 
{
  
}

void carrega_erros_na_variavel() {
 
}

void carrega_variavel_no_erro() {


}
////////////////////////////////////////////////
//Controle de execu��o do ciclo
/////////////////////////////////////////////////
void ControleExeCiclo (unsigned char num_ciclo)
{
  //Ciclo Parado 
  if(EstadoCiclo == ESTA_CICLO_PARADO)
  {
    
    
  }//Ciclo Pausado
  else
  if(EstadoCiclo == ESTA_CICLO_PAUSADO)
  {
    //Retorno autom�tico para o ciclo
    if((TempoRetPause >= TEMP_RET_PAUSE) && (TempoRetPause <= 0xFF) && numAlarmes==0)
    {
        Proc_Ret_Ciclo(1);
        EstadoRco = ESTA_EXE_CICLO;       
    }
  }//Ciclo Aguardando Retardo
  else
  if(EstadoCiclo == ESTA_CICLO_AGUARDA)
  {
      //Testa Data e Hora de in�cio
      if(anoRtc  >= ano_execucao && mesRtc  >= mes_execucao && diaRtc  >= dia_execucao && horaRtc >= hora_execucao && minRtc >= minutos_execucao)
      {
        // Inicia Ciclo
        EstadoCiclo = ESTA_CICLO_DESCE; 
        posCaneca = 1;
        SubEstadoCiclo = 0;
      }
  }
  //Ciclo Desce
  else
  if(EstadoCiclo == ESTA_CICLO_DESCE)
  {
     //Ativa descida
     if(posCarrossel == CARRO_BAIXO)
     {
        EstadoCiclo = ESTA_CICLO_AGITA;
        SubEstadoCiclo = 0;
     }
     //Desce
     else
     if(SubEstadoCiclo == 0)
     {
        if(Proc_Descida())//checa se ligou descida
        {
          SubEstadoCiclo++;  
        }
        else//N�o ligou descida
        {
          EstadoCiclo == ESTA_CICLO_ERRO;  
        }
     }
 
  }//Ciclo Agita
  else
  if(EstadoCiclo == ESTA_CICLO_AGITA)
  {
      if(SubEstadoCiclo==0)
      {
        //Carrega tempo
        Tempo_de_sub_estagio = array_parametros[posCaneca - 1][num_ciclo];        
        Tempo_prox_sub_estagio = array_parametros[posCaneca][num_ciclo];
        
        if(Proc_Agita(1))//checa se ligou agitacao
        {
            meioSeg = Seg = TempoAgitando = 0; //Inicia contagem do tempo de agita��o 
            SubEstadoCiclo++; 
        }
        else// n�o ligou a agita��o
        {
          EstadoCiclo == ESTA_CICLO_ERRO;  
        }
      }
      //Aguarda Fim do tempo de agita��o 
      else if(SubEstadoCiclo==1)
      {
        if(TempoAgitando >= Tempo_de_sub_estagio)
        {
            TempoAgitando = 0xFF;//Para de contar tempo agita
            SubEstadoCiclo = 0;
            if(Proc_Agita(0))
            {    
               EstadoCiclo = ESTA_CICLO_SOBE;            
            } 
            else
            {
                EstadoCiclo == ESTA_CICLO_ERRO;  
            }
        } 
      }     
  }//Ciclo Sobe e Espera
  else
  if(EstadoCiclo == ESTA_CICLO_SOBE)
  {
      if(SubEstadoCiclo==0)
      {
        //Carrega tempo de escorrimento
        TempoEscorreDefinido = TEMPO_ESCORRIMENTO;//Segundos
        
        if(posCarrossel == CARRO_ALTO)//checa se esta em cima
        {
            meioSeg = Seg = TempoEscorrendo = 0; //Inicia contagem do tempo escorrendo
            SubEstadoCiclo++; 
        }
        else if(Proc_Subida())//manda subir
        {
            SubEstadoCiclo++; 
        }
        else
        {
          EstadoCiclo = ESTA_CICLO_ERRO;  //erro na subida          
        }
      }
      else if(SubEstadoCiclo==1)//Espera Subir
      {
        if(posCarrossel == CARRO_ALTO)//checa se esta em cima
        {
            meioSeg = Seg =  TempoEscorrendo = 0; //Inicia contagem do tempo escorrendo
            SubEstadoCiclo++; 
        }
      }
      //Aguarda Fim do tempo de escorrimento 
      else if(SubEstadoCiclo==2)
      {
        if(TempoEscorrendo >= TempoEscorreDefinido)
        {
            TempoEscorrendo = 0xFF;//Para de contar tempo Escorrendo
            SubEstadoCiclo = 0;
            EstadoCiclo = ESTA_CICLO_GIRA;            
        } 
      }
    
  }//Ciclo Gira
  else
  if(EstadoCiclo == ESTA_CICLO_GIRA)
  {
      if(SubEstadoCiclo==0)
      {
        if(Proc_Gira())//checa se ligou giro
        {
            posCanecaCicloAnt = posCaneca;//Salva valor atual da caneca
            SubEstadoCiclo++; 
        }
        else// n�o ligou giro
        {
          EstadoCiclo == ESTA_CICLO_ERRO;  
        }
      }
      //Aguarda giro completo 
      else if(SubEstadoCiclo==1)
      {
        if(posCaneca > posCanecaAnt)
        {
            posCanecaAnt = posCaneca;
            SubEstadoCiclo = 0;
            EstadoCiclo = ESTA_CICLO_TESTA;            
        } 
      }
    
  }//Ciclo Testa Fim
  else
  if(EstadoCiclo == ESTA_CICLO_TESTA)
  {
       if(posCaneca == MAX_CANECAS)
       {
         EstadoCiclo = ESTA_CICLO_FIM;
       }
       else
       {
         EstadoCiclo = ESTA_CICLO_DESCE;
       }
    
  }//Ciclo TERMINOU
  else
  if(EstadoCiclo == ESTA_CICLO_FIM)
  {
      NumCiclo = 0;
      Proc_Desliga();
      EstadoCiclo = 0;
  }//ERRO no Ciclo
  else
  if(EstadoCiclo == ESTA_CICLO_ERRO)
  {
    
    
  }
  
}
/////////////
//Parte do controle geral de alarmes
////////////
void ControleAlarmesTemp (void)
{
   ////////Ativar os alarmes temperatura
 //Temperaturas altas (acima de MAX_TEMP graus)
    if(tempT1 > (temperatura_ajustada_T1 + MAX_TEMP_1))
    {
       alarTempAlta[1] = 1;
       alarTempBaixa[1] = 0;
    }
    if(tempT2 > (temperatura_ajustada_T2 + MAX_TEMP_2))
    {
       alarTempAlta[2] = 1;
       alarTempBaixa[2] = 0;
    }
    if(tempT3 > (temperatura_ajustada_T3 + MAX_TEMP_3))
    {
       alarTempAlta[3] = 1;
       alarTempBaixa[3] = 0;
    }
 //Temperaturas Baixas (abaixo de MIN_TEMP)
    if(tempT1 < (temperatura_ajustada_T1 - MIN_TEMP_1))
    {
      TempoTempBaixa1 = 0xFF;//Para timer
      alarTempBaixa[1] = 1;//Ativa alarme 
    }
    if(tempT2 < (temperatura_ajustada_T2 - MIN_TEMP_2))
    { 
      TempoTempBaixa2 = 0xFF;
      alarTempBaixa[2] = 1;           
    }
    if(tempT3 < (temperatura_ajustada_T3 - MIN_TEMP_3))
    {
      TempoTempBaixa3 = 0xFF;
      alarTempBaixa[3] = 1;
    }
//Temperaturas Erro Sensor 


    if(((tempT1 - off_set_temperatura1)  < ALAR_SENSOR_1_MIN) || ((tempT1 - off_set_temperatura1) > ALAR_SENSOR_1_MAX))
    {
      alarTempSensor[1] = 1;            
      alarTempAlta[1] = 0;
      alarTempBaixa[1] = 0;
    } 
    else
    {
      acetAlarTempSensor[1] = 0;            
      alarTempSensor[1] = 0;            
    }
    if(((tempT2 - off_set_temperatura2)  < ALAR_SENSOR_2_MIN) || ((tempT2 - off_set_temperatura2) > ALAR_SENSOR_2_MAX))
    {
      alarTempSensor[2] = 1;
      alarTempAlta[2] = 0;
      alarTempBaixa[2] = 0;            
    } 
    else
    {
      acetAlarTempSensor[2] = 0;            
      alarTempSensor[2] = 0;            
    }
    if(((tempT3 - off_set_temperatura3)  < ALAR_SENSOR_3_MIN) || ((tempT3 - off_set_temperatura3) > ALAR_SENSOR_3_MAX))
    {
      alarTempSensor[3] = 1;
      alarTempAlta[3] = 0;
      alarTempBaixa[3] = 0;            
    } 
    else
    {
      acetAlarTempSensor[3] = 0;            
      alarTempSensor[3] = 0;            
    }
 ////////FIM Ativar os alarmes temperatura   
  
 ////////Desativar os alarmes temperatura
 //Temperaturas altas (acima de MAX_TEMP graus)
    if(tempT1 < (temperatura_ajustada_T1 + MAX_TEMP_1))
    {
       alarTempAlta[1] = 0;
       acetAlarTempAlta[1] = 0;
       
    }
    if(tempT2 < (temperatura_ajustada_T2 + MAX_TEMP_2))
    {
       acetAlarTempAlta[2] = 0;
       alarTempAlta[2] = 0;
    }
    if(tempT3 < (temperatura_ajustada_T3 + MAX_TEMP_3))
    {
       acetAlarTempAlta[3] = 0;
       alarTempAlta[3] = 0;
    }
 //Temperaturas Baixas (abaixo de MIN_TEMP)
    if(tempT1 > (temperatura_ajustada_T1 - MIN_TEMP_1))
    {
      TempoTempBaixa1 = 0;//libera contagem de 2 horas
    }
    if(TempoTempBaixa1 >= T_TEMP_BAIXO_1 && TempoTempBaixa1 < 0xFF)//desativa depois de 2 horas
    {
      acetAlarTempBaixa[1] = 0;
      alarTempBaixa[1] = 0;
    }
    if(tempT2 > (temperatura_ajustada_T2 - MIN_TEMP_2))
    {
      TempoTempBaixa2 = 0;//libera contagem de 2 horas
    }
    if(TempoTempBaixa2 >= T_TEMP_BAIXO_2 && TempoTempBaixa2 < 0xFF)//desativa depois de 2 horas
    {
      acetAlarTempBaixa[2] = 0;
      alarTempBaixa[2] = 0;
    }
    if(tempT3 > (temperatura_ajustada_T3 - MIN_TEMP_3))
    {
      TempoTempBaixa3 = 0;//libera contagem de 2 horas
    }
    if(TempoTempBaixa3 >= T_TEMP_BAIXO_3 && TempoTempBaixa3 < 0xFF)//desativa depois de 2 horas
    {
      acetAlarTempBaixa[3] = 0;
      alarTempBaixa[3] = 0;
    }
//Temperaturas Erro Sensor 
//    if(tempT1 > ALAR_SENSOR_1)
//    {
//      acetAlarTempSensor[1] = 0;            
//      alarTempSensor[1] = 0;            
//    }
//    if(tempT2 > ALAR_SENSOR_2)
//    {
//      acetAlarTempSensor[2] = 0;            
//      alarTempSensor[2] = 0;            
//    }
//    if(tempT3 > ALAR_SENSOR_3)
//    {
//      acetAlarTempSensor[3] = 0;            
//      alarTempSensor[3] = 0;            
//    }  
////////FIM Desativar os alarmes temperatura   
}

/////////////
//Parte do controle geral de alarmes
////////////
void ControleAlarmesMov (void)
{
//Ativa alarmes de movimento
   if(TempoSubindo  > TEMP_ERRO_SOBE && TempoSubindo < 0xFF)
   {
      alarSobe = 1;
      TempoSubindo = 0xFF;
   }
   if(TempoDescendo > TEMP_ERRO_DESCE && TempoDescendo < 0xFF)
   {
      alarDesce = 1;
      TempoDescendo = 0xFF;
   }
   if(TempoGirando  > TEMP_ERRO_GIRO && TempoGirando < 0xFF)
   {
      alarGiro = 1;
      TempoGirando = 0xFF;
   }
   if(posCarrossel == CARRO_ERRO)
   {
    alarSensor = 1;
   }
//FIM Ativa alarme de movimento
//Desativa alarme de movimento
   if(posCarrossel != CARRO_ERRO)
   {
      acetAlarSensor = 0;
      alarSensor = 0;
   }


//FIM Desativa alarme de movimento   
}
////////////////////////////////////////////////
//Controle de ativar alarme
//alarTempBaixa1
//alarTempAlta1
//alarTempSensor1
//alarSobe
//alarDesce
//alarGiro
//alarSensor
/////////////////////////////////////////////////
void ControleAlarmes (void)
{
  if(EstadoRco >= ESTA_HOME)
  {
    ControleAlarmesTemp();   
    ControleAlarmesMov();
    //Calcula numero de alarmes ativos
    numAlarmes = alarTempBaixa[1]+alarTempBaixa[2]+alarTempBaixa[3]+
                  alarTempAlta[1]+alarTempAlta[2]+alarTempAlta[3]+
                  alarTempSensor[1]+alarTempSensor[2]+alarTempSensor[3]+
                  alarSobe+alarDesce+alarGiro+alarSensor;
    
    //Calcula numero de alarmes de movimento ativos                  
    numAlarmesMov = alarSobe+alarDesce+alarGiro+alarSensor;
    
    //Calcula numero de alarmes ativos n�o aceitos pelo usu�rio
    numAlarmesNaoAceitos =  (alarTempBaixa[1]	- acetAlarTempBaixa[1]	)+
                            (alarTempBaixa[2]	- acetAlarTempBaixa[2]	)+
                            (alarTempBaixa[3]	- acetAlarTempBaixa[3]	)+
                            (alarTempAlta[1]	- acetAlarTempAlta[1]	)+
                            (alarTempAlta[2]	- acetAlarTempAlta[2]	)+
                            (alarTempAlta[3]	- acetAlarTempAlta[3]	)+
                            (alarTempSensor[1]	- acetAlarTempSensor[1]	)+
                            (alarTempSensor[2]	- acetAlarTempSensor[2]	)+
                            (alarTempSensor[3]	- acetAlarTempSensor[3]	)+
                            (alarSobe			- acetAlarSobe			)+
                            (alarDesce			- acetAlarDesce		)+
                            (alarGiro			- acetAlarGiro			)+
                            (alarSensor			- acetAlarSensor	); 
        if(numAlarmesMov > 0)
            Proc_Desliga();
      } 
        
}