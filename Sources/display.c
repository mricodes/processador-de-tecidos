/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
/ Rotinas para o display
===============================================================================================*/
#include "masterheader.h"

/////////////////////////////////////////////////////////////////////////////////////////////
// Soma total de micras em byte alto e byte baixo                                             
/////////////////////////////////////////////////////////////////////////////////////////////
void SomaTotalMicras(unsigned char valor){
 unsigned char diff = 0;
 
 diff = (254 - totalMicrasL); 
 if(valor > diff){
    totalMicrasL = 0;
    totalMicrasL = valor-diff;
//    testeDA++;
    if(totalMicrasH<0xFF){totalMicrasH++;}
    
 }else{
  //totalMicrasH=0;
  totalMicrasL +=valor;
 }

}


/////////////////////////////////////////////////////////////////////////////////////////////
// Carrega Tela no display   A5 5A 04 80 03 00 TT                                                       
/////////////////////////////////////////////////////////////////////////////////////////////
void CarregaTela(unsigned char tela){
  
  auxTela = tela + off_set_idioma;
  
  if(auxTela != auxTelaAtual){    //para n�o enviar continuamente a mesma tela
    TxChar2(0xA5);
    TxChar2(0x5A);
    TxChar2(0x04);
    TxChar2(0x80);
    TxChar2(0x03);
    TxChar2(0x00);
    TxChar2(auxTela);
    auxTelaAtual = auxTela;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Carrega PopUp no display   A5 5A 03 80 4F PU                                                       
/////////////////////////////////////////////////////////////////////////////////////////////

void CarregaPopUp(unsigned char pop)
{
  unsigned char auxPop = pop;
 	if(auxPop != auxPopAtual)
 	{
 	//auxPop != auxPopAtual){
 	//  auxPop != auxPopAtual){    //para n�o enviar continuamente a mesma tela
 	TxChar2(0xA5);
	TxChar2(0x5A);
	TxChar2(0x03);
	TxChar2(0x80);
	TxChar2(0x4F);
	TxChar2(auxPop);
	auxPopAtual = auxPop;
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////
// Buzzer                                                        
/////////////////////////////////////////////////////////////////////////////////////////////
void Buzzer(unsigned char tempo)
{ // tempo multiplo de 10ms 
TxChar2(0xA5);
TxChar2(0x5A);
TxChar2(0x03);
TxChar2(0x80);
TxChar2(0x02);
TxChar2(tempo);  
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Leitura das teclas do touch                                                         
/////////////////////////////////////////////////////////////////////////////////////////////
char LeTouch(void)
{
	if(valorLido1 != 0xFF)
	{
     	teclaDisp = valorLido1;
      valorLido1 = 0xFF;
     	teclaDisp1 = endRamBaixo;
      endRamBaixo = 0xFF; 
      DelTela = 0;
      return 1; 
   	} 
   	else
   	{
     	return 0;
   	}     
}


void escreve_texto(char strng[],int HHH, int LLL)
{
    int i3;
    i3 = strlen(strng);		
    TxChar2(0xA5);
    TxChar2(0x5A);
    TxChar2(i3+0x03);
    TxChar2(0x82);
    TxChar2(HHH);
    TxChar2(LLL);

    for(i1=0 ; i1 < i3 ; i1++)
    {
        TxChar2(strng[i1]);
    }		
		
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Atualiza o texto na posicao "pos" 
// informar "tamanho" sendo a quantidade de caracteres a serem escritos
// A5 5A 0D 82 POS POS DD DD DD DD DD
/////////////////////////////////////////////////////////////////////////////////////////////
void AtualizaTexto(char *ptext, unsigned char tamanho,unsigned char posH, unsigned char posL)
{
	unsigned char i;
	unsigned char *p;
	tamanho = DecToHex(tamanho);

	TxChar2(0xA5);
  TxChar2(0x5A);	
  TxChar2(tamanho + 3);
	TxChar2(0x82);
	TxChar2(posH);
	TxChar2(posL);

	for (i = 0; i < tamanho; i++)
	{
		TxChar2(*ptext);
		ptext++;
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Atualiza o valor do campo na posicao "pos" com o valor A5 5A 05 82 00 POS ValH ValL                                                 
/////////////////////////////////////////////////////////////////////////////////////////////
void AtualizaCampo(long int pos,long int valor){       

      TxChar2(0xA5);
      TxChar2(0x5A);
      TxChar2(0x05);
      TxChar2(0x82);
      TxChar2(pos/256);
      TxChar2(pos);
      TxChar2(valor >>8);
      TxChar2(valor);  
      
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Atualiza icone do bot�o   A5 5A 05 82 00 AA 00 VV   AA: endere�o VV: valor                                    
/////////////////////////////////////////////////////////////////////////////////////////////
void AtualizaIcone(char pos,char valor){
      TxChar2(0xA5);
      TxChar2(0x5A);
      TxChar2(0x05);
      TxChar2(0x82);
      TxChar2(0x00);
      TxChar2(pos);
      TxChar2(0x00);
      TxChar2(valor); 
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Leitura dos valores de RTC   A5 5A 03 81 20 07 03                                                   
/////////////////////////////////////////////////////////////////////////////////////////////
void LeRtc(void)
{
    TxChar2(0xA5);
    TxChar2(0x5A);
    TxChar2(0x03);
    TxChar2(0x81);
    TxChar2(0x20);
    TxChar2(0x07);
    TxChar2(0x03);
   
  	while(EstSerial < (numBytes + 2))
  	{
  	  TrataSerial();
  	}
    anoRtc = HexToDec(valorLido);
    mesRtc = HexToDec(valorLido0);
    diaRtc = HexToDec(valorLido1);
    semRtc = HexToDec(valorLido2);
    horaRtc = HexToDec(valorLido3);
    minRtc = HexToDec(valorLido4);
    segRtc = HexToDec(valorLido5);
      
    valorLido = 0xff;
    valorLido0 = 0xff;
    valorLido1 = 0xff;
    valorLido2 = 0xff;
    valorLido3 = 0xff;
    valorLido4 = 0xff;
    valorLido5 = 0xff;
    endRamAlto = 0xff;
    endRamBaixo = 0xff;
   
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Escrita dos valores de RTC   A5 5A 0A 80 1F 5A AA MM DD 00 HH MM SS                                                   
/////////////////////////////////////////////////////////////////////////////////////////////

void EscreveRtc(void)
{
      TxChar2(0xA5);
      TxChar2(0x5A);
      TxChar2(0x0A);
      TxChar2(0x80);
      TxChar2(0x1F);
      TxChar2(0x5A);
      TxChar2(DecToHex(Aj_Ano));
      TxChar2(DecToHex(Aj_Mes));
      TxChar2(DecToHex(Aj_Dia));
      TxChar2(0x00);
      TxChar2(DecToHex(Aj_Horas));
      TxChar2(DecToHex(Aj_Minutos));
      TxChar2(DecToHex(0));
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Convers�o  Hexa para Decimal                                             
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char HexToDec(unsigned char hexa){
    unsigned char A;
    unsigned char B;
    A=hexa/16;
    B=hexa%16;
    return A*10+B;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Convers�o Decimal para Hexa                                             
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char DecToHex(unsigned char dec){
    unsigned char A;
    
    A = (dec/10)*16+(dec%10);
    return A;
}
///////////////////////////////////////////////////////////////////////////////
// Converte valores para Ascii
void ConverteAscii(unsigned int num)
{
   if (num <= 9999) 
   {
   	  Msg[0]=Msg[1]=Msg[2]=Msg[3]='0';
	  while (num>=1000)
	  { 
	  	num = num - 1000;
	  	Msg[0]++; 
	  }
	  while (num>=100)
	  { 
	  	num = num - 100;
	  	Msg[1]++;
	  }
	  while (num>=10)
	  { 
	  	num = num - 10;
	  	Msg[2]++;
	  }
	  Msg[3] = '0' + num;
   }
   else { Msg[0]=Msg[1]=Msg[2]=Msg[3]='-'; }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Checa os Pop Ups para Aceitar os Erros
//    estado_rco_ant: estado para retorno na RCO
//
//    retorna: O pop-up que deve ser carregado
//                                             
/////////////////////////////////////////////////////////////////////////////////////////////

unsigned char ChecaPopUPAceita(unsigned char estado_rco_ant)
{
  unsigned char num_pop_aceita=0;
	if(alarTempBaixa[1] && !acetAlarTempBaixa[1])
	{
		num_pop_aceita = POP_ACET_ALARM_BAIXA_1;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempBaixa[1] = 1;}
		acetAlarTempBaixa[1] = 1;
	}
	else if(alarTempBaixa[2]	&& !acetAlarTempBaixa[2])
	{
		num_pop_aceita = POP_ACET_ALARM_BAIXA_2;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempBaixa[2] = 1;}
		acetAlarTempBaixa[2] = 1;
	}
	else if(alarTempBaixa[3]	&& !acetAlarTempBaixa[3])
	{
		num_pop_aceita = POP_ACET_ALARM_BAIXA_3;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempBaixa[3] = 1;}
		acetAlarTempBaixa[3] = 1;
		
	}
	else if(alarTempAlta[1]	&& !acetAlarTempAlta[1])
	{
		num_pop_aceita = POP_ACET_ALARM_ALTA_1;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempAlta[1] = 1;}
		acetAlarTempAlta[1] = 1;
	}
	else if(alarTempAlta[2]	&& !acetAlarTempAlta[2])
	{
		num_pop_aceita = POP_ACET_ALARM_ALTA_2;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempAlta[2] = 1;}
		acetAlarTempAlta[2] = 1;
	}
	else if(alarTempAlta[3]	&& !acetAlarTempAlta[3])
	{
		num_pop_aceita = POP_ACET_ALARM_ALTA_3;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempAlta[3] = 1;}
		acetAlarTempAlta[3] = 1;
	}
	else if(alarTempSensor[1] && !acetAlarTempSensor[1])
	{
		num_pop_aceita = POP_ACET_ALARM_SENSOR_1;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempSensor[1] = 1;}
		acetAlarTempSensor[1] = 1;
	}
	else if(alarTempSensor[2] && !acetAlarTempSensor[2])
	{
		num_pop_aceita = POP_ACET_ALARM_SENSOR_2;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempSensor[2] = 1;}
		acetAlarTempSensor[2] = 1;
	}
	else if(alarTempSensor[3] && !acetAlarTempSensor[3])
	{
		num_pop_aceita = POP_ACET_ALARM_SENSOR_3;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarTempSensor[3] = 1;}
		acetAlarTempSensor[3] = 1;
	}
//MOVIMENTO	
	else if(alarSobe	  && !acetAlarSobe)
	{
		num_pop_aceita = POP_ACET_ALARM_SOBE;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarSobe = 1;}
		acetAlarSobe = 1;
	}
	else if(alarDesce  && !acetAlarDesce)
	{
		num_pop_aceita = POP_ACET_ALARM_DESCE;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarDesce = 1;}
		acetAlarDesce = 1;
	}
	else if(alarGiro	  && !acetAlarGiro)
	{
		num_pop_aceita = POP_ACET_ALARM_GIRO;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarGiro = 1;}
		acetAlarGiro = 1;
	}
	else if(alarSensor && !acetAlarSensor)
	{
		num_pop_aceita = POP_ACET_ALARM_SENS;
		//if(tecla == CONFIRMA_ACEITA_ALARME)
		//{acetAlarSensor = 1;}
		acetAlarSensor = 1;
	}
	else
	{
	   num_pop_aceita = 0x01;
	}
	
  if(num_pop_aceita != 1)
  {
    CarregaPopUp(num_pop_aceita);
    
    if(EstadoRco==ESTA_EXE_CICLO)
    {
      Proc_Pausa_Ciclo(1); 
    }
    EstadoRco = ESTA_ALARME_ATIVO;
	  EstadoRcoAnt = estado_rco_ant;
  }
  else
  {auxPopAtual = 0;}

  	
  	
	return num_pop_aceita;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caixa do alarme
/////////////////////////////////////////////////////////////////////////////////////////////
void PrintAlarme(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    posH = 0x01;
    posL = 0x06;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2alarme[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  //AtualizaTexto(text2alarme,20,posH, posL);
}
////////////////////////////////////////
// Atualiza o campo dos alarmes no display
//
///////////////////////////////////////
void AtuCampoAlarmes(void)
{     
      if(alarTempBaixa[1] && ptrAlarme < 1 && AtualizaAlarme)
      {
         escreve_texto("T1 BAIXA  ", 0X01, 0X06);
         PrintAlarme(1,10,  "T1 BAIXA  ");
         cntNumAlarmes++;
         ptrAlarme = 1;
         AtualizaAlarme = 0;
      }
      if(alarTempAlta[1] && ptrAlarme < 2 && AtualizaAlarme)
      {
         escreve_texto("T1 ALTA   ", 0X01, 0X06);
         PrintAlarme(1,10,  "T1 ALTA   ");
         cntNumAlarmes++;         
         ptrAlarme = 2;
         AtualizaAlarme = 0;
      }
      if(alarTempSensor[1] && ptrAlarme < 3 && AtualizaAlarme)
      {
         escreve_texto("T1 SENSOR ", 0X01, 0X06);
         PrintAlarme(1,10,  "T1 SENSOR ");
         cntNumAlarmes++;
         ptrAlarme = 3;
         AtualizaAlarme = 0;
      }
      if(alarTempBaixa[2] && ptrAlarme < 4 && AtualizaAlarme)
      {
         escreve_texto("T2 BAIXA  ", 0X01, 0X06);
         PrintAlarme(1,10,  "T2 BAIXA  ");
         cntNumAlarmes++;
         ptrAlarme = 4;
         AtualizaAlarme = 0;
      }
      if(alarTempAlta[2] && ptrAlarme < 5 && AtualizaAlarme)
      {
         escreve_texto("T2 ALTA   ", 0X01, 0X06);
         PrintAlarme(1,10,  "T2 ALTA   ");
          cntNumAlarmes++;
         ptrAlarme = 5;
         AtualizaAlarme = 0;
      }
      if(alarTempSensor[2] && ptrAlarme < 6 && AtualizaAlarme)
      {
         escreve_texto("T2 SENSOR ", 0X01, 0X06);
         PrintAlarme(1,10,  "T2 SENSOR ");
         cntNumAlarmes++;
         ptrAlarme = 6;
         AtualizaAlarme = 0;
      }
      if(alarTempBaixa[3] && ptrAlarme < 7 && AtualizaAlarme)
      {
         escreve_texto("T3 BAIXA  ", 0X01, 0X06);
         PrintAlarme(1,10,  "T3 BAIXA  ");
         cntNumAlarmes++;
         ptrAlarme = 7;
         AtualizaAlarme = 0;
      }
      if(alarTempAlta[3] && ptrAlarme < 8 && AtualizaAlarme)
      {
         escreve_texto("T3 ALTA   ", 0X01, 0X06);
         PrintAlarme(1,10,  "T3 ALTA   ");
         cntNumAlarmes++;
         ptrAlarme = 8;
         AtualizaAlarme = 0;
      }
      if(alarTempSensor[3] && ptrAlarme < 9 && AtualizaAlarme)
      {
         escreve_texto("T3 SENSOR ", 0X01, 0X06);
         PrintAlarme(1,10,  "T3 SENSOR ");
         cntNumAlarmes++;
         ptrAlarme = 9;
         AtualizaAlarme = 0;
      }
      if(alarSobe && ptrAlarme < 10 && AtualizaAlarme)
      {
         escreve_texto("ERRO SOBE ", 0X01, 0X06);
         PrintAlarme(1,10,  "ERRO SOBE ");
         cntNumAlarmes++;
         ptrAlarme = 10;
         AtualizaAlarme = 0;
      }
      if(alarDesce && ptrAlarme < 11 && AtualizaAlarme)
      {
         escreve_texto("ERRO DESCE", 0X01, 0X06);
         PrintAlarme(1,10,  "ERRO DESCE");
         cntNumAlarmes++;
         ptrAlarme = 11;
         AtualizaAlarme = 0;
      }
      if(alarGiro && ptrAlarme < 12 && AtualizaAlarme)
      {
         escreve_texto("ERRO GIRO ", 0X01, 0X06);
         PrintAlarme(1,10,  "ERRO GIRO ");
         cntNumAlarmes++;
         ptrAlarme = 12;
         AtualizaAlarme = 0;
      }
      if(alarSensor && ptrAlarme < 13 && AtualizaAlarme)
      {
         escreve_texto("SENSOR MOV", 0X01, 0X06);
         PrintAlarme(1,10,  "SENSOR MOV");
         cntNumAlarmes++;
         ptrAlarme = 13;
         AtualizaAlarme = 0;
      }
      
      if((numAlarmes <= cntNumAlarmes) || (numAlarmesAnt!=numAlarmes))
      {
        cntNumAlarmes = 0;
        ptrAlarme = 0;
        numAlarmesAnt = numAlarmes;
      }

      if(numAlarmes == 0)
      {
        escreve_texto("SEM ALARME", 0X01, 0X06);
        PrintAlarme(1,10,  "SEM ALARME");
      }
      
      
      
}
////////////////////////////////////////
// Atualiza a Barra de Informa��es Inferior
//
///////////////////////////////////////
void AtuBarraInfo(void)
{  
                  
   if(EstadoRco == ESTA_EXE_CICLO)
	 {
      escreve_texto(" Aguarde final do programa, pause ou encerre    ", 0X01, 0X1A);
	 }
	 if(EstadoRco == ESTA_MANUAL)
	 {
	    escreve_texto(" Escolha a opcao de movimentacao do carrossel.  ", 0X01, 0X1A);
	 }
	 if(EstadoRco == ESTA_HOME)
	 {
	   escreve_texto (" Escolha a opcao de execucao ou idioma.        ", 0X01, 0X1A); 
	 }
	 if(EstadoRco == ESTA_DEF_RETARDO)
	 {
	   escreve_texto (" Escolha a data de retardo do ciclo.           ", 0X01, 0X1A); 
	 }
	 
	 
	 
   //if (off_set_idioma == 0) escreve_texto("Ativos 1234567890 tda", 0X01, 0x06);

}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caixa da Temperatura 3
/////////////////////////////////////////////////////////////////////////////////////////////

void DispCiclo(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x00;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2ciclo[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  //Para atualizar o texto mas n�o escrever no display em momento errado
	  if(EstadoRco==ESTA_EXE_CICLO)
	  {
	      AtualizaTexto(text2ciclo,15,posH, posL);  
	  }
	  
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caneca
/////////////////////////////////////////////////////////////////////////////////////////////
void DispCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x35;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2caneca[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  if(EstadoRco==ESTA_EXE_CICLO || EstadoRco == ESTA_MANUAL)
	  {
	    AtualizaTexto(text2caneca,15,posH, posL);
    }     
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caneca
/////////////////////////////////////////////////////////////////////////////////////////////
void DispTempoCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x49;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2tcaneca[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  if(EstadoRco==ESTA_EXE_CICLO)
	  {
	    AtualizaTexto(text2tcaneca,15,posH, posL);
	  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caneca
/////////////////////////////////////////////////////////////////////////////////////////////
void DispProxCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x5D;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2pcanecaciclo[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  if(EstadoRco==ESTA_EXE_CICLO)
	  {
	    AtualizaTexto(text2pcanecaciclo,15,posH, posL);
	  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caneca
/////////////////////////////////////////////////////////////////////////////////////////////
void DispTempoProxCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x71;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2tproxcaneca[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  if(EstadoRco==ESTA_EXE_CICLO)
	  {
	    AtualizaTexto(text2tproxcaneca,15,posH, posL);
	  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caneca
/////////////////////////////////////////////////////////////////////////////////////////////
void DispAcao(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x71;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2acao[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  
}
////////////////////////////////////////
// Atualiza Campo Ciclo
//
///////////////////////////////////////
void AtuCampoCiclo(void)
{

     
}
////////////////////////////////////////
// Atualiza Campo estado
//
///////////////////////////////////////
void AtuCampoEstado(void)
{
   unsigned char em_movimento=0;
   
   if((posCarrossel == CARRO_ERRO) || alarDesce || alarGiro || alarSobe)
   {
      if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
	    {   
	        escreve_texto("ERRO MOVI ", 0X01, 0X4A);
          escreve_texto("ERRO MOVI ", 0X02, 0X6C);
	    }
      DispAcao(1, 10,  "ERRO MOVI ");
      
   }
   else
   {
         if(Carrossel) 
         {
            em_movimento=1;
            
            if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	      {
              escreve_texto("Girando        ", 0X01, 0X4A);
              escreve_texto("Girando        ", 0X02, 0X6C);
    	      }
            DispAcao(1, 15,  "Girando        ");
         }  
         if(Sobe_cesto) 
         {
            em_movimento=1;
            if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	      {
              escreve_texto("Subindo        ", 0X01, 0X4A);
              escreve_texto("Subindo        ", 0X02, 0X6C);
    	      }
            DispAcao(1, 15,  "Subindo        ");        
         }
         if(Desce_cesto)
         {
            em_movimento=1;
            if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	      {
              escreve_texto("Descendo       ", 0X01, 0X4A);
              escreve_texto("Descendo       ", 0X02, 0X6C);
    	      }
            DispAcao(1, 15, "Descendo       ");
         }
         if(Vibrador) 
         {
            em_movimento=1;
            if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	      {
              escreve_texto("Agitando       ", 0X01, 0X4A);
              escreve_texto("Agitando       ", 0X02, 0X6C);
    	      }
            DispAcao(1, 15,  "Agitando       ");
         }
       
         if(!em_movimento)
         {
           if(posCarrossel == CARRO_ALTO) //Carrossel no alto
           {
             if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	       {
                escreve_texto("Alto           ", 0X01, 0X4A);
                escreve_texto("Alto           ", 0X02, 0X6C);
    	       }
             DispAcao(1, 15,  "Alto           ");
           }
           if(posCarrossel == CARRO_BAIXO) //Carrossel no baixo
           {
              if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	        {
                escreve_texto("Baixo          ", 0X01, 0X4A);
                escreve_texto("Baixo          ", 0X02, 0X6C);
    	        }
              DispAcao(1, 15,  "Baixo          ");
           }
           if(posCarrossel == CARRO_MEIO) //Carrossel no meio
           {
              if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	        {
                escreve_texto("Meio           ", 0X01, 0X4A);
                escreve_texto("Meio           ", 0X02, 0X6C);
    	        }
              DispAcao(1, 15,  "Meio           ");
           }
           if(!Carrossel && !SENSOR_GIRO_CARROSSEL)//Meio do giro 
           {
              if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
    	        {
                escreve_texto("Meio giro      ", 0X01, 0X4A);
                escreve_texto("Meio giro      ", 0X02, 0X6C);
    	        }
              DispAcao(1, 15,  "Meio giro      ");
           }
         }
   }
}

////////////////////////////////////////
// Atualiza os campos da tela de execu��o 
// do Ciclo                                       
///////////////////////////////////////
void AtuCamposExeCiclo(void)
{
       //Atualiza Campo Ciclo
                    
         if(EstadoCiclo == 0)
         {
            DispCiclo(1, 13,  "Manual       "); 
         }
         else
         {
            ConverteAscii(NumCiclo+1); 
            DispCiclo(1, 2,  Msg+2);
         }
         

         if(EstadoCiclo == ESTA_CICLO_PAUSADO)
         {
 
            DispCiclo(3, 11,  " Pausado   "); 
         }
         if(EstadoCiclo == ESTA_CICLO_PARADO)
         {
            DispCiclo(3, 11,  " Parado    "); 
         }
         if(EstadoCiclo == ESTA_CICLO_AGUARDA)
         {
            DispCiclo(3, 11,  " Aguardando"); 
         }         
         if(EstadoCiclo > ESTA_CICLO_AGUARDA && EstadoCiclo !=ESTA_CICLO_ERRO)
         {
           DispCiclo(3, 11,  " Executando");
         }
         else if(EstadoCiclo == ESTA_CICLO_ERRO)
         {
            DispCiclo(3, 11,  " ERRO      ");          
         }

         
                  
       //Atualiza Caneca Atual
         ConverteAscii(posCaneca);
         DispCaneca(1, 2,  Msg+2);
       //Atualiza tempos da caneca atual
         if(EstadoCiclo == ESTA_CICLO_AGITA)
         {
            ConverteAscii(Tempo_de_sub_estagio - TempoAgitando-1); 
            DispTempoCaneca(1, 2,  Msg+2);
            DispTempoCaneca(3, 1,  ":");
            ConverteAscii(59-Seg);
            DispTempoCaneca(4, 2,  Msg+2);                
         } 
         else if(EstadoCiclo == ESTA_CICLO_SOBE || SubEstadoCiclo==2)//esta no estado de sobe SubEstadoCiclo == j� subiu
         {
            
            DispTempoCaneca(1, 2,  "00");
            DispTempoCaneca(3, 1,  ":");
            ConverteAscii(TempoEscorreDefinido - TempoEscorrendo); 
            DispTempoCaneca(4, 2,  Msg+2);
         }
         else
         {
            ConverteAscii(0);
            DispTempoCaneca(1, 2,  Msg+2);
            DispTempoCaneca(3, 1,  ":");
            ConverteAscii(0);
            DispTempoCaneca(4, 2,  Msg+2);
         }
       //Atualiza pr�xima caneca         
         if(posCaneca==MAX_CANECAS)
         {
             DispProxCaneca(1, 2, "0");
         }
         else
         {
             ConverteAscii(posCaneca + 1);
             DispProxCaneca(1, 2,  Msg + 2);
         }
       //Atualiza tempo da proxima caneca
         ConverteAscii(Tempo_prox_sub_estagio);
         DispTempoProxCaneca(1, 2,  Msg+2);
         DispTempoProxCaneca(3, 1,  ":");
         DispTempoProxCaneca(4, 2,  "00");
          
       //  
         AtuTemperaturas();
         AtuCampoEstado();
}

////////////////////////////////////////
// Atualiza as Temperaturas
//
///////////////////////////////////////
void AtuTemperaturas(void)
{  
     if(alarTempSensor[1])
     {
        DispTemp1(1, 4,  "SENS");  
     }
     else
     {
        ConverteAscii(tempT1); 
        Msg[0] = Msg[1];
        Msg[1] = Msg[2];
        Msg[2] = '.';
        Msg[3] = Msg[3];
        DispTemp1(1, 4,  Msg);  
     }
     if(alarTempSensor[2])
     {
        DispTemp2(1, 4,  "SENS");  
     }
     else
     {
       ConverteAscii(tempT2);
       Msg[0] = Msg[1];
       Msg[1] = Msg[2];
       Msg[2] = '.';
       Msg[3] = Msg[3];
       DispTemp2(1, 4, Msg);
     }
     if(alarTempSensor[3])
     {
        DispTemp3(1, 4,  "SENS");  
     }
     else
     {
       ConverteAscii(tempT3);
       Msg[0] = Msg[1];
       Msg[1] = Msg[2];
       Msg[2] = '.';
       Msg[3] = Msg[3];         
       DispTemp3(1, 4, Msg);
     }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Atualiza numero de casas decimais A5 5A 05 82 SP:H SP:L+03 nDec 00
// nDec: 00 sem casas decimais
//       01 uma casa  decimal ... etc                                               
/////////////////////////////////////////////////////////////////////////////////////////////
void AtualizaCasaDec(char posH, char posL, char nDec){       

      TxChar2(0xA5);
      TxChar2(0x5A);
      TxChar2(0x05);
      TxChar2(0x82);
      TxChar2(posH);
      TxChar2(posL + 0x06);
      TxChar2(nDec);
      TxChar2(0x00);  
      
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caixa da Temperatura 1
/////////////////////////////////////////////////////////////////////////////////////////////

void DispTemp1(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x15;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2temp1[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
    if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
	  {
	    AtualizaTexto(text2temp1,6,posH, posL);
	  }
}        
 /////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caixa da Temperatura 2
/////////////////////////////////////////////////////////////////////////////////////////////

void DispTemp2(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x1B;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2temp2[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
	  {
	    AtualizaTexto(text2temp2,6,posH, posL);
	  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina para escrever na caixa da Temperatura 3
/////////////////////////////////////////////////////////////////////////////////////////////

void DispTemp3(unsigned char pos, unsigned char Nchar,  char *msgPrint)
{
	  unsigned char i=0;
	  unsigned char posFim = pos + Nchar;
    unsigned char tam;
    unsigned char posH,posL;
    
    posH = 0x04;
    posL = 0x21;
    tam = Nchar;
    
	  for(i = (pos-1); i < ((pos-1) + tam);i++)
	  {
		  if(i < posFim-1)
		  {
		    text2temp3[i] = *msgPrint;
		  }
		  msgPrint++;
	  }
	  if(EstadoRco==ESTA_EXE_CICLO || EstadoRco==ESTA_MANUAL)
	  {
	    AtualizaTexto(text2temp3,6,posH, posL);
	  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
//Atualiza o Estado dos Botoes e icones
//Deve ser chamado periodicamente                                            
/////////////////////////////////////////////////////////////////////////////////////////////
void DisplayIcones(void)
{
    if(atualizaIcon)
    {
        if(EstRco ==  EST_OPERAR)
        {
            if(StOutInterna){AtualizaIcone(ICO_INTERNA,0x01);}
            else{AtualizaIcone(ICO_INTERNA,0x00);} 
            if(StOutUV){AtualizaIcone(ICO_UV,0x01);}
            else{AtualizaIcone(ICO_UV,0x00);}
            if(StOutOzonio){AtualizaIcone(ICO_OZONIO,0x01);}
            else{AtualizaIcone(ICO_OZONIO,0x00);}
            
            if(StTRIM){AtualizaIcone(ICO_TRIM,0x01);AtualizaIcone(ICO_SEC,0x00);}
            else{AtualizaIcone(ICO_TRIM,0x00);AtualizaIcone(ICO_SEC,0x01);}
            
                 
            atualizaIcon = 0;
        }
        else if(EstRco == EST_CONFIG)
        {
          
           atualizaIcon = 0;
        }
        else if(EstRco == EST_CONFIG)
        {
          
           atualizaIcon = 0;
        }
        else if(EstRco == EST_PROG_AUTO)
        {
         // temp = 0x13;
         // nbH = (temp >> 4)&0x0F;
         // nbL = temp & 0x0F;
          
           AtualizaIcone(0x53,nbH);//semana
           AtualizaIcone(0x52,nbL);//funcao
           atualizaIcon = 0;
        }
        
    }    
}
/////////////////////////////////////////////////////////////////////////////////////////////
//Atualiza o Valores num�ricos do display
//Deve ser chamado periodicamente                                            
/////////////////////////////////////////////////////////////////////////////////////////////
void DisplayValores(void)
{
  if((EstRco ==  MANUTENCAO) | (EstRco ==  EST_OPERAR2) | (EstRco ==  EST_MANUAL)|(EstRco == EST_AJ_OFF_SET))
  {
    AtualizaCampo(Temperatura_Lida_1     , valorADC[1] + off_set_temperatura1);    
    AtualizaCampo(Temperatura_Lida_2     , valorADC[2] + off_set_temperatura2);  
    AtualizaCampo(Temperatura_Lida_3     , valorADC[3] + off_set_temperatura3);
    AtualizaCampo(Temperatura_Lida_4     , valorADC[4] + off_set_temperatura4);
    
    
          
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
//Atualiza os elementos do display
//Deve ser chamado periodicamente                                            
/////////////////////////////////////////////////////////////////////////////////////////////
void Display(void)
{
   if(flagAtualizaDisp)
   {
    //DisplayIcones();
    DisplayValores();
    flagAtualizaDisp = 0;
   }
}

