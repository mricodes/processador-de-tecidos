/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
/ Definicoes para manipulacao da memoria flash
===============================================================================================*/
#ifndef _doonstack
#define _doonstack
#ifdef __cplusplus
extern "C" { /* our assembly functions have C calling convention */
  #endif
  /* prototype for DoOnStack routine */
  void DoOnStack(void);
  /* prototype for FlashErase routine */
  /* Page Erase command */
  void FlashErase(unsigned char *);
  /* prototype for FlashProg routine */
  /* Byte Program command */
  void FlashProg(unsigned char *, unsigned char);
  void EscreveDados(void);
  #ifdef __cplusplus
}
#endif
#endif /* _doonstack */