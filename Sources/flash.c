/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
/ Rotinas para manipulacao da memoria flash
===============================================================================================*/
#include "masterheader.h"


byte res;

/////////////////////////////////////////////////////////////////////////////////////////////
// L� um endere�o de mem�ria FLASH                                                 
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char ReadFlash(unsigned int endereco)
{
unsigned char dado;
   asm{
      ldhx endereco
      lda 0,X
    	sta dado
   	}
return dado;
/*
  unsigned char *ponteiro;    
  ponteiro = (char*) endereco; //guarda o endere�o da mem�ria em um ponteiro
  return (*ponteiro); //retorna o valor armazenado no endere�o indicado pelo ponteiro
  */
}
/////////////////////////////////////////////////////////////////////////////////////////////
//Escreve em um endere�o da mem�ria FLASH
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char WriteFlash(unsigned int endereco, unsigned char dado, unsigned char flagPrimeiroByte)
{
unsigned char *ponteiro;  
ponteiro = (char*) endereco; //guarda o endere�o da mem�ria em um ponteiro
	if(flagPrimeiroByte)
	{
      FlashErase(ponteiro);  
  		if(FSTAT_FACCERR)
  		{
  		FSTAT_FACCERR = 1;
  		} //Verifica e apaga flag de erro de acesso a FLASH
	}
FlashProg(ponteiro,dado);  //Chama a fun��o de programa��o da FLASH
	if(FSTAT_FACCERR || FSTAT_FPVIOL)
	{
	return(1);
	}//se houver erro, retorna 1. Caso contr�rio, retorna 0
else{return(0);}
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Apaga uma p�gina na FLASH
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char PageEraseFlash(unsigned int endereco)
{
unsigned char *ponteiro;  
ponteiro = (char*) endereco;//guarda o endere�o da mem�ria em um ponteiro
	if(FSTAT_FACCERR)
	{
	FSTAT_FACCERR = 1;
	}  //Verifica e apaga flag de erro de acesso a FLASH 
FlashErase(ponteiro);     //Chama a fun��o de programa��o da FLASH  
  	if(FSTAT_FACCERR || FSTAT_FPVIOL)
  	{
  	return(1);
  	}//se houver erro, retorna 1. Caso contr�rio, retorna 0
  	else{return(0);}
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina de inicializacao 
// erro = 0 sem erro, erro > 0 ocorreu erro na gravacao de algum dado                                                  
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char AtualizaFlash_UP(void)
{
    unsigned long int i3;   
    unsigned char erro = 0;
    unsigned char chkSum = 0;
    unsigned char auxH = 0;
    unsigned char auxM = 0;
    unsigned char i=0;
    i3 = 0x7000;
    erro = WriteFlash(i3,0xff,1);  //DUMMY Write
    i3++;
    erro += WriteFlash(i3,temperatura_ajustada_T1,0);i3++;
    erro += WriteFlash(i3,temperatura_ajustada_T1 >> 8,0);i3++;
    erro += WriteFlash(i3,temperatura_ajustada_T2,0);i3++;
    erro += WriteFlash(i3,temperatura_ajustada_T2 >> 8,0);i3++;	
    erro += WriteFlash(i3,temperatura_ajustada_T3,0);i3++;
    erro += WriteFlash(i3,temperatura_ajustada_T3 >> 8,0);i3++;	

    return (erro); 
}
 /////////////////////////////////////////////////////////////////////////////////////////////
// Rotina de inicializacao 
// erro = 0 sem erro, erro > 0 ocorreu erro na gravacao de algum dado                                                  
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char AtualizaFlash(void)
{
   delAtualiza = 1;
}
///////////////////////////////////////////////
//  Carrega valores padr�o e fabrica
//
///////////////////////////////////////////////
void CarregaDefault(void)
{
      unsigned int iX,iX1;
      
      Numero_programa_edit = 0;
    
      EstSerial = 0;
      EstRco = 0;
      EstRcoAntPop = 0;
      Sub_estagio_processo = 0;
      
      PtrProtDisp = PtrEnt1=PtrSai1=PtrEnt2=PtrSai2=0;
                
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 0 ] = 60;
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 1 ] = 30;
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 2 ] = 15;
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 3 ] = 1;
      teclaDisp = 0xFF; 

/////Init temperatura
      Flag_PT100_1 = 1;
      Flag_PT100_2 = 1;
      Flag_PT100_3 = 1;
      
      temperatura_ajustada_T1 = 600;
      temperatura_ajustada_T2 = 600;
      temperatura_ajustada_T3 = 600;
      
      Flag_aquecimento = 1;
      
      off_set_temperatura1 = 89-70;
    	off_set_temperatura2 = 66-54;
    	off_set_temperatura3 = -100;
    	
      cntDebBaixo = 0;
      cntDebAlto = 0;
      cntDebPos0 = 0;
      cntDebGiro = 0;

    	
///////Inicializa��o do Timer
//igual a 0xFF timer parado
      TempoEscorrendo = 0xFF;
      TempoTempBaixa1 = 0xFF;
      TempoTempBaixa2 = 0xFF;
      TempoTempBaixa3 = 0xFF;    	
    	TempoSubindo  = 0xFF;
      TempoDescendo = 0xFF;
      TempoGirando  = 0xFF;
      TempoRetPause = 0xFF;
      
      numAlarmesAnt = numAlarmes = 0;
    	
    	auxConfigADC = 0b01000100;
      ADC1SC1 = auxConfigADC;

}


/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina de inicializacao                                                   
/////////////////////////////////////////////////////////////////////////////////////////////
void InitFlash(void)
{
    unsigned char i,j;
    unsigned long int i3;
    if(FSTAT_FACCERR){FSTAT_FACCERR = 1;}
    FCDIV = vFCDIV;  
   
    CarregaValoresFlash();
    
    if(temperatura_ajustada_T1 < 1)//memoria apagada  ou chkSum inconsistente
  	{
    	CarregaDefault();	
	  }
}

void CarregaValoresFlash(void)
{
  unsigned long int i3 = 0x7001;
 
  CarregaDefault();
 
  temperatura_ajustada_T1 = ReadFlash(i3);i3++;
  temperatura_ajustada_T1 += ReadFlash(i3) << 8;i3++;

  temperatura_ajustada_T2 = ReadFlash(i3);i3++;
  temperatura_ajustada_T2 += ReadFlash(i3) << 8;i3++;

  temperatura_ajustada_T3 = ReadFlash(i3);i3++;
  temperatura_ajustada_T3 += ReadFlash(i3) << 8;i3++;
    
}

void valores_padrao_de_fabrica()
{
    AtualizaFlash();
}





