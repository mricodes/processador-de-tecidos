/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
/ Rotina Principal do programa
===============================================================================================*/
#include <hidef.h>       //for EnableInterrupts macro 
#include "derivative.h"  //include peripheral declarations
#include "masterheader.h"//constantes variaveis e prototipos globais 
#include <string.h>
#include <stdlib.h>
//#include <stdio.h>

	      

void main(void)
{
    
    MCU_init();//OK    
    //Inicializa_variaveis();//OK avaliar valores iniciais           
    InitFlash();
    //Flag_aquecimento = 1;//??
     
    for(;;)
    {
	   	  Rco_MRI();//OK
	      TrataSerial();//OK  
	      AtuPosCarro();//Atualizacao Posicao do Carrossel
	      ControleExeCiclo(NumCiclo);
	      ControleAlarmes ();
        if(DelSendNuvem > TEMPO_WEB)
        {
          EnviaDataNuvem();
          DelSendNuvem = 0;   
        }
	      if(flagLeRtc > 10 && EstadoCiclo == ESTA_CICLO_AGUARDA )//Leitura do RTC a cada 10 segundos
	      {
	        LeRtc();
	        flagLeRtc = 0; 
	      }
	  }
}

void Inicializa_variaveis()
{
        unsigned int iX,iX1;
      
      Numero_programa_edit = 0;
    
      EstSerial = 0;
      EstRco = 0;
      EstRcoAntPop = 0;
      Sub_estagio_processo = 0;
      
      PtrProtDisp = PtrEnt1=PtrSai1=PtrEnt2=PtrSai2=0;
                
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 0 ] = 60;
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 1 ] = 30;
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 2 ] = 15;
      for (iX = 0 ; iX < 20; iX++) array_parametros [ iX ][ 3 ] = 1;
      teclaDisp = 0xFF; 

/////Init temperatura
      Flag_PT100_1 = 1;
      Flag_PT100_2 = 1;
      Flag_PT100_3 = 1;
      
      temperatura_ajustada_T1 = 240;
      temperatura_ajustada_T2 = 240;
      temperatura_ajustada_T3 = 240;
      
      Flag_aquecimento = 1;
      
      off_set_temperatura1 = 89-70;
    	off_set_temperatura2 = 66-54;
    	off_set_temperatura3 = -100;
    	
      cntDebBaixo = 0;
      cntDebAlto = 0;
      cntDebPos0 = 0;
      cntDebGiro = 0;

    	
///////Inicializa��o do Timer
//igual a 0xFF timer parado
      TempoEscorrendo = 0xFF;
      TempoTempBaixa1 = 0xFF;
      TempoTempBaixa2 = 0xFF;
      TempoTempBaixa3 = 0xFF;    	
    	TempoSubindo  = 0xFF;
      TempoDescendo = 0xFF;
      TempoGirando  = 0xFF;
      TempoRetPause = 0xFF;
      
      numAlarmesAnt = numAlarmes = 0;
    	
    	auxConfigADC = 0b01000100;
      ADC1SC1 = auxConfigADC;
}

	










