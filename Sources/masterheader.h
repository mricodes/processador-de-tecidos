////////////////////////////////////////////////////////////////////////
// Master Header
// Cabe�alho geral para uso de variaveis e defini��es globais
///////////////////////////////////////////////////////////////////////
#include <MC9S08AC32.h>
#include "doonstack.h"

//Constantes Globais
#define DEMO_NUVEM 0
#define VERSAO   10
#define MaxBufRx 100
#define TEMPOINI 220
#define DEBUG 0
#define vFCDIV 76 // divisor para o FCLK = (BUSCLK) / (8 * 12+1)   FCLK = 20Mhz / (112) = 192,3 Khz
                  //  O valor m�ximo n�o pode ultrapassar 200 Khz

#define MEDIA_ADC 64

#define CMD_NUVEM_AP 10
#define TEMPO_WEB 3


#define MAX_CANECAS 12
#define MAX_TEMP_1 100
#define MAX_TEMP_2 100
#define MAX_TEMP_3 100

#define MIN_TEMP_1 100
#define MIN_TEMP_2 100
#define MIN_TEMP_3 100

#define ALAR_SENSOR_1_MIN 50
#define ALAR_SENSOR_2_MIN 50
#define ALAR_SENSOR_3_MIN 50

#define ALAR_SENSOR_1_MAX 1000
#define ALAR_SENSOR_2_MAX 1000
#define ALAR_SENSOR_3_MAX 1000

#define T_TEMP_BAIXO_1 120
#define T_TEMP_BAIXO_2 120
#define T_TEMP_BAIXO_3 120
#define TEMPO_ESCORRIMENTO 30

#define CNT_DEB_GIRO  2
#define CNT_DEB_ALTO  2
#define CNT_DEB_BAIXO 2
#define CNT_DEB_ZERO  2 

#define BUZZER_EXT		PTBD_PTBD2
#define LED_EXT		    PTBD_PTBD1

#define TEMP_RET_PAUSE 45 //segundos

#define TEMP_ERRO_SOBE  15 //segundos
#define TEMP_ERRO_DESCE 15 //segundos
#define TEMP_ERRO_GIRO  15 //segundos

//Pop-ups para alarme
#define POP_ACET_ALARM_BAIXA_1	50//0x10
#define POP_ACET_ALARM_BAIXA_2	51//0x10
#define POP_ACET_ALARM_BAIXA_3	52//0x10
#define POP_ACET_ALARM_ALTA_1	  53//0x10
#define POP_ACET_ALARM_ALTA_2	  54//0x10
#define POP_ACET_ALARM_ALTA_3	  55//0x10
#define POP_ACET_ALARM_SENSOR_1	56//0x10
#define POP_ACET_ALARM_SENSOR_2	57//0x10
#define POP_ACET_ALARM_SENSOR_3	58//0x10
#define POP_ACET_ALARM_SOBE		  59//0x10
#define POP_ACET_ALARM_DESCE	  60//0x10
#define POP_ACET_ALARM_GIRO		  61//0x10
#define POP_ACET_ALARM_SENS		  62//0x10
#define CONFIRMA_ACEITA_ALARME  0X97//0x10
 
///////////////////////////////
//Definicao de Estados
#define ESTA_INTRO      0
#define ESTA_HOME       1
#define ESTA_MANUAL     2
#define ESTA_SEL_CICLO  3
#define ESTA_EXE_CICLO  4
#define ESTA_EDIT_CICLO 5
#define ESTA_EDIT_TEMPE 6
#define ESTA_PRESET     7
#define ESTA_SOL_SENHA  8
#define ESTA_TIPO_CICLO 9
#define ESTA_DEF_RETARDO 10
#define ESTA_PARA_CICLO   11 
#define ESTA_ALARME_ATIVO 12
#define ESTA_AJU_TEMP     13
#define ESTA_TESTE_MANU   14
////////////////////////////////
//Estado dos ciclos
#define ESTA_CICLO_PARADO	  1
#define ESTA_CICLO_PAUSADO 	2
#define ESTA_CICLO_AGUARDA	3
#define ESTA_CICLO_TESTA	  4
#define ESTA_CICLO_GIRA	    5
#define ESTA_CICLO_SOBE		  6
#define ESTA_CICLO_AGITA	  7
#define ESTA_CICLO_DESCE	  8
#define ESTA_CICLO_FIM		  9
#define ESTA_CICLO_ERRO     11

///////////////////////////////
//Definicao das Telas
#define TELA_INTRO        0
#define TELA_HOME         3
#define TELA_PRESET       6
#define TELA_AJU_TEMP     21
#define TELA_SEL_CICLO    9
#define TELA_EXE_CICLO    12
#define TELA_EDIT_CICLO   15
#define TELA_MANUAL       18
#define TELA_DEF_RETARDO  54
#define TELA_TESTE_MANU   27

///////////////////
//POPUP
#define POP_TIPO_CICLO 7
#define POP_PARA_CICLO 1

#define BOT_OK_POP    0x99
///////////////////////////////
//Definicao dos botoes
//Botoes Tela HOME
#define BOT_MANUAL        0xF1
#define BOT_PRESET        0xF2
#define BOT_INIT_CICLO    0xF0
#define BOT_SOBRE         0xF3


//Botoes Tela MANUAL
#define BOT_SOBE          0x94
#define BOT_DESCE         0x95
#define BOT_GIRA          0x96
#define BOT_ZERAR         0x98
//Botoes Gerais
#define BOT_VOLTAR        0x97
#define BOT_HOME          0x0A
#define BOT_SALVAR        0xAD
#define BOT_ABORTAR_POP   0x97
#define BOT_RETOMAR_POP   0xCF
#define BOT_MANUAL_POP    0xD0
#define BOT_PAUSAR        0x32
//BOTOES PRESET
#define BOT_FABRICA       0x19
#define BOT_AJU_TEMP		  0X17	
#define BOT_TESTE_MANU    0x1B

//BOTOES Ajuste Temperatu
#define BOT_DEC_T1	0X98	//152
#define BOT_INC_T1	0X99	//153
#define BOT_DEC_T2	0X9A	//154
#define BOT_INC_T2	0X9B	//155
#define BOT_DEC_T3	0X9C	//156
#define BOT_INC_T3	0X9D	//157

//#define BOT_SALVA_AJU 0xAD

//CAMPO AJUSTE RETARDO

#define CAMPO_AJU_DIA_RET 	0x0140
#define CAMPO_AJU_MES_RET	  0x0142
#define CAMPO_AJU_ANO_RET	  0x0144
#define CAMPO_AJU_HORA_RET	0x0146
#define CAMPO_AJU_MIN_RET	  0x0148

//BOTOES AJUSTE RETARDO
#define BOT_ENTER_RET 0xAC
#define BOT_DEC_DIA_RET  	0xA9
#define BOT_INC_DIA_RET		0xA8
#define BOT_DEC_MES_RET		0xA6
#define BOT_INC_MES_RET		0xA5
#define BOT_DEC_ANO_RET		0xA3
#define BOT_INC_ANO_RET		0xA2
#define BOT_DEC_HORA_RET	0xA1
#define BOT_INC_HORA_RET	0xAB
#define BOT_DEC_MIN_RET		0xA7
#define BOT_INC_MIN_RET		0xAA

#define TEMP_LIDA_DISP1		0X1A
#define TEMP_LIDA_DISP2		0X1C
#define TEMP_LIDA_DISP3		0X1E


#define POS_CANECA_DISP         0X0E
#define POS_CANECA_EXE_DISP     0X14
#define POS_PROX_CANECA_DISP    0X1A
#define POS_TEMPO_CANECA_DISP   0X015E
#define POS_TEMPO_PROX_DISP     0X0186

#define CAMPO_AJU_T1		0XF6
#define CAMPO_AJU_T2		0XF8
#define CAMPO_AJU_T3		0XFA

#define CAMPO_ADC_T1		0X1A
#define CAMPO_ADC_T2		0X1C
#define CAMPO_ADC_T3		0X1E


#define BOT_CANCELA_POP   0X97
#define BOT_ATRASO_POP    0XCE
#define BOT_IMEDIATO_POP  0XCD

//#define RECUO 30 // total de micras para recuo

//Definicao de Estados
#define EST_INICIAR 	    	0x01
#define EST_OPERAR 	  	    0x02
#define EST_PROGRAMAR 	    0x04
#define EST_MANUAL  		    0x05
#define EST_EDITAR_PROG     0x06
#define AJUSTAR_T_PARAFINA  0x07
#define MANUTENCAO	      	0x08
#define EST_SENHA			      0X09
#define CIRRIGE_TEMPO		    0X0A
#define DATA_DE_EXECUCAO	  0X0B
#define HORA_DE_EXECUCAO	  0X0C
#define MINUTO_DE_EXECUCAO	0X0D
#define EST_OPERAR2 		    0x12
#define EST_CONFIG 		      0x14
#define EST_PROG_DESCON   	0x16
#define EST_PROG_PRESET   	0x17
#define EST_CONF_IDIOMA   	0x18
#define EST_SENHA_TROCA	  	0X19
#define EST_CONF_CAL       	0x1A
#define EST_CONF_TESTES    	0x1B
#define EST_CONF_FABRICA   	0x1C
#define EST_CONF_SENHA     	0x1D
#define EST_CONF_SOBRE     	0x1E
#define EST_PROGRAMAR2 		0x99
#define EST_ERRO_TEMP_EXTREMA 0x9A //154
#define EST_SENHA_CONF    	0x9B //155
#define EST_PROG_AUTO		0X10 //
#define EST_AJ_OFF_SET		0X9C //156
#define EST_ERRO_DESCIDA      0x9d //157
#define EST_ERRO_GIRO		0X9E //158
#define EST_ERRO_SENS_TEMPER  0X9F //159
#define EST_ERRO_SUBIDA		0XA0 //160
#define EST_ERRO_TEMP_BAIXA   0XA1 //161


//Definicao das Telas				//PICID
#define Tela_Iniciar				03  
#define Tela_Configurar				6  



#define Tela_Manual				18
#define Tela_sobre				51
#define Tela_Seleciona_programa_editar	9 
#define Tela_Ajusta_programas			15	

#define Tela_Seleciona_programa_executar	13 	//13
#define Tela_de_execucao			12	//16
#define Tela_Seleciona_hora			19  	//19
#define Tela_seleciona_minutos		16	//22


#define Tela_Temperatura_parafina		21	//31
#define Tela_Seleciona_dia_da_execucao	34	//34
#define Tela_Digita_senha			24	//37
#define Tela_manutencao				27  	//40
#define Tela_acerta_hora			54  	//43
#define Tela_erro_temp_extrema		67
#define Tela_erro_mov_descida			79
#define Tela_erro_mov_giro			82
#define Tela_erro_sensor_temperatura      85
#define Tela_erro_mov_subida			88
#define Tela_erro_temp_baixa			70


#define Tela_ajusta_off_set			100






//Display

#define POPFIMCURSO   				0x01
#define	Pop_up_1				0X02	 	  			//popup que seleciona qual modo de uso ser� usado.
//#define	Pop_up_2				0X03 	  				//popup que sconfirma uma acao.

#define pop_up_data_invalida			0X04
#define pop_up_move_carrossel			0X05


#define     Pop_up_pausa_P        			 1
#define     Pop_confirma_P        			 4
#define     Pop_exec_prog_P        			 7
#define     Pop_cofig_ori_P        			10
#define     Pop_senha_alter_P        		13
#define     Pop_senha_erro_P        		16
#define     Pop_data_invalida_P        		19
#define     Pop_zerando_carrossel_P       	22
#define     Pop_temp_alta_P        			25
#define     Pop_confirm_zero_carr_P       	28
#define     Pop_temp_baixa_P        		31
#define     Pop_erro_descida_P        		34
#define     Pop_erro_subida_P       	 	37
#define     Pop_erro_giro_P        			40
#define     Pop_temp_baixa_confirm_movimento_P  43
#define     Poop_confirm_config_default_P       46
#define     Pop_config_salva_P        		49



//Icones e Botoes
#define Programa_executado			0xCA
#define Campo_posicao				    0x0E
#define Campo_proxima_posicao			0x14
#define Programa_editado			0xCA
#define Visor_Temperatura_aj_1		0XF6
#define Visor_Temperatura_aj_2		0XF8
#define Visor_Temperatura_aj_3		0XFA
#define Visor_Temperatura_aj_4		0XFC

#define	Opto_sensor_1			0X0128
#define	Opto_sensor_2			0X012A
#define	Opto_sensor_3			0X012C
#define	Opto_sensor_4			0X012E
#define	entrada_opto_1		0X0130
#define	entrada_opto_2		0X0132



#define Temperatura_Lida_1		0X1A
#define Temperatura_Lida_2		0X1C
#define Temperatura_Lida_3		0X1E
#define Temperatura_Lida_4		0X20
#define Corrente_Motor_Lida_1		0X2C

#define	Milhar_senha			0X00FE
#define	Centena_senha			0X0100
#define	Dezena_senha			0X0102
#define	Unidade_senha			0X0104
#define	Mensagem_senha			0X0112



#define	Campo_dia				0X0140
#define	Campo_mes				0X0142
#define	Campo_ano				0X0144
#define	Campo_hora				0X0146
#define	Campo_minutos			0X0148

#define	posicao_carrossel2	 	0X014A	//tela Manual

#define	Tempo_atual				0X015E
#define	Tempo_futuro			0X0186
#define	hora_de_ativacao			0X026C




#define	Semente_senha			0X028E

#define	Estado_futuro2			0X0290
		  
#define	Estado_motor_1			0X02A4
#define	Estado_motor_2			0X0014
#define	Estado_motor_3			0X02B8
		  
		  

#define ICO_TRIM      				0x51
#define ICO_SEC       				0x52
#define ICO_INTERNA   				0x50
#define ICO_UV        				0x53
#define ICO_OZONIO    				0x54
#define ICO_STAND     				0x55
#define ICO_DEFROST   				0x56
#define ICO_BLOCK     				0x57
#define ICO_VOLANTE   				0x58


/////////////////////////////////////////////////////////////////////////////////////////////Macros Bot�es
////////Menu Lateral
#define BTIniciar					0X0A	//10
#define BTOperacao				0X0B	//11
#define BTConfiguracao				0X0C	//12
#define BTProgramar				0X0D	//13
#define BTManual_lateral			0X0E	//14
///////Tela iniciar

					
#define BTProgramas				0X0F	//15
#define BTManual_iniciar			15	//16
#define BTPreset					16	//16
#define BTPortugues				18	//18
#define BTEspanhol				19	//19
#define BTIngles					20	//20
///////Tela config						
#define BTEditar_programas			0X15	//21
#define BTData_hora				0X16	//22

#define BTSobre					    0X18	//24
#define BTConfiguracao_de_fabrica		0X19	//25
#define BTAlterar_Senha				0X1A	//26
#define BTTestes_manutencao			0X1B	//27
// tela seleciona programa para executar e editar							
#define BTPrograma1				0X1C	//28
#define BTPrograma2				0X1D	//29
#define BTPrograma3				0X1E	//30
#define BTPrograma4				0X1F	//31
#define BTPrograma5				0X20	//32
#define BTPrograma6				0X21	//33
#define BTPrograma7				0X22	//34
#define BTPrograma8				0X23	//35
#define BTPrograma9				0X24	//36
#define BTPrograma10				0X25	//37
#define BTPrograma11				0X26	//38
#define BTPrograma12				0X27	//39
#define BTPrograma13				0X28	//40
#define BTPrograma14				0X29	//41
#define BTPrograma15				0X2A	//42
#define BTPrograma16				0X2B	//43
#define BTPrograma17				0X2C	//44
#define BTPrograma18				0X2D	//45
#define BTPrograma19				0X2E	//46
#define BTPrograma20				0X2F	//47
// tela de execucao							
#define BTExecuta					0X30	//48
#define BTPara					0X31	//49
#define BTPausa					0X32	//50
// tela seleciona dia							
#define BTDia_1					0X33	//51
#define BTDia_2					0X34	//52
#define BTDia_3					0X35  //53		
#define BTDia_4					0X36	//54
#define BTDia_5					0X37	//55
#define BTDia_6					0X38	//56
#define BTDia_7					0X39	//57
#define BTDia_8					0X3A	//58
#define BTDia_9					0X3B	//59
#define BTDia_10					0X3C	//60
#define BTDia_11					0X3D	//61
#define BTDia_12					0X3E	//62
#define BTDia_13					0X3F	//63
#define BTDia_14					0X40	//64
#define BTDia_15					0X41	//65
#define BTDia_16					0X42	//66
#define BTDia_17					0X43	//67
#define BTDia_18					0X44	//68
#define BTDia_19					0X45	//69
#define BTDia_20					0X46	//70
#define BTDia_21					0X47	//71
#define BTDia_22					0X48	//72
#define BTDia_23					0X49	//73
#define BTDia_24					0X4A	//74
#define BTDia_25					0X4B	//75
#define BTDia_26					0X4C	//76
#define BTDia_27					0X4D	//77
#define BTDia_28					0X4E	//78
#define BTDia_29					0X4F	//79
#define BTDia_30					0X50	//80
#define BTDia_31					0X51	//81
#define BTDia_32					0X52	//82
#define BTDia_33					0X53	//83
#define BTDia_34					0X54	//84
#define BTDia_35					0X55	//85
// tela seleciona hora							
#define BTSeleciona_hora_0			0X56	//86
#define BTSeleciona_hora_1			0X57	//87
#define BTSeleciona_hora_2			0X58	//88
#define BTSeleciona_hora_3			0X59	//89
#define BTSeleciona_hora_4			0X5A	//90
#define BTSeleciona_hora_5			0X5B	//91
#define BTSeleciona_hora_6			0X5C	//92
#define BTSeleciona_hora_7			0X5D	//93
#define BTSeleciona_hora_8			0X5E	//94
#define BTSeleciona_hora_9			0X5F	//95
#define BTSeleciona_hora_10			0X60	//96
#define BTSeleciona_hora_11			0X61	//97
#define BTSeleciona_hora_12			0X62	//98
#define BTSeleciona_hora_13			0X63	//99
#define BTSeleciona_hora_14			0X64	//100
#define BTSeleciona_hora_15			0X65	//101
#define BTSeleciona_hora_16			0X66	//102
#define BTSeleciona_hora_17			0X67	//103
#define BTSeleciona_hora_18			0X68	//104
#define BTSeleciona_hora_19			0X69	//105
#define BTSeleciona_hora_20			0X6A	//106
#define BTSeleciona_hora_21			0X6B	//107
#define BTSeleciona_hora_22			0X6C	//108
#define BTSeleciona_hora_23			0X6D	//109
// tela seleciona minutos							
#define BTMinutos_0				0X6E	//110
#define BTMinutos_5				0X6F	//111
#define BTMinutos_10				0X70	//112
#define BTMinutos_15				0X71	//113
#define BTMinutos_20				0X72	//114
#define BTMinutos_25				0X73	//115
#define BTMinutos_30				0X74	//116
#define BTMinutos_35				0X75	//117
#define BTMinutos_40				0X76	//118
#define BTMinutos_45				0X77	//119
#define BTMinutos_50				0X78	//120
#define BTMinutos_55				0X79	//121
// tela ajuste dos programas							
#define BTTecla_esquerda			0X7A	//122
#define BTTecla_direita				0X7B	//123
#define BTTecla_desce				0X7C	//124
#define BTTecla_sobe				0X7D	//125
#define BTTecla_decrementa			0X7E	//126
#define BTTecla_incrementa			0X7F	//127
#define BTSeleciona_param_1			0X80	//128
#define BTSeleciona_param_2			0X81	//129
#define BTSeleciona_param_3			0X82	//130
#define BTSeleciona_param_4			0X83	//131
#define BTSeleciona_param_5			0X84	//132
#define BTSeleciona_param_6			0X85	//133
#define BTSeleciona_param_7			0X86	//134
#define BTSeleciona_param_8			0X87	//135
#define BTSeleciona_param_9			0X88	//136
#define BTSeleciona_param_10			0X89	//137
#define BTSeleciona_param_11			0X8A	//138
#define BTSeleciona_param_12			0X8B	//139
#define BTSeleciona_param_13			0X8C	//140
#define BTSeleciona_param_14			0X8D	//141
#define BTSeleciona_param_15			0X8E	//142
#define BTSeleciona_param_16			0X8F	//143
#define BTSeleciona_param_17			0X90	//144
#define BTSeleciona_param_18			0X91	//145
#define BTSeleciona_param_19			0X92	//146
#define BTSeleciona_param_20			0X93	//147
// tela manual						
#define BTTecla_sobe_cesto			0X94	//148
#define BTTecla_desce_cesto			0X95	//149
#define BTgira_carrossel			0X96	//150
#define BTRetorna					0X97	//151
// tela ajusta temperatura parafina						
#define BTDecrementa_temp_parafina_1	0X98	//152
#define BTIncrementa_temp_parafina_1	0X99	//153
#define BTDecrementa_temp_parafina_2	0X9A	//154
#define BTIncrementa_temp_parafina_2	0X9B	//155
#define BTDecrementa_temp_parafina_3	0X9C	//156
#define BTIncrementa_temp_parafina_3	0X9D	//157
#define BTDecrementa_temp_parafina_4	0X9E	//158
#define BTIncrementa_temp_parafina_4	0X9F	//159
#define BTRetorna_2				0XA0	//160
// telado num�rico						
#define BTTecla_0					0XA1	//161
#define BTTecla_1					0XA2	//162
#define BTTecla_2					0XA3	//163
#define BTTecla_3					0XA4	//164
#define BTTecla_4					0XA5	//165
#define BTTecla_5					0XA6	//166
#define BTTecla_6					0XA7	//167
#define BTTecla_7					0XA8	//168
#define BTTecla_8					0XA9	//169
#define BTTecla_9					0XAA	//170
#define BTTecla_retroagir			0XAB	//171
#define BTTecla_unidade				0XAC	//172
#define BTtecla_dezena				0XAD	//173
#define BTTecla_centena				0XAE	//174
#define BTTecla_milhar				0XAF	//175
// Tela manutencao 1
#define BT_Resistencia_127_1			0X00AF
#define BT_Resistencia_220_1			0X00B0
#define BT_Resistencia_Desliga_1		0X00B1
#define BT_Resistencia_127_2			0X00B2
#define BT_Resistencia_220_2			0X00B3
#define BT_Resistencia_Desliga_2		0X00B4
#define BT_Resistencia_127_3			0X00B5
#define BT_Resistencia_220_3			0X00B6
#define BT_Resistencia_Desliga_3		0X00B7
#define BT_Resistencia_127_4			0X00B8
#define BT_Resistencia_220_4			0X00B9
#define BT_Resistencia_Desliga_4		0X00BA
#define BT_Motor_1_Sobe				0X00BB
#define BT_Motor_1_desce			0X00BC
#define BT_Motor_1_para				0X00BD
#define BT_Motor_2_Gira_carrossel		0X00BE
#define BT_Motor_2_para_carrossel		0X00BF
#define BT_Motor_3_vibracao			0X00C0
#define BT_Motor_3_para_vibracao		0X00C1

//Tela acerta tempo
#define BT_Salvar					0X00AD      //novo
#define BT_Ajusta_Dia				200
#define BT_Ajusta_Mes				201
#define BT_Ajusta_Ano				202
#define BT_Ajusta_Hora				203
#define BT_Ajusta_Minutos			204

#define BT_Pop_up_imediato			0X00CD
#define BT_Pop_up_retardo			0X00CE

#define BT_Pop_up_confirm_SIM  		0X00CF   // sim, � sinonimo de ok
#define BT_Pop_up_confirm_NAO  		0X00D0







/*
//Macros Sensores
#define fimCurso0 PTDD_PTDD0
#define fimCurso1 PTDD_PTDD1
*/






//Portas Acionamentos
#define RESET_RS232		PTAD_PTAD0	  	// Reseta RS232
#define Sobe_cesto		PTAD_PTAD1	  	// Aciona motor que sobe o cesto
#define Desce_cesto		PTAD_PTAD2	 	// Aciona motor que desce o cesto
#define Carrossel       	PTAD_PTAD3		// Gira carrossel     Motor 2
#define Vibrador        	PTAD_PTAD4		// Aciona vibracao do cesto  Motor 3

#define DRIVER_COM_1		PTBD_PTBD0   	// Drive de saida transistorizado
#define DRIVER_COM_2		PTBD_PTBD1   	// Drive de saida transistorizado
#define BUZZER_AUX		1//PTBD_PTBD2   	// Drive de saida transistorizado
#define AD1P3			PTBD_PTBD3		// Medida da corrente dos motores
#define AD1P4			PTBD_PTBD4		// Medida de temperatura 1
#define AD1P5			PTBD_PTBD5		// Medida de temperatura 2
#define AD1P6			PTBD_PTBD6		// Medida de temperatura 3
#define AD1P7			PTBD_PTBD7		// Medida de temperatura 4

#define AQC11			PTCD_PTCD2  		// Aciona Resist�ncia 1 110/220
#define TX_display		PTCD_PTCD3  		// Conex�o display TX
#define AQC12			PTCD_PTCD4  		// Aciona Resist�ncia 1 110
#define RX_display		PTCD_PTCD5  		// Conex�o display RX

//////////////////////////////
//POSICAO CARROSSEL
#define CARRO_ALTO    1
#define CARRO_BAIXO   2
#define CARRO_MEIO    3
#define CARRO_ERRO    4
#define CARRO_MOV     5

#define SENS_GIRO_CARROSSEL	PTDD_PTDD0  		// Fim de curso opto acoplada	 4
#define SENS_CESTO_BAIXO	  PTDD_PTDD2  		// Fim de curso opto acoplada	 3
#define SENS_CESTO_ALTO	    PTDD_PTDD3  		// Fim de curso opto acoplada	 2
#define SENS_POSICAO_0	    PTDD_PTDD4  		// Fim de curso opto acoplada	 1

#define IN_127_220		PTDD_PTDD5  		// Detecta tens�o de entrada
#define Externo_1			PTDD_PTDD6  		// Entrada digital 1 opto acoplada
#define Externo_2			PTDD_PTDD7  		// Entrada digital 2 opto acoplada

#define AQC21			PTED_PTED2  		// Aciona Resist�ncia 2 110/220
#define AQC22			PTED_PTED3  		// Aciona Resist�ncia 2 110
#define AQC31			PTED_PTED4  		// Aciona Resist�ncia 3 110/220
#define AQC32			PTED_PTED5  		// Aciona Resist�ncia 3 110
#define AQC41			PTED_PTED6  		// Aciona Resist�ncia 4 110/220
#define AQC42			PTED_PTED7  		// Aciona Resist�ncia 4 110

#define TPM1CH2			PTFD_PTFD0  		//
#define TPM1CH3			PTFD_PTFD1  		//
#define TPM1CH4			PTFD_PTFD2  		//
#define TPM1CH5			PTFD_PTFD3  		//
#define Aux_5			PTFD_PTFD4  		// porta para uso n�o definido


#define AUX_1			PTGD_PTGD0  		// Entrada digital AUX1 n�o isolada
#define AUX_2			PTGD_PTGD1  		// Entrada digital AUX2 n�o isolada
#define AUX_3			PTGD_PTGD2  		// Entrada digital AUX3 n�o isolada
#define AUX_4			PTGD_PTGD3  		// Entrada digital AUX4 n�o isolada

//ADC
#define AD3				0b00000011 			//TEMPERATURA PARAFINA 4 		
#define AD4 			0b00000100 			//TEMPERATURA PARAFINA 3 
#define AD5 			0b00000101			//TEMPERATURA PARAFINA 2 
#define AD6 			0b00000110			//TEMPERATURA PARAFINA 1 
#define AD7 			0b00000111 			//CORRENTE DO MOTOR 
 
//Temperatura
#define T_PARAF_4   	4 //4
#define T_PARAF_3    	3 //3
#define T_PARAF_2		2 //1
#define T_PARAF_1 	1 //2
#define CORRENTE_MOTOR  0 //0

//Comunicacao Nuvem
#define T_OUT_COM 10 //Time-out para comunica��o com nuvem

/////////////////////////////////////////////////////////////////////////////////////////////
//Variaveis Globais
/////////////////////////////////////////////////////////////////////////////////////////////
//Remoto
extern unsigned char flagWeb;
extern unsigned char flagZerar;
extern unsigned char flagConectado;
extern unsigned char text2caneca[20];
extern unsigned char text2tcaneca[20];
extern unsigned char text2pcanecaciclo[20];
extern unsigned char text2acao[20];
extern unsigned char text2tproxcaneca[20];
extern unsigned char TempoAgitando;
extern unsigned char SubDispExe;
extern unsigned char TempoEscorrendo;
extern unsigned char PtrProtDisp;
extern unsigned char BufProtDisp[30];
extern unsigned char segRemoto;
extern unsigned char minRemoto;
extern unsigned char horaRemoto;
extern unsigned char diaRemoto;
extern unsigned char mesRemoto;
extern unsigned char numCicloRemoto;
extern unsigned char trocaAlarme;
extern unsigned char numAlarmes;
extern unsigned char numAlarmesMov;
extern unsigned char numAlarmesTemp;
extern unsigned char numAlarmesAnt;
extern unsigned char numAlarmesNaoAceitos;
extern unsigned char multTimer;
extern unsigned char SENSOR_CESTO_BAIXO;
extern unsigned char SENSOR_CESTO_ALTO;
extern unsigned char SENSOR_POSICAO_0;
extern unsigned char SENSOR_GIRO_CARROSSEL;

extern unsigned char pausaCicloAlarme;
extern unsigned char numPopAceita;

extern unsigned char acetAlarTempBaixa[4];	
extern unsigned char acetAlarTempAlta[4];	
extern unsigned char acetAlarTempSensor[4];		
extern unsigned char acetAlarSobe;			
extern unsigned char acetAlarDesce;			
extern unsigned char acetAlarGiro;			
extern unsigned char acetAlarSensor;		

extern unsigned char cntDebBaixo;
extern unsigned char cntDebAlto;
extern unsigned char cntDebPos0;
extern unsigned char cntDebGiro;
                           
//extern unsigned char pinoN,cntPinoN++,

extern unsigned char cntNumAlarmes;
extern unsigned char ligaBuzzer;

extern unsigned char DelSendNuvem;

extern unsigned char TempoEscorreDefinido;


extern unsigned char TempoTempBaixa1;
extern unsigned char TempoTempBaixa2;
extern unsigned char TempoTempBaixa3;

extern unsigned char TempoSubindo;
extern unsigned char TempoDescendo;
extern unsigned char TempoGirando;

extern unsigned char TempoEscorreSave;
extern unsigned char TempoAgitandoSave;
extern unsigned char EstadoCicloSave;
extern unsigned char posCanecaSave;

extern unsigned char alarTempBaixa[4];
extern unsigned char alarTempAlta[4];
extern unsigned char alarTempSensor[4];
extern unsigned char alarSobe;
extern unsigned char alarDesce;
extern unsigned char alarGiro;
extern unsigned char alarSensor;

extern unsigned char posCanecaCicloAnt;

extern unsigned char meioSeg;
extern unsigned char Seg;
extern unsigned char flagLeRtc;
extern unsigned char TempoRetPause;

extern char text2alarme[20];
extern char text2temp1[10];
extern char text2temp2[10];
extern char text2temp3[10];
extern char text2ciclo[20];

extern unsigned char ptrAlarme;
extern unsigned char AtualizaAlarme;
extern unsigned char ptrAlarme;

extern unsigned char TamResp;
extern unsigned char Dado2Resp[200];

extern unsigned char posCarrossel;
extern unsigned char posCaneca,posCanecaAnt;
extern unsigned char auxPosAnt;

extern unsigned char EstadoCiclo;
extern unsigned char SubEstadoCiclo;
extern unsigned char NumCiclo;
extern unsigned char NumCicloTemp;

extern unsigned char Estpro;
extern unsigned char PtrProt;
extern unsigned char BufProt[50];
extern unsigned char DelComando;



//Serial
extern unsigned char PtrEnt1,PtrSai1,PtrEnt2,PtrSai2;
extern unsigned char BufRx1[MaxBufRx],BufRx2[MaxBufRx];
extern unsigned char EstSerial; //estado da maquina da serial
extern unsigned char flagPonte; //libera ponte do display para o PC

//Display
extern char Msg[50];      //string p/ conversao ascii
extern unsigned char numBytes;    //numero de bytes indicado do display
extern unsigned char comando;     //comando recebido do display
extern unsigned char endRamAlto;  //
extern unsigned char endRamBaixo; //
extern unsigned char numWords;    //
extern unsigned char valorLido;  //
extern unsigned char valorLido0;  //
extern unsigned char valorLido1;  //
extern unsigned char valorLido2;  //
extern unsigned char valorLido3;  //
extern unsigned char valorLido4;  //
extern unsigned char valorLido5;  //

extern unsigned char offSetIdioma; //Auxiliar para escolher tela com idioma correto
extern unsigned char teclaDisp;    //ultima tecla pressionada lido pelo key value
extern unsigned char teclaDisp1;   //ultima tecla pressionada lido pelo endere�o
extern unsigned char auxTelaAtual; 
 
extern unsigned char anoRtc;
extern unsigned char mesRtc;
extern unsigned char diaRtc;
extern unsigned char horaRtc;
extern unsigned char minRtc;
extern unsigned char segRtc;
extern unsigned char semRtc;

extern unsigned char auxPopAtual;
extern unsigned char buffTexto[20];

extern unsigned char flagEnaTeste;
extern unsigned char flagDirTeste;
extern unsigned int flagPulTeste; 

extern unsigned char atualizaIcon;

//RCO
extern unsigned char EstadoRco,EstadoRcoAnt,SubEstadoRco;
extern unsigned char DelTela;




extern unsigned char flagAtualizaDisp;
extern unsigned char EstRco;    //estado da m�quina do Rco
extern unsigned char EstRcoAnt;
extern unsigned char EstRcoAntPop;
extern unsigned char tempoIni;  //auxiliar para tempo de tela inicial
extern unsigned char flagDelay;
extern unsigned char contDelay;
extern unsigned char contDelay1;

extern unsigned char totalMicrasL;//byte baixo L e byte alto H 
extern unsigned char totalCortesL;
extern unsigned char totalMicrasH;
extern unsigned char totalCortesH;
extern int 			micras;
extern int 		totalCortes;
extern unsigned int totalMicras;
extern unsigned char buffSenha[4];
extern unsigned char novaSenha[4];
extern unsigned char contDigitoSenha;
extern unsigned char numPulsoMultAjuste;
extern unsigned char index;

extern int valorTrim;   //em uso
extern int valorSecao;  //em uso

extern int valorTrim1; //preset 1
extern int valorSecao1;//preset 1
extern int valorTrim2; //preset 2
extern int valorSecao2;//preset 2
extern int valorTrim3; //preset 3
extern int valorSecao3;//preset 3
extern int valorTrim4; //preset 4
extern int valorSecao4;//preset 4
extern int valorTrim5; //preset 5
extern int valorSecao5;//preset 5
extern int valorTrim6; //preset 6
extern int valorSecao6;//preset 6

extern unsigned char StTRIM;

extern unsigned char EstProxRco;
extern unsigned char EstadoSenha;

extern unsigned char ChCalPos;
extern unsigned char ChCalNeg;

extern unsigned char TempoSetCam;

//Controle
extern unsigned char periodoPulso; 
extern unsigned char periodoPulsoCont;
extern unsigned char periodoPulsoAjuste;
extern unsigned char fimMovimento;
extern unsigned char avancoAuto;
extern unsigned char unicoAvanco;
extern int numPulso;
extern int numPulsoCont;
extern unsigned char numPulsoMult;
extern unsigned char unicoRetorno;
  
extern unsigned char EstMotor;
extern unsigned char flagTimer;

extern unsigned char valorRetrac;

extern unsigned char StOutCompressor;   
extern unsigned char StOutPeltier;   
extern unsigned char StOutUV;   
extern unsigned char StOutInterna;   
extern unsigned char StOutOzonio;   
extern unsigned char StOutFog;   
extern unsigned char StOutAdicional;

extern unsigned char StDescongelamento;
extern unsigned char StStandBy;
extern unsigned char StBloqueio;
   

//Flash

extern unsigned char erroFlash;
extern unsigned char senhaFabricante[4];
extern unsigned char senhaUsuario[4]; 
extern unsigned char somaInicial;
extern unsigned char chkSum;
extern unsigned char buff2Flash[10];

extern unsigned char dadoFlash ;
extern unsigned char enderecoFlash;

extern unsigned char salvaCoeficentes;
extern unsigned char delAtualiza;

extern unsigned char flagAtualizaFlash;

//ACD
extern unsigned int   somaADC;
extern unsigned int   valorADC[5];
extern unsigned char  cntMediaADC;
extern unsigned char  auxConfigADC;
extern unsigned char indexADC;

extern long TAmbiente;
extern long TCamara;
extern long TPeltier;
extern long TAmostra;
extern long TExtra;

////Sets de temperatura efetivos

extern long TSetCamara;
extern long TSetPeltier;
extern long TSetAmostra;
extern long TSetExtra;

////Sets de temperatura para o modo normal
extern long TSetCamaraN;
extern long TSetPeltierN;
extern long TSetAmostraN;
extern long TSetExtraN;

extern unsigned char fTSetCamara[3];
extern unsigned char fTSetPeltier[3];
extern unsigned char fTSetAmostra[3];
extern unsigned char fTSetExtra[3];


extern long ErroTemp;
extern char HistTemp[5];


extern long TAux;
extern long TAux1;
//extern long TAux2;
extern long TOut;

extern long ADC_T1;
extern long ADC_T2;

extern long A_CH[5];
extern long B_CH[5];

extern long temp;
extern unsigned char A_1[5];
extern unsigned char A_2[5];
extern unsigned char A_3[5];
extern unsigned char B_1[5];
extern unsigned char B_2[5];
extern unsigned char B_3[5];
//////Programa��o
extern unsigned char minutosProg[35];
extern unsigned char horasProg[35];
extern unsigned char minutosProgTemp;
extern unsigned char horasProgTemp;
extern unsigned char indexProg;

extern unsigned char nbH;
extern unsigned char nbL;

extern unsigned char atualizaProg;



//minhas vari�veis
extern unsigned int off_set_idioma;
extern unsigned int Numero_programa;
extern unsigned int Numero_programa_edit;



//vari�veis programas
//Controle de Temperatura//////////////////////////////////////////////////////////////////////////////////
extern long int temperatura_ajustada_T1,  temperatura_ajustada_T1_temp;
extern long int temperatura_ajustada_T2,  temperatura_ajustada_T2_temp;
extern long int temperatura_ajustada_T3,  temperatura_ajustada_T3_temp;
extern long int temperatura_ajustada_T4,  temperatura_ajustada_T4_temp;

extern signed long int DUTY_1;
extern signed long int DUTY_2;
extern signed long int DUTY_3;
extern signed long int DUTY_4;
extern unsigned int Base_PWM;
extern unsigned int KP,KI;

extern signed long int Erro_t1,Erro_t2,Erro_t3,Erro_t4,erro_maximo;

extern signed long int	erro_integral_t1;
extern signed long int	erro_integral_t2;
extern signed long int	erro_integral_t3;
extern signed long int	erro_integral_t4;

extern signed long int	erro_anterior_t1;
extern signed long int	erro_anterior_t2;
extern signed long int	erro_anterior_t3;
extern signed long int	erro_anterior_t4;

extern signed long int off_set_t1;
extern signed long int off_set_t2;
extern signed long int off_set_t3;
extern signed long int off_set_t4;

extern signed long int tempT1;
extern signed long int tempT2;
extern signed long int tempT3;

//FIM Controle de Temperatura//////////////////////////////////////////////////////////////////////////////////

extern unsigned int  		index_parametro,index_parametro_temp;
extern unsigned int  		array_parametros [20][20];	    //Coluna 0 cont�m o ponteiro,coluna 1 a 20 programas
extern unsigned int		parametros_temp[20];
extern unsigned short int     salva_parametro;
extern unsigned long int	indexadorPP,indexadorPPold;
extern unsigned int i1,i2;


extern unsigned long int teste_interrupt;
extern unsigned long int atraso_geral;

extern unsigned int Aj_Horas,Aj_Minutos,Aj_Dia,Aj_Mes,Aj_Ano,numero_de_toques,minuto_anterior;
extern unsigned int dias_calendario[35];
extern unsigned int dia_old,conta_dias;
extern char ultimo_dia_mes;
extern int senha_usuario[4];
extern int senha_fab[4];
extern int senha_master[4];
extern int contra_senha[4];
extern int ind_index_senha,ind_index_horas,ind_index_horas_old,ind_index_horas_old2;
extern int grau_da_senha; 
extern int EstRco_OLD,EstRco_AUX,EstRco_OLD_AUX;
extern unsigned int posicao_carrossel,proxina_posicao_carrossel,posicao_aux_carrossel;
extern short int sensor_de_posicao;
extern short int detecta_borda_de_subida_carrossel;
extern short int detecta_borda_de_subida_pos_0;
extern short int Carrossel_sobe;
extern short int Carrossel_desce;
extern short int Carrossel_gira;
extern short int Carrossel_agita;
extern short int Flag_aquecimento;
extern short int Flag_PT100_1;
extern short int Flag_PT100_2;
extern short int Flag_PT100_3;
extern short int Flag_PT100_4;
extern short int Flag_buzzer_aux;
extern short int Flag_posicao_12;
extern short int Flag_carrosel_cima_baixo;     // 1 cima 0 baixo.
extern short int Flag_restabelece_condicao_anterior;
extern unsigned int comprimento_passo, numero_maximo_passos;

extern unsigned int posicao_carrocel_no_programa;

extern short int icone_salvar;


extern unsigned int debounce_sensor_carrossel;
extern int tempo_5_segundos,dia_semana_old;
extern unsigned int hora_execucao,minutos_execucao,segundos_execucao,dia_execucao,mes_execucao,ano_execucao;
extern unsigned long data_atual,hora_atual,data_exec,hora_exec;
extern unsigned int estado_do_carrossel;
extern unsigned long int semente_da_senha, semente_2;
extern unsigned int indice_dia,indice_dia_aux;
extern unsigned int STATUS_EXECUCAO;
extern unsigned int Sub_estagio_processo;
extern unsigned int Tempo_de_sub_estagio;
extern unsigned int Tempo_prox_sub_estagio;

extern unsigned int Tensao_alimentacao;
extern unsigned int indice_processo,ultima_fase;
extern unsigned int debounce_posicao_carrossel;

extern unsigned int hora_armazenada;
extern unsigned int minuto_armazenado;
extern unsigned int dia_armazenado;
extern unsigned int mes_armazenado;
extern unsigned int ano_armazenado;

extern unsigned int show_temperatura_parafina;
extern unsigned int nova_senha,nova_senha_aux;
extern unsigned char auxTela;
extern unsigned short int flag_passagem_senha;
extern unsigned int tempo_de_escorrimento;
extern short int flag_para_processo, flag_inicializa_carrossel, flag_bateria_esgotada;
extern unsigned int valida_flag;

extern unsigned int flag_vibrador, flag_subindo, flag_descendo, flag_girando;
extern signed int off_set_temperatura;
extern signed short int posicao_baixo,posicao_alto,posicao_indefinida,posicao_12,posicao_indice;
extern signed int posicao_baixo_i,posicao_alto_i,posicao_indefinida_i,posicao_12_i,posicao_indice_i;
extern unsigned int primeira_caneca_de_parafina,Prim_can_par_old;  //indica primeira caneca que receber� parafina.
extern unsigned int error01,error02;

// erros detectados /////////////////////////////////////
extern unsigned int posicao_carrossel_erro_movimento;
extern unsigned short int retorno_erro_baixa_temperatura;




extern short int flag_nao_repete_escrita;

extern signed long int off_set_temperatura1;
extern signed long int off_set_temperatura2;
extern signed long int off_set_temperatura3;
extern signed long int off_set_temperatura4;
extern unsigned int indice_off_set_temperatura;
extern unsigned int contador_erro_movimento;




///////////////////////////////////////////////////////// 	


/////////////////////////////////////////////////////////////////////////////////////////////
//Prototipos das funcoes
//////////////////////////////////////////////////////////////////////////////////
void CarregaValoresFlash(void);

unsigned char Proc_Zerar (void);
void EnviaComandoNuvem(unsigned char comando);
void ComandoRetornaAP(void);
void AtuPosCarro (void);
unsigned char Proc_Subida (void);
unsigned char Proc_Descida (void);
unsigned char Proc_Gira (void);
unsigned char Proc_Agita (char onoff);
unsigned char Proc_Desliga (void);
void CarregaDefault(void);
void DispAcao(unsigned char pos, unsigned char Nchar,  char *msgPrint);

unsigned char ChecaPopUPAceita(unsigned char tecla);
void DispCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint);
void DispTempoCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint);
void DispProxCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint);
void DispTempoProxCaneca(unsigned char pos, unsigned char Nchar,  char *msgPrint);

void AtuCamposExeCiclo(void);

void DispTemp1(unsigned char pos, unsigned char Nchar,  char *msgPrint);
void DispTemp2(unsigned char pos, unsigned char Nchar,  char *msgPrint);
void DispTemp3(unsigned char pos, unsigned char Nchar,  char *msgPrint);
void DispCiclo(unsigned char pos, unsigned char Nchar,  char *msgPrint);
void AtuTemperaturas(void);

void ControleExeCiclo (unsigned char num_ciclo);
void AtuBarraInfo(void);
//Serial
void TxChar1(char c);
void TxChar2(char c);
void PonteSerial(void);

//Display
void CarregaTela(unsigned char tela);
void CarregaPopUp(unsigned char pop);
void Buzzer(char tempo);
void AtualizaCampo(long int pos,long int valor);
void AtualizaIcone(char pos,char valor);
void LeRtc(void);
unsigned char HexToDec(unsigned char hexa);
unsigned char DecToHex(unsigned char hexa);
void SomaTotalMicras(unsigned char valor);
void AtualizaTexto(char *ptext, unsigned char tamanho,unsigned char posH, unsigned char posL);
void ConverteAscii(unsigned int num);

void AtualizaCasaDec(char posH, char posL, char nDec);       
void Display(void);


//Rco
char Delay(char tempo);
unsigned char ChecaSenha(void);
void GuardaSenha(void);
void LimpaSenha(void);
unsigned char ComparaSenha(void);
void Rco_MRI(void);

//Controle
void Controle(void);
char AvancaMotor();
char RetrocedeMotor();
char RetrocedeMotorTotal(void);
void ControlaRele (void);
void AvancoAuto (void);
void Medidas(void);
void ControlaTemperatura(void);
void ControlaFuncoesAuto(void);

//Flash
void InitFlash(void);
unsigned char ReadFlash(unsigned int endereco);
unsigned char WriteFlash(unsigned int endereco, unsigned char dado, unsigned char flagPrimeiroByte);
unsigned char PageEraseFlash(unsigned int endereco);
unsigned char AtualizaFlash(void);
unsigned char AtualizaFlash_UP(void);

//Procedimentos
void ProcIncTrim(void);
void ProcIncSec(void);
void ProcDecTrim(void);
void ProcDecSec(void);
void ProcPadraoH(unsigned char canal);
void ProcCoef(unsigned char canal, unsigned char faixa);
void ProcLeTemp(unsigned char canal);
void Inicializa_Port();
void Rco();
void MCU_init();

void Proc_Init_Ciclo(unsigned char tipo);
void Proc_Pausa_Ciclo(unsigned char tipo);
void Proc_Inter_Ciclo(unsigned char tipo);
void Proc_Ret_Ciclo(unsigned char tipo); 
void EnviaDataNuvem(void);
void AtuCampoAlarmes(void);

//novos prot�tipos
void destacador();
void Inicializa_variaveis();
void escreve_texto();
void confere_senha();
void escreve_senha_incorreta();
void escreve_senha();
void escreve_dia_semana();
void imprime_data();
void EscreveRtc();
void preenche_tela_de_uso();
void preenche_hora_execucao();
void gerador_de_semente();
void imprime_calendario();
void Avalia_dias_do_mes();
void Le_VP();
void maquina_rodando();
void desce_cesto();
void sobe_cesto();
void gira_carrossel_prog();
void carrega_variavel_tempo();
void Sobe_carrossel();
void Desce_carrossel();
void Gira_carrossel();
void Agita_carrossel();
void zera_buffer_dados_display();
void retorna_posicao_anterior();
void zera_carrossel();
void valores_padrao_de_fabrica();
void avalia_erro_posicao();
void carrega_erros_na_variavel();
void carrega_variavel_no_erro();
void controle_de_erros();
void temperatura_alta();
void temperatura_baixa();
void erro_sensor_temperatura();
void erro_movimento();
void elimina_campo();
void Detecta_erro_de_Sensor_temperatura();
void Detecta_erro_Temperatura_Extrema();
void Detecta_erro_Temperatura_baixa();