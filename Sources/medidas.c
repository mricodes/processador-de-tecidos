/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU02
/	EMPRESA: LUPETEC
/ Rotinas de Calculo das medidas de temperatura
===============================================================================================*/
#include "masterheader.h"
/////////////////////////////////////////////////////////////////////////////////////////////////
//     Realiza a atualizacao das medidas de temperatura     /////////////////////////////////////                      
/////////////////////////////////////////////////////////////////////////////////////////////////
void Medidas(void)
{
    if(EstRco>0)
    {
          
    }
    
}

void escreve_dia_semana()
{
	if(semRtc == 0) 
	{
	if (off_set_idioma == 0) {escreve_texto("  Domingo",0X01,0X06);return;}
	if (off_set_idioma == 1) {escreve_texto("   Sunday",0X01,0X06);return;}
	if (off_set_idioma == 2) {escreve_texto("  Domingo",0X01,0X06);return;}
	}
	
	if(semRtc == 1)
	{
	if (off_set_idioma == 0) {escreve_texto("  Segunda",0X01,0X06);return;}
	if (off_set_idioma == 1) {escreve_texto("    Lunes",0X01,0X06);return;}
	if (off_set_idioma == 2) {escreve_texto("   Monday",0X01,0X06);return;}	
	}
	
	if(semRtc == 2)
	{
	if (off_set_idioma == 0) {escreve_texto("    Terca",0X01,0X06);return;}
	if (off_set_idioma == 1) {escreve_texto("   Martes",0X01,0X06);return;}
	if (off_set_idioma == 2) {escreve_texto("  Tuesday",0X01,0X06);return;}
	}
	
	if(semRtc == 3)
	{
	if(off_set_idioma == 0) {escreve_texto("   Quarta",0X01,0X06);return;}
	if(off_set_idioma == 1) {escreve_texto("Miercoles",0X01,0X06);return;}
	if(off_set_idioma == 2) {escreve_texto("Wednesday",0X01,0X06);return;}
	}
	
	if(semRtc == 4)
	{
	if(off_set_idioma == 0) {escreve_texto("   Quinta",0X01,0X06);return;}
	if(off_set_idioma == 1) {escreve_texto("   Jueves",0X01,0X06);return;}
	if(off_set_idioma == 2) {escreve_texto(" Thursday",0X01,0X06);return;}
	}
	
	if(semRtc == 5)
	{
	escreve_texto("  Sexta",0X01,0X06);
	if(off_set_idioma == 0) {escreve_texto("    Sexta",0X01,0X06);return;}
	if(off_set_idioma == 1) {escreve_texto("  Viernes",0X01,0X06);return;}
	if(off_set_idioma == 2) {escreve_texto("   Friday",0X01,0X06);return;}
	}
	
	if(semRtc == 6)
	{
	if(off_set_idioma == 0) {escreve_texto("   Sabado",0X01,0X06);return;}
	if(off_set_idioma == 1) {escreve_texto("   Sabado",0X01,0X06);return;}
	if(off_set_idioma == 2) {escreve_texto(" Saturday",0X01,0X06);return;}
	}	
}


void preenche_tela_de_uso()
{
    AtualizaCampo(Programa_executado,Numero_programa + 1);
    AtualizaCampo(Campo_posicao,posicao_carrossel);
    AtualizaCampo(Campo_proxima_posicao,proxina_posicao_carrossel);
    AtualizaCampo(posicao_carrossel2,posicao_carrossel);
    AtualizaCampo(Tempo_atual  , Tempo_de_sub_estagio);
    AtualizaCampo(Tempo_futuro , array_parametros [proxina_posicao_carrossel] [Numero_programa]);  
    preenche_hora_execucao();
}


void preenche_hora_execucao()
{
int hora_exec1[15];
int i3;

hora_exec1[0]  = (dia_execucao / 10 )    | 0X30;
hora_exec1[1]  = (dia_execucao % 10 )    | 0X30;
hora_exec1[2]  = '/';
hora_exec1[3]  = (mes_execucao / 10  )   | 0X30;
hora_exec1[4]  = (mes_execucao % 10  )   | 0X30;   
hora_exec1[5]  = '/';
hora_exec1[6]  = (ano_execucao / 10 )    | 0X30;
hora_exec1[7]  = (ano_execucao % 10 )    | 0X30;


hora_exec1[8]  = ' ';
hora_exec1[9]  = (hora_execucao / 10 )   | 0X30;
hora_exec1[10]  = (hora_execucao % 10 )   | 0X30;
hora_exec1[11]  = ':';
hora_exec1[12]  = (minutos_execucao / 10) | 0X30;
hora_exec1[13] = (minutos_execucao % 10) | 0X30;   
hora_exec1[14] = 0X00;

i3 = 15;		
TxChar2(0xA5);
TxChar2(0x5A);
TxChar2(i3+0x03);
TxChar2(0x82);
TxChar2(0X02);
TxChar2(0X6C);		
 	for(i1=0 ; i1 < i3 ; i1++)
	{
	TxChar2(hora_exec1[i1]);
	}	
}

void mensagens_estado_atual()
{
	switch(Sub_estagio_processo)
	{
	case 0: 
	if (off_set_idioma == 0) escreve_texto("Zera posicao       ",0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("zera posicion      ",0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("reset position     ",0X01,0X72);	
	break;
	case 1: 
	if (off_set_idioma == 0) escreve_texto("Descendo           ",0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("abajo              ",0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("going down         ",0X01,0X72);
	break;
	case 2: 
	if (off_set_idioma == 0) escreve_texto("Espera             " ,0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("Espera             " ,0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("wait               " ,0X01,0X72);
	break;
	case 3: 
	if (off_set_idioma == 0) escreve_texto("Subindo            " ,0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("ascender           " ,0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("Going up           " ,0X01,0X72);
	break;
	case 4: 
	if (off_set_idioma == 0) escreve_texto("Girando            " ,0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("torneado           " ,0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("Turning            " ,0X01,0X72);
	break;	
	case 5: 
	if (off_set_idioma == 0) escreve_texto("Pausa              "  ,0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("Pausa              "  ,0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("Break              "  ,0X01,0X72);
	break;	
	case 6: 
	if (off_set_idioma == 0) escreve_texto("Hora prog.         "  ,0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("Hora prog.         "  ,0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("sched. time        "  ,0X01,0X72);
	break;	
	if (off_set_idioma == 0) escreve_texto("Espera             " ,0X01,0X72);
	if (off_set_idioma == 1) escreve_texto("Espera             " ,0X01,0X72);
	if (off_set_idioma == 2) escreve_texto("wait               " ,0X01,0X72);
	}			
}


void imprime_data()
{
    AtualizaCampo(Campo_dia ,Aj_Dia);
    AtualizaCampo(Campo_mes ,Aj_Mes);
    AtualizaCampo(Campo_ano ,Aj_Ano);
    AtualizaCampo(Campo_hora ,Aj_Horas);
    AtualizaCampo(Campo_minutos ,Aj_Minutos);	
}
/*
void gerador_de_semente()
{
char semente_aux;
semente_aux = (segRtc + minRtc + horaRtc + diaRtc); 
srand(semente_aux);
semente_da_senha = rand() % 9999;	
AtualizaCampo(Semente_senha , semente_da_senha);

senha_master[0]  = (semente_da_senha / 1000) + 7;
semente_da_senha = (semente_da_senha % 1000);
	
senha_master[1]  = (semente_da_senha / 100) + 5;
semente_da_senha = (semente_da_senha % 100);
		
senha_master[2]  = (semente_da_senha / 10) + 3;
semente_da_senha = (semente_da_senha % 10);
	
senha_master[3]  = (semente_da_senha) + 2;
semente_da_senha = senha_master[3];
	
if(senha_master[0] > 9) senha_master[0] = senha_master[0] - 10;
if(senha_master[1] > 9) senha_master[1] = senha_master[1] - 10;
if(senha_master[2] > 9) senha_master[2] = senha_master[2] - 10;
if(senha_master[3] > 9) senha_master[3] = senha_master[3] - 10;  
}
*/