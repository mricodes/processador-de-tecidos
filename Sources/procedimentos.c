/*===============================================================================================
/	EQUIPAMENTO:Placa LPCPU01
/	EMPRESA: LUPETEC
/ Rotinas de Procedimentos
===============================================================================================*/
#include "masterheader.h"




void maquina_rodando()
{

}

void carrega_variavel_tempo()
{

}


void Sobe_carrossel()
{

}

void Desce_carrossel()
{

}
void Gira_carrossel()
{

}	


void zera_carrossel()
{

}

void avalia_erro_posicao()
{

	
}


void avalia_posicao()
{
}

//////////////////////////////////////////////
//Procedimento para inicio imediato de ciclo
// tipo: 1 imediato
//       
//       3 remoto
//////////////////////////////////////////////
void Proc_Init_Ciclo(unsigned char tipo)
{
  
       if(tipo == 1) //Imediato
       {
          //Carrega tempo atual para inciar execu��o
          segundos_execucao = segRtc;
          minutos_execucao = minRtc;
          hora_execucao = horaRtc;
          dia_execucao = diaRtc;
          mes_execucao = mesRtc;
          //Carrega Ciclo selecionado para execu��o          
          NumCiclo = NumCicloTemp;
          NumCicloTemp = 0; 
          pausaCicloAlarme = 0;         
       }
       if(tipo == 2)
       {
          NumCiclo = NumCicloTemp;
          NumCicloTemp = 0; 
          pausaCicloAlarme = 0;         
       }
       if(tipo==3)
       {
          //Carrega tempo atual para inciar execu��o
          segundos_execucao = segRemoto;
          minutos_execucao = minRemoto;
          hora_execucao = horaRemoto;
          dia_execucao = diaRemoto;
          mes_execucao = mesRemoto;
          //Carrega Ciclo selecionado para execu��o
          NumCiclo = numCicloRemoto - 1;
        }
        EstadoCiclo = ESTA_CICLO_AGUARDA; 
    
}
//////////////////////////////////////////////
//Procedimento para pausa do ciclo
// tipo: 1 interno
//       3 remoto
//////////////////////////////////////////////
void Proc_Pausa_Ciclo(unsigned char tipo)
{
      //Salva o tempo atual de agitacao e escorrer e Para 
      TempoEscorreSave = TempoEscorrendo;
      TempoAgitandoSave = TempoAgitando;
      posCanecaSave = posCaneca;
      
      TempoAgitando = 0xFF;
      TempoEscorrendo = 0xFF;
      
      //Salva ciclo
      EstadoCicloSave = EstadoCiclo;
      Proc_Desliga();
      SubEstadoCiclo = 0;
      TempoRetPause = 0;
      EstadoCiclo = ESTA_CICLO_PAUSADO;
}
 //////////////////////////////////////////////
//Procedimento para interromper o ciclo
// tipo: 1 interno
//       3 remoto
//////////////////////////////////////////////
void Proc_Inter_Ciclo(unsigned char tipo)
{
    EstRcoAntPop = 0;       //     0X01 - PAUSADO
    auxPopAtual = 0;        //     0X02 - AGUARDANDO HORA PROGRAMADA
    //CarregaPopUp(0x0D);     //	   0X03 - PROGRAMA RODANDO
    TempoSubindo = TempoDescendo = TempoGirando = 0xFF;
    delAtualiza = 1;
    EstadoCiclo = 0;
    EstRco = EST_INICIAR;

}
//////////////////////////////////////////////
//Procedimento para retomar o ciclo depois de pausar
// AO Pausar reinicia a fase
// tipo: 1 interno
//       3 remoto
//////////////////////////////////////////////
void Proc_Ret_Ciclo(unsigned char tipo)
{
      //Salva ciclo
      EstRcoAntPop = 0;       
      auxPopAtual = 0;  
      CarregaPopUp(0x34);//Fechar pop-up
             
             
      EstadoCiclo = ESTA_CICLO_DESCE;         
      //se mudou de caneca deve voltar para fase descer
      //if(posCanecaSave < posCaneca)
      //{
      //    EstadoCiclo = ESTA_CICLO_DESCE;  
      //}
      //else
      //{
      //    EstadoCiclo = EstadoCicloSave;  
      //}
      //if(posCarrossel == CARRO_ALTO)
      //{
      //  
      //}
      
      SubEstadoCiclo = 0; 
      
      TempoRetPause = 0xFF; //para timer de retorno  
}
/////////////////////////////////////////
//Atualiza Posicao Carrossel
//
/////////////////////////////////////////
void AtuPosCarro (void)
{
    //Chegou no alto
    if(SENSOR_CESTO_ALTO)
    {
       posCarrossel = CARRO_ALTO;
       TempoSubindo = 0xFF;//Para timer de subida
       Sobe_cesto  = 0;
    }
    //Chegou em baixo
    if(SENSOR_CESTO_BAIXO)
    {
       posCarrossel = CARRO_BAIXO;
       TempoDescendo = 0xFF;//Para timer de descida
       Desce_cesto  = 0;
    }
    //Esta no meio (os dois sensores n�o acionados
    if(!SENSOR_CESTO_BAIXO && !SENSOR_CESTO_ALTO)    
    {
       posCarrossel = CARRO_MEIO;
    }
    //Situa��o de erro dois sensores acionados (condi��o fisica imposs�vel)
    if(SENSOR_CESTO_BAIXO && SENSOR_CESTO_ALTO)    
    {
      posCarrossel = CARRO_ERRO;
      Sobe_cesto  = Desce_cesto = Carrossel = Vibrador = 0;
    }
/////////Atualizacao do GIRO//////////
    //Estava na posicao e saiu
    if(!SENSOR_GIRO_CARROSSEL && auxPosAnt == 1)
    {
      auxPosAnt = 0;
    }
    //Estava fora de posi��o e entrou
    if(SENSOR_GIRO_CARROSSEL && auxPosAnt == 0)
    {
      posCaneca++;
      auxPosAnt = 1;
    }
    //Mudou de posicao
    if((posCaneca > posCanecaAnt) || (flagZerar ==  1))
    {
      if(flagZerar == 1 && posCaneca == 1)
      {
        Carrossel  = 0;//Para 
        flagZerar = 0;   
      }
      if(flagZerar == 0)
      {
         Carrossel  = 0;//Para 
      }
      TempoGirando = 0xFF;//Para timer de giro
    }
    //Passou da caneca de posicao maior (foi para a primeira)
    if(posCaneca > MAX_CANECAS)
    {
      posCaneca = 1;
    }
    //Chegou no fim do ciclo (ultima caneca)
    if(SENSOR_POSICAO_0)//passou pelo sensor de fim de ciclo Caneca Max
    {
       posCaneca = MAX_CANECAS;
       flagZerar = 1;
    }
///////////FIM Atualiza�ao do GIRO/////////////    
}
////////////////////////////////////////////
//Procedimento Subida
// retorno: 0 n�o executado  
//          1 executado com sucesso
///////////////////////////////////////////
unsigned char Proc_Subida (void)
{
    unsigned char ret = 0;
    AtuPosCarro();
    //n�o est� no alto e n�o tem erro e est� alinhado
    if((posCarrossel != CARRO_ALTO) && (posCarrossel != CARRO_ERRO) && SENSOR_GIRO_CARROSSEL)
    {
       if(Sobe_cesto  == 1)//j� est� subindo
       {
            Sobe_cesto  = 0;//Para
            TempoSubindo = 0xFF;//Para timer de subida
       }
       else
       {
          if(Desce_cesto == 0 && Carrossel == 0 && Vibrador == 0)//n�o est� ligado descida
          {
              Sobe_cesto  = 1;//Sobe
              TempoSubindo = 0;//Liga timer de subida
              ret = 1;
          }    
       }  
    }
    return ret;
}

////////////////////////////////////////////
//Procedimento Descida
// retorno: 0 n�o executado  
//          1 executado com sucesso
///////////////////////////////////////////
unsigned char Proc_Descida (void)
{
    unsigned char ret =0;
    AtuPosCarro();
    //n�o est� embaixo e n�o tem erro e est� alinhado
    if((posCarrossel != CARRO_BAIXO) && (posCarrossel != CARRO_ERRO) && SENSOR_GIRO_CARROSSEL)
    {
       if(Desce_cesto  == 1)//j� est� descendo
       {
            Desce_cesto  = 0;//Para
            TempoDescendo = 0xFF;//Para timer de descida
       }
       else
       {
          if(Sobe_cesto == 0 && Carrossel == 0 && Vibrador == 0)//n�o est� ligado subida
          {
              Desce_cesto  = 1;//desce
              TempoDescendo = 0;//Liga timer de descida
              ret = 1;
          }    
       }
    }
    else//testa erro fora de posicao
    {
         if(SENSOR_GIRO_CARROSSEL)
         {
           /////ERROOO FORA DE POSICAO/////
         }
    }
    return ret;
}
////////////////////////////////////////////
//Procedimento Zerar
///////////////////////////////////////////
unsigned char Proc_Zerar (void)
{
    Proc_Gira();
    if(flagZerar == 0)
    {
      flagZerar = 3;  
    }
    else
    {
      flagZerar = 0;
    }
    return 1;


}
////////////////////////////////////////////
//Procedimento Giro
// retorno: 0 n�o executado  
//          1 executado com sucesso
///////////////////////////////////////////
unsigned char Proc_Gira (void)
{
    unsigned char ret = 0;
    
    posCanecaAnt = posCaneca;
    AtuPosCarro();
    //deve estar no alto, n�o tem erro e PARADO
    if((posCarrossel == CARRO_ALTO) && (posCarrossel != CARRO_ERRO))
    {
       if(Carrossel  == 1 && flagZerar == 0)//j� est� girando
       {
            Carrossel  = 0;//Para
            TempoGirando = 0xFF;//Para timer de giro
       }
       else
       {
          if(Sobe_cesto == 0 && Desce_cesto == 0 && Vibrador == 0)//PARADO
          {
              Carrossel  = 1;//gira
              TempoGirando = 0;//Liga timer de giro
              ret = 1;
          }    
       }
    }
    return ret;
}

////////////////////////////////////////////
//Procedimento Agita��o
//onoff: 1 liga agitacao
//       0 desliga agitacao 
///////////////////////////////////////////
// retorno: 0 n�o executado  
//          1 executado com sucesso
///////////////////////////////////////////
unsigned char Proc_Agita (char onoff)
{
    unsigned char ret = 0;
    AtuPosCarro();
    //deve estar embaixo, n�o tem erro, na posicao
    if((posCarrossel == CARRO_BAIXO) && (posCarrossel != CARRO_ERRO) && SENSOR_GIRO_CARROSSEL)
    {
       if(onoff  == 1)//Liga
       {
          if(Sobe_cesto == 0 && Desce_cesto == 0 && Vibrador == 0)//PARADO
          {
            Vibrador  = 1;//Agita
            ret = 1;
          }
       }
       else if(onoff == 0)//Desliga
       {
          Vibrador  = 0;  //Desliga  
          ret = 1;
       }
    }
    else//testa erro fora de posicao
    {
         if(SENSOR_GIRO_CARROSSEL)
         {
           /////ERROOO FORA DE POSICAO/////
         }
    }
    return ret;
}
////////////////////////////////////////////
//Procedimento Desliga Motores
// retorno: 0 n�o executado  
//          1 executado com sucesso
///////////////////////////////////////////
unsigned char Proc_Desliga (void)
{
    unsigned char ret = 0;
    AtuPosCarro();     
     
    Vibrador = 0;     // motor 3
    Carrossel = 0;    // motor 2
    Desce_cesto = 0;  // motor 1	
    Sobe_cesto = 0; 
    TempoSubindo = 0xFF;//Para timer de subida
    TempoDescendo = 0xFF;//Para timer de descida
    TempoGirando = 0xFF;//Para timer de giro  
    ret = 1;
    return ret;
}
